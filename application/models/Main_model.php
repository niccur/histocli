<?php

class main_model extends CI_Model{
	
     function __construct() 
	{
        parent::__construct();	
	    $this->load->database();

    }
	
    public function traerMenuTree()
	{
		$this->db->select('id, id_padre, nombre, ruta');
		$this->db->order_by("id", "asc"); 
		$query = $this->db->get('menutree');
		
		return $query->result();
    }
    public function load_profesionales(){
        $this->db->select("a.id as idProf,a.nombre,a.apellido", FALSE);      
        $this->db->from("profesional a");							
        $this->db->order_by("a.nombre");
        $query = $this->db->get();		
	return $query->result();  
        
    }
		
    public function get_turnos_profesional_main($username) {
          $this->db->select(" a.id_paciente, 
                                a.id id_turno, 
                                a.id_profesional, 
                                a.turno,
                                a.descripcion,
                                b.nombre, 
                                b.apellido,
                                b.notas,
                                if((DATE_ADD(NOW(), INTERVAL 24 HOUR) >=  a.turno),'warning','ok') estado",FALSE
                        );         
        $this->db->from('trat_paciente a');
        $this->db->join('paciente b', 'b.id = a.id_paciente');
        $this->db->join('usuarioprofs c', 'c.id_profesional = a.id_profesional');
        $this->db->where('c.username = "'.$username.'" and a.id_histo is null');
        $this->db->order_by('a.turno');
        
        $query = $this->db->get();  
        //echo "<pre>";
        //var_dump($this->db->last_query());
        //exit;	
        return $query->result(); 
    }
    public function get_turnos_profesional($idProf) {
        $this->db->select(" a.id_paciente, 
                              a.id id_turno, 
                              a.id_profesional, 
                              a.turno,
                              a.descripcion,
                              b.nombre, 
                              b.apellido,
                              b.notas,
                              if((DATE_ADD(NOW(), INTERVAL 24 HOUR) >=  a.turno),'warning','ok') estado",FALSE
                      );         
      $this->db->from('trat_paciente a');
      $this->db->join('paciente b', 'b.id = a.id_paciente');
      $this->db->where('a.id_profesional = "'.$idProf.'" and a.id_histo is null');
      $this->db->order_by('a.turno');
      
      $query = $this->db->get();  
      //echo "<pre>";
      //var_dump($this->db->last_query());
      //exit;	
      return $query->result(); 
  }
    function get_id_profesional_session($username){
        $this->db->select("a.id_profesional",FALSE);
        $this->db->from("usuarioprofs a");
        $this->db->where("username = '".$username."'");
        $query = $this->db->get();
	    return $query->num_rows();    
    }
        
    public function eliminar_turno($idTurno){
        $this->db->where('id',$idTurno);
        $this->db->delete('trat_paciente');

        if (!$this->db->affected_rows()) {
            return array('valido'=>false);
        } else {
            return array('valido'=>true);
        }
    }
    public function eliminar_profesional($idProf){

        $this->db->select('id_profesional');
        $this->db->from('trat_paciente');
        $this->db->where("id_profesional = ".$idProf);
        $num_results = $this->db->count_all_results();
        if($num_results > 0 ){
            return array('valido'=>3);
        } //error existe dependencia de HC

        $this->db->select('id_profesional');
        $this->db->from('histo');
        $this->db->where("id_profesional = ".$idProf);
        $num_results = $this->db->count_all_results();
        if($num_results > 0 ){
            return array('valido'=>2);
        } //error existe dependencia de HC

        $this->db->where('id',$idProf);
        $this->db->delete('profesional');

        if (!$this->db->affected_rows()) {
            return array('valido'=>1); //error no borro nada
        } else {
            return array('valido'=>0); // ok
        }
    }

    public function agregar_profesional($data)
    {
		//array es de objetos con flechita $data->start;
		//array simple con corchetes corchetes  $data['start'];
        $insert_id = null;
	    $this->db->trans_start();
        $this->db->insert('profesional',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         
//aca devolver el id del paciente 
         if($insert_id != null){
            return array('valido'=>true,
                           'id' => $insert_id );
          }else return array('valido'=>false,'id' => 0);
	
    } 
}
