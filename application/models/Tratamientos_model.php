<?php

class Tratamientos_model extends CI_Model{
   //ACTIVE RECORD
	
     function __construct() 
    {
        parent::__construct();	
	    $this->load->database();
    }	
    
    public function load_tratamientos_cab(){
        $this->db->select("a.id, a.nombre,
                           a.descripcion,
                           a.estado,
                           null detalle", FALSE);      
        $this->db->from("tratamiento_cab a");
        $this->db->where("a.estado = 0");
        $query = $this->db->get();		
	$tratamientos = $query->result();         
        if($tratamientos){
            foreach ($tratamientos as $row) {
                $this->db->select("c.id_tratamiento_cab,
                                   c.id,
                                   a.id id_intervencion,
                                   a.nombre,
                                   a.descrip,
                                   b.nombre departamento,
                                   c.sesiones,
                                   c.estado", FALSE);      
                $this->db->from("intervencion a,departamento b,tratamiento_det c");
                $this->db->where("c.id_tratamiento_cab = ".$row->id." and c.estado = 0 and
                                    a.id = c.id_intervencion and
                                    b.id = a.id_departamento");							
                $this->db->order_by("c.id asc");
                $query = $this->db->get();
                $row->detalle = $query->result();
            }
        }
        return $tratamientos;
    }
    public function load_profesionales(){
        $this->db->select("a.id as idProf,"
                        . "concat(a.nombre,' ',a.apellido) as nombre", FALSE);      
        $this->db->from("profesional a");							
        $this->db->order_by("a.nombre");
        $query = $this->db->get();		
	return $query->result();  
        
    }
    public function load_tratamiento_det($cab){
        $this->db->select("c.id,
                        a.id id_intervencion,
                        a.nombre,
                        a.descrip,
                        b.nombre departamento,
                        c.sesiones,
                        c.estado", FALSE);      
        $this->db->from("intervencion a,departamento b,tratamiento_det c");
        $this->db->where("c.id_tratamiento_cab = ".$cab." and c.estado = 0 and
                        a.id = c.id_intervencion and
                        b.id = a.id_departamento");							
        $this->db->order_by("c.id asc");
        $query = $this->db->get();		
	return $query->result();  
        
    }
    public function load_pendientes_paciente($idPaciente){
        $select = "a.id,
                  a.id_paciente,                  
                  a.id_profesional,
                  concat(date_format(a.turno,'%d/%m/%Y - %H:%i'),'Hs') as turno,                  
                  a.turno as turno_original,
                  d.nombre profesional,
                  a.descripcion,
                  if((DATE_ADD(NOW(), INTERVAL 1 HOUR) >=  turno),'warning','ok') estado,
                  null as intervenciones";    
        $this->db->select($select,FALSE);
        $this->db->where('a.id_paciente = '.$idPaciente.' and a.id_histo Is Null and '.
                         'a.id_profesional = d.id');
        $this->db->from("trat_paciente as a, profesional as d");
        $this->db->order_by("a.turno");
        $query = $this->db->get();
        $intervenciones = $query->result();         
        if($intervenciones){
            foreach ($intervenciones as $row) {
                $this->db->select("a.id, b.id id_intervencion, b.nombre intervencion",FALSE);
                $this->db->where("a.id_trat_paciente = ".$row->id." and"
                        . "       b.id = a.id_intervencion");
                $this->db->from("trat_paciente_inter as a, intervencion as b");
                $query = $this->db->get();
                $row->intervenciones = $query->result();   
            }           
        }
     
	return $intervenciones; 
    
    }
    
    public function load_pendientes_paciente_turnos($idPaciente){
        $select = "a.id,
                  a.id_paciente,
                  a.id_turno,
                  a.id_tratamiento_cab, 
                  a.id_intervencion,
		  concat(DATE_FORMAT(c.start, '%d/%m/%Y %H:%i'),'Hs') AS turno,
                  d.nombre promo,
                  b.nombre intervencion,
		  a.sesion";        
        $this->db->select($select,FALSE);
        $this->db->where('a.id_paciente = '.$idPaciente.' and a.id_histo Is Null and '.
                         'a.id_intervencion = b.id and a.id_turno Is Null');
        $this->db->from("trat_paciente as a, intervencion as b");
        $this->db->join('turno as c', 'a.id_turno = c.id', 'left outer');
        $this->db->join('tratamiento_cab as d', 'a.id_tratamiento_cab = d.id', 'left outer');
        $this->db->order_by("a.id_intervencion, a.sesion");
        $query = $this->db->get();		
	return $query->result(); 
    }    
    public function load_tratamiento_det_precio($idDet){
        $this->db->select("c.id, c.id_intervencion, c.id_tratamiento_cab,a.nombre,a.descrip,
                        c.sesiones, c.estado,p.precio pesos_unit,d.precio dolares_unit,
                        desc_trat.porcentaje porc_desc_trat,
                        desc_trat.nombre nom_desc_trat,                        
                        desc_trat.descripcion desc_desc_trat,
                        date_format(desc_trat.fecha_desde,'%Y-%m-%d') fdesde_desc_trat,
                        date_format(desc_trat.fecha_hasta,'%Y-%m-%d') fhasta_desc_trat,
                        desc_intrv.porcentaje porc_desc_intrv,
                        desc_intrv.nombre nom_desc_intrv,                        
                        desc_intrv.descripcion desc_desc_intrv,
                        date_format(desc_intrv.fecha_desde,'%Y-%m-%d') fdesde_desc_intrv,
                        date_format(desc_intrv.fecha_hasta,'%Y-%m-%d') fhasta_desc_intrv
                        ", FALSE);      
        $this->db->from("intervencion a,tratamiento_det c");
        $this->db->join("(select a.precio, a.id_intervencion
                        from precios a
                        where a.moneda = 0 and
                       a.vigencia = (Select max(x.vigencia)
                                      from precios x
                                      where x.id_intervencion = a.id_intervencion and
                                          x.moneda = a.moneda and
                                          date_format(x.vigencia,'%Y-%m-%d') <= date_format(sysdate(),'%Y-%m-%d')
                                      )
                        ) p", "c.id_intervencion = p.id_intervencion", "left outer");	
        $this->db->join("(select a.precio, a.id_intervencion
                        from precios a
                        where a.moneda = 1 and
                                   a.vigencia = (Select max(x.vigencia)
                                                  from precios x
                                                  where x.id_intervencion = a.id_intervencion and
                                                      x.moneda = a.moneda and
                                                      date_format(x.vigencia,'%Y-%m-%d') <= date_format(sysdate(),'%Y-%m-%d')
                                                         )
                        ) d", "c.id_intervencion = d.id_intervencion", "left outer");        
        $this->db->join("(select a.id_tratamiento_cab,
                                a.nombre,
                                a.porcentaje ,
                                a.descripcion,
                                a.fecha_desde,
                                a.fecha_hasta
                        from descuentos a
                        where   a.id_intervencion Is Null and	
                                a.estado = 0 and
                                a.id_paciente Is Null and
                                a.id_producto Is Null and
                                date_format(sysdate(),'%Y-%m-%d') between date_format(a.fecha_desde,'%Y-%m-%d') and date_format(a.fecha_hasta ,'%Y-%m-%d') ) desc_trat", 
                           "c.id_tratamiento_cab = desc_trat.id_tratamiento_cab", "left outer");
        $this->db->join("(select a.id_intervencion,
                                a.nombre,
                                a.porcentaje ,
                                a.descripcion,
                                a.fecha_desde,
                                a.fecha_hasta
                        from descuentos a
                        where a.id_tratamiento_cab Is Null and
                                a.estado = 0 and
                                a.id_paciente Is Null and
                                a.id_producto Is Null and
                                date_format(sysdate(),'%Y-%m-%d') between date_format(a.fecha_desde,'%Y-%m-%d') and date_format(a.fecha_hasta ,'%Y-%m-%d')) desc_intrv", 
                           "c.id_intervencion = desc_intrv.id_intervencion", "left outer");

        $this->db->where("c.id = ".$idDet." and a.id = c.id_intervencion");	
        $this->db->order_by("c.id asc");
        $query = $this->db->get();		
	return $query->row();  
    }
    
    public function agregar_tratamiento_paciente($data){
        $paciente = $data["paciente"];
        $idPaciente = $paciente['idPaciente'];
        $idProfesional =  $data["profesional"];  
        $descripcion =  $data["descripcion"];        
        $turno =  $data["turno"];
        
        $insertar_trat_paciente = array('id_paciente' =>$idPaciente,
                                        'descripcion' => $descripcion,
                                        'id_profesional' =>$idProfesional,
                                        'turno' => $turno);
        
        $insert_id = null;
	$this->db->trans_start();
        $this->db->insert('trat_paciente',$insertar_trat_paciente);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        
        if($insert_id != null){
            return array('idPaciente'=>$idPaciente);
          }else return array('valido'=>false,'id' => 0);                
        }
        
 public function eliminar_tratamientos_paciente($idTrat){
        $this->db->where('id_trat_paciente',$idTrat);
        $this->db->delete('trat_paciente_inter');
        $this->db->where('id',$idTrat);
        $this->db->delete('trat_paciente');

        if (!$this->db->affected_rows()) {
            return array('valido'=>false);
        } else {
            return array('valido'=>true);
        }

    }
    
    public function update_tratamientos_paciente($data2,$data){        
            $this->db->where('id', $data2["idTrat"]);    
            $resul = $this->db->update('trat_paciente', $data); 	
            
             if($this->db->affected_rows()){
                 return array('valido'=>true,
                               'id' => $data2["idTrat"] );
              }else return array('valido'=>false,'id' => $data2["idTrat"]);
    }
}