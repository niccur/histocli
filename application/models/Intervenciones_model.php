<?php

class Intervenciones_model extends CI_Model{
   
	
     function __construct() 
    {
        parent::__construct();	
	    $this->load->database();
    }	
    
   
    public function load_intervenciones(){
        $this->db->select("a.id id_intervencion,
                        a.nombre,
                        a.descrip,
                        b.nombre departamento,
                        b.id id_departamento", FALSE);      
        $this->db->from("intervencion a,departamento b");
		$this->db->where("a.id_departamento = b.id");
        $this->db->order_by("b.nombre,a.nombre");
        $query = $this->db->get();		
	return $query->result();         
    }	

    public function load_departamentos(){
        $this->db->select("id id_departamento,
                          nombre", FALSE);      
        $this->db->from("departamento ");
        $query = $this->db->get();		
	return $query->result();         
    }	

    
    public function load_tratamiento_det_precio($idDet){
        $this->db->select("c.id, c.id_intervencion, c.id_tratamiento_cab,a.nombre,a.descrip,
                        c.sesiones, c.estado,p.precio pesos_unit,d.precio dolares_unit,
                        desc_trat.porcentaje porc_desc_trat,
                        desc_trat.nombre nom_desc_trat,                        
                        desc_trat.descripcion desc_desc_trat,
                        date_format(desc_trat.fecha_desde,'%Y-%m-%d') fdesde_desc_trat,
                        date_format(desc_trat.fecha_hasta,'%Y-%m-%d') fhasta_desc_trat,
                        desc_intrv.porcentaje porc_desc_intrv,
                        desc_intrv.nombre nom_desc_intrv,                        
                        desc_intrv.descripcion desc_desc_intrv,
                        date_format(desc_intrv.fecha_desde,'%Y-%m-%d') fdesde_desc_intrv,
                        date_format(desc_intrv.fecha_hasta,'%Y-%m-%d') fhasta_desc_intrv
                        ", FALSE);      
        $this->db->from("intervencion a,tratamiento_det c");
        $this->db->join("(select a.precio, a.id_intervencion
                        from precios a
                        where a.moneda = 0 and
                       a.vigencia = (Select max(x.vigencia)
                                      from precios x
                                      where x.id_intervencion = a.id_intervencion and
                                          x.moneda = a.moneda and
                                          date_format(x.vigencia,'%Y-%m-%d') <= date_format(sysdate(),'%Y-%m-%d')
                                      )
                        ) p", "c.id_intervencion = p.id_intervencion", "left outer");	
        $this->db->join("(select a.precio, a.id_intervencion
                        from precios a
                        where a.moneda = 1 and
                                   a.vigencia = (Select max(x.vigencia)
                                                  from precios x
                                                  where x.id_intervencion = a.id_intervencion and
                                                      x.moneda = a.moneda and
                                                      date_format(x.vigencia,'%Y-%m-%d') <= date_format(sysdate(),'%Y-%m-%d')
                                                         )
                        ) d", "c.id_intervencion = d.id_intervencion", "left outer");        
        $this->db->join("(select a.id_tratamiento_cab,
                                a.nombre,
                                a.porcentaje ,
                                a.descripcion,
                                a.fecha_desde,
                                a.fecha_hasta
                        from descuentos a
                        where   a.id_intervencion Is Null and	
                                a.estado = 0 and
                                a.id_paciente Is Null and
                                a.id_producto Is Null and
                                date_format(sysdate(),'%Y-%m-%d') between date_format(a.fecha_desde,'%Y-%m-%d') and date_format(a.fecha_hasta ,'%Y-%m-%d') ) desc_trat", 
                           "c.id_tratamiento_cab = desc_trat.id_tratamiento_cab", "left outer");
        $this->db->join("(select a.id_intervencion,
                                a.nombre,
                                a.porcentaje ,
                                a.descripcion,
                                a.fecha_desde,
                                a.fecha_hasta
                        from descuentos a
                        where a.id_tratamiento_cab Is Null and
                                a.estado = 0 and
                                a.id_paciente Is Null and
                                a.id_producto Is Null and
                                date_format(sysdate(),'%Y-%m-%d') between date_format(a.fecha_desde,'%Y-%m-%d') and date_format(a.fecha_hasta ,'%Y-%m-%d')) desc_intrv", 
                           "c.id_intervencion = desc_intrv.id_intervencion", "left outer");

        $this->db->where("c.id = ".$idDet." and a.id = c.id_intervencion");	
        $this->db->order_by("c.id asc");
        $query = $this->db->get();		
	return $query->row();  
    }
}