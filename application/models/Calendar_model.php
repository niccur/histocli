<?php

class Calendar_model extends CI_Model{
   
	
     function __construct() 
    {
        parent::__construct();	
	    $this->load->database();
    }
	
	
	
    public function get_ColorProf($id)
    {
		
        $this->db->select("id,color", FALSE );                                          

        $this->db->where('id =', $id);  
        $query = $this->db->get('profesional');

        return $query->row('color');
    }	

    public function get_paciente($id)
    {
		
        $this->db->select("a.nombre, a.apellido, a.id as idPaciente, IFNULL(a.email,'Sin Email') email,
				  								CASE a.sexo
				    									WHEN 'F' THEN IFNULL(b.nombre,'female_profile.jpg')
				    									WHEN 'M' THEN IFNULL(b.nombre,'male_profile.jpg')
				    									ELSE 'profilex.jpg'
				  								END as foto", FALSE);

		$this->db->from("paciente as a");
		$this->db->where("a.id = ".$id);
        $this->db->join('fotos as b', 'a.id_foto = b.id', 'left outer');
        $query = $this->db->get();		
		return $query->row(); 
    }	    
    
    public function validar_Sobreturno($data)
    {
            $fdesde = $data['start'];
            $fhasta = $data['end'];
            $idProf = $data['id_prof'];
            $id = $data['id'];

            $where = 'id_prof = '.$idProf.' and ocupado = "true" and id != '.$id.' and 
                            ((start <= "'.$fdesde.'" and end >= "'.$fhasta.'") or
                            (start <= "'.$fdesde.'" and end >= "'.$fdesde.'") or
                            (start between "'.$fdesde.'" and "'.$fhasta.'" )) and
                            ("'.$fdesde.'" != end and "'.$fhasta.'" != start)'; 

            $this->db->where($where);  
            $this->db->from('evenement');

            $solapamiento = $this->db->count_all_results();

            if($solapamiento > 0){ 
                    return true;
                    }else{
                        return false;
              } 		

    }	
    
   public function get_EstadosTurnos()
    {
		
        $this->db->select("id,nombre", FALSE );                                          
        $this->db->order_by("id"); 
        $query = $this->db->get('estadosturnos');

        return $query->result();
    }	
    
    public function get_Pacientes()
    {
		
        $this->db->select("a.nombre, a.apellido, a.id as idPaciente, IFNULL(a.email,'Sin Email') email,
				  								CASE a.sexo
				    									WHEN 'F' THEN IFNULL(b.nombre,'female_profile.jpg')
				    									WHEN 'M' THEN IFNULL(b.nombre,'male_profile.jpg')
				    									ELSE 'profilex.jpg'
				  								END as foto", FALSE);
        $this->db->order_by("a.nombre, a.apellido"); 
        $this->db->from("paciente as a");
        $this->db->join('fotos as b', 'a.id_foto = b.id', 'left outer');
				$query = $this->db->get();
		
				return $query->result();
    }	
    
        
    public function get_Prof()
    {		
        $this->db->select("concat(IFNULL(a.tratamiento,''),IF(a.tratamiento IS NULL,'',' '),a.nombre,' ',a.apellido) as nombre, a.id as idProf, a.color,IFNULL(a.email,'Sin Email') email,
											        CASE a.sexo
															    WHEN 'F' THEN IFNULL(b.nombre,'female_profile.jpg')
															    WHEN 'M' THEN IFNULL(b.nombre,'male_profile.jpg')
															    ELSE 'profilex.jpg'
															END as foto, medico", FALSE );
		    $this->db->from("profesional as a");
        $this->db->join('fotos as b', 'a.id_foto = b.id', 'left outer');
        $this->db->order_by("a.medico desc,a.nombre asc"); 
				$query = $this->db->get();
				
				
				return $query->result();
    }	
	
    public function get_Turnos($data)
	{
			if(!$data['sele']){
				$ss = '0';
                        }else $ss = $data['sele'];
                        
//concat(IFNULL(b.tratamiento,''),IF(b.tratamiento IS NULL,'',' '),b.nombre,' ',b.apellido) as nombre", FALSE ); 
                        
               $this->db->select("a.id, 
                                  concat(c.nombre,' ',c.apellido) as title,
                                  DATE_FORMAT(a.start, '%Y-%m-%dT%H:%i:%s') AS start,
                                  DATE_FORMAT(a.end, '%Y-%m-%dT%H:%i:%s') AS end,
                                  a.editable,
                                  a.ocupado,
                                  b.color,
                                  a.id_profesional,
                                  a.id_paciente,
                                  a.id_estadoturno,
                                  a.descrip", FALSE);		

		$where = 'a.id_profesional in('.$ss.') and b.id = a.id_profesional and '
                        .'(c.id= '.$data['idPaciente'].' or 2 = '.$data['ver_paciente'].') and '
                        . 'c.id = a.id_paciente and '
                        . 'a.id_estadoturno = '.$data['estado'].' and '
                        .'((a.start <= "'.$data['start'].'" and a.end >= "'.$data['end'].'") or '
                        .'(a.start <= "'.$data['start'].'" and a.end  >= "'.$data['start'].'") or '
                        .'(a.start between "'.$data['start'].'" and "'.$data['end'].'" ))'
                        .'';
					 				
		$this->db->where($where);  		   
		$this->db->order_by("id"); 
		$query = $this->db->get('turno as a, profesional as b, paciente as c');
		
		return $query->result();
    }

        public function get_Agendas($data)
	{
			if(!$data['sele']){
				$ss = '0';
                        }else $ss = $data['sele'];
                                                
               $this->db->select("a.id, 
                                  concat(IFNULL(b.tratamiento,''),IF(b.tratamiento IS NULL,'',' '),b.nombre,' ',b.apellido) as title,
                                  DATE_FORMAT(a.start, '%Y-%m-%dT%H:%i:%s') AS start,
                                  DATE_FORMAT(a.end, '%Y-%m-%dT%H:%i:%s') AS end,
                                  a.editable,
                                  a.ocupado,
                                  b.color,
                                  a.id_profesional,
                                  a.descrip",FALSE);
					
			$where = 'a.id_profesional in('.$ss.') and b.id = a.id_profesional and
					 ((a.start <= "'.$data['start'].'" and a.end >= "'.$data['end'].'") or
					(a.start <= "'.$data['start'].'" and a.end  >= "'.$data['start'].'") or
					(a.start between "'.$data['start'].'" and "'.$data['end'].'" )) 	
					 ';
							
		$this->db->where($where);  		   
		$this->db->order_by("id"); 
		$query = $this->db->get('agenda as a, profesional as b');
		
		return $query->result();
    }

    
    
    public function add_Turno($data,$tratamientos)
    {
		//array es de objetos con flechita $data->start;
		//array simple con corchetes corchetes  $data['start'];	

              
        $insert_id = null;
	$this->db->trans_start();
        $this->db->insert('turno',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
          
         if($insert_id != null){
            $arrayUpdate = array('id_turno' => $insert_id);
            $this->db->where_in("id",$tratamientos);
            $this->db->update("trat_paciente",$arrayUpdate);            
            return array('valido'=>true,
                           'id' => $insert_id );
          }else return array('valido'=>false,'id' => 0);
              
              
    }
	
    public function update_Turno($update,$id)
    {

              $this->db->where('id', $id);    
              $this->db->update('turno', $update); 	
					  
    }    
   
    public function add_Agenda($data)
    {
		//array es de objetos con flechita $data->start;
		//array simple con corchetes corchetes  $data['start'];
	
              $result = $this->db->insert('agenda', $data);

              if($result){
                  return array('valido'=>true);
              }else return array('valido'=>false);
              
	
    }
	
    public function update_Agenda($update,$id)
    {

              $this->db->where('id', $id);    
              $this->db->update('agenda', $update); 	
					  
    }       
}
