<?php

class Usuario_model extends CI_Model{
	
     function __construct() 
	{
        parent::__construct();	
	    $this->load->database();
    }
	
    public function validarUsuario($username, $clave)
	{
        $this->db->from('usuarios');
        $this->db->where('username', $username);
        $this->db->where('clave', sha1($clave));
		//$this->db->where('clave', $clave);
        $usuario = $this->db->get()->row();
        if (empty($usuario)){	
            $this->session->sess_destroy();
            return array('valido'=>false);
        }else{
            $this->session->set_userdata('usuario', $usuario);
            //$_SESSION["usuario"]=$usuario;
/*
            $this->session->set_userdata('usuario', $usuario);
            $this->session->set_userdata('username', );
            $this->session->set_userdata('acceso', $usuario['acceso']);
*/            
            return array('valido'=>true,
                        'estado' => $usuario->estado);
        }
    }
		
    function actualizar($old,$new) {
         
        $this->db->from('usuarios');
        $this->db->where('username', $this->session->userdata('username'));
        $this->db->where('clave', sha1($old));
	    //$this->db->where('clave', $old);
        $usuario = $this->db->get()->row();
		
        if (empty($usuario)){	
			
			$resultado['valido'] = false ;     
        } 
		else 
		{	
			$usuario->clave = sha1($new);			
			$this->db->where('username', $usuario->username);
			$this->db->where('clave', sha1($old));
			//$this->db->where('clave', $old);
            $this->db->update('usuarios', $usuario);
            $resultado['valido'] =true ;        
		}         
         
         return $resultado;             
    }
}
