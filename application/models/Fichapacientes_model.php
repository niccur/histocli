<?php

class Fichapacientes_model extends CI_Model{
   
	
     function __construct() 
    {
        parent::__construct();	
	    $this->load->database();
    }
	
	
	
     public function get_pacientes_paging($start,$pageing) {
         
        $query = $this->db->query("select SQL_CALC_FOUND_ROWS dni, nombre, apellido, sexo, fechanacimiento 
                                   from socio LIMIT ".$start.", ".$pageing,FALSE);	
				
        $resul = $query->result();
        
        $sql = $this->db->query("SELECT FOUND_ROWS() AS found_rows",FALSE);
        $total_rows = $sql->row('found_rows');
         
        $data[] = array( 
            'TotalRows' => $total_rows,
	    'Rows' => $resul
	);
        
        return $data; 
    }
    
    public function get_paciente_data($idPaciente){
        $this->db->select("a.id as idPaciente, 
                            a.nombre, 
                           a.apellido, 
                            a.telefono,
                           date_format(a.fecha_nac,'%d/%m/%Y') fecha_nac,
                           a.notas,
                           IFNULL(a.email,'Sin Email') email,
                           CASE a.sexo
				WHEN 'F' THEN IFNULL(b.nombre,'female_profile.jpg')
                                WHEN 'M' THEN IFNULL(b.nombre,'male_profile.jpg')
				ELSE 'profilex.jpg'
			   END as foto", FALSE);
        $this->db->from("paciente as a");
        $this->db->join('fotos as b', 'a.id_foto = b.id', 'left outer');
        $this->db->where('a.id = '.$idPaciente);
	$query = $this->db->get();		
	return $query->row();  
    }
    
    public function get_Pacientes()
    {
		
        $this->db->select("a.id as idPaciente, 
                            a.nombre, 
                           a.apellido, 
                            a.telefono,
                           date_format(a.fecha_nac,'%d/%m/%Y') fecha_nac,
                           a.notas,
                           IFNULL(a.email,'Sin Email') email,
                           CASE a.sexo
				WHEN 'F' THEN IFNULL(b.nombre,'female_profile.jpg')
                                WHEN 'M' THEN IFNULL(b.nombre,'male_profile.jpg')
				ELSE 'profilex.jpg'
			   END as foto, ranking", FALSE);
        $this->db->order_by("a.nombre, a.apellido"); 
        $this->db->from("paciente as a");
        $this->db->join('fotos as b', 'a.id_foto = b.id', 'left outer');
	$query = $this->db->get();
		
        return $query->result();
    }
    
     

//     public function get_pacientes() {
//         
//        $query = $this->db->query("select a.id idPaciente, 
//                                         a.nombre, 
//                                         a.apellido, 
//                                         if(a.sexo = 'F','FEMENINO','MASCULINO') sexo, 
//                                         a.fecha_nac,
//                                         a.celular,
//                                         a.telefono,
//                                         a.email,
//					 a.notas,
//                                         if(a.sexo = 'F',IFNULL(b.ruta,'female_profile.JPG'),IFNULL(b.ruta,'male_profile.JPG')) fotoPerfil,
//                                         null vitales,
//                                         null fotos
//                        from paciente a LEFT JOIN fotos b on a.id_foto = b.id
//                        order by a.apellido, a.nombre",FALSE);	
//													
//        $pacientes = $query->result();
//		
//        if($pacientes){
//                    foreach ($pacientes as $row) {
//                            $resul = $this->db->query("select date_format(fecha,'%d/%m/%Y') fecha, 
//                                                              IFNULL(bmi,'') bmi,
//                                                              IFNULL(freqcardiaca,'') freqcardiaca,
//                                                              IFNULL(freqrespiratoria,'') freqrespiratoria,
//                                                              IFNULL(peso,'') peso,
//                                                              IFNULL(talla,'') talla,
//                                                              IFNULL(tarterialdiastolica,'') tarterialdiastolica,
//                                                              IFNULL(tarterialsistolica,'') tarterialsistolica
//                                                    from signosvitales
//                                                    where id_paciente = ".$row->idPaciente,FALSE);	
//				
//                            $signos = $resul->result();				
//                            $row->vitales = $signos;	
//
////                            $resul = $this->db->query("select date_format(fecha,'%d/%m/%Y') fecha, 
////                                                              ruta,
////                                                              IFNULL(nombre,'') nombre,
////                                                              IFNULL(descripcion,'') descripcion
////                                                    from fotos
////                                                    where id_paciente = ".$row->idPaciente,FALSE);	
////				
////				$fotos = $resul->result();				
////				if($fotos){
////				
////					$i = 0;
////					$c = 0;
////					foreach ($fotos as $rowFoto) {
////						$a[] = array('fecha' => $rowFoto->fecha, 'ruta' => $rowFoto.ruta, 'nombre' => $rowFoto.nombre, 'descripcion' => $rowFoto.descripcion);
////						$c = $c + 1;	
////						if($c == 2) {
////							$i = $i + 1;
////							$c = 0;
////						}
////					}
////					$row->fotos = $a;	
////				}else $row->fotos = null;
//			};
//		};
//		
//        return $pacientes;
//    }	
    
     public function get_tratamientos($paciente) {
         
        $query = $this->db->query("select a.id,
                                        a.nombre,
                                        date_format(b.start,'%d/%m/%Y') fecha,
                                        concat(c.nombre,' ',c.apellido) profesional,
                                        IFNULL(a.descrip_intervencion,'') descrip_intervencion,
                                        IFNULL(a.descrip_preliminar,'') descrip_preliminar,
                                        d.nombre intervencion,
                                        e.nombre departamento,
                                        null diagnosticos
                        from  turno b,
                                  profesional c,
                                  intervencion d RIGHT JOIN tratamiento a ON d.id = a.id_intervencion,
                                  departamento e
                        where b.id = a.id_turno	and
                                        b.id_paciente = ".$paciente." and
                                        c.id = a.id_profesional and
                                        e.id = a.id_departamento
                        order by b.start",FALSE);	
				
        $tratamientos = $query->result();
		
		if($tratamientos){
			foreach ($tratamientos as $row) {
                            $query = $this->db->query("select b.nombre
                                                        from diagnosticogrupo a,
                                                                  diagnostico b
                                                        where a.id_tratamiento = ".$row->id." and
                                                                        b.id = a.id_diagnostico",FALSE);	
				
				$diagnosticos = $query->result();
				
				$row->diagnosticos = $diagnosticos;			
			};
		};

        return $tratamientos;
    }	
    public function eliminar_histo($idHisto){
        //ELIMINAR FOTOS
        $targetFileFoto = '/histocli/resources/fotos/';
        $targetFileFoto = $_SERVER['DOCUMENT_ROOT'] . $targetFileFoto;
        $targetFileThumb = '/histocli/resources/fotos/thumbnail/';
        $targetFileThumb = $_SERVER['DOCUMENT_ROOT'] . $targetFileThumb;

        $query = $this->db->query("select nombre from fotos where id_histo = ".$idHisto);
        $fotos = $query->result();

        foreach($fotos as $foto){
            if (file_exists($targetFileFoto . $foto->nombre)) 
                unlink($targetFileFoto . $foto->nombre);                     
            if (file_exists($targetFileThumb . $foto->nombre)) 
                unlink($targetFileThumb . $foto->nombre);                                   
        };
        $this->db->where('id_histo',$idHisto);
        $this->db->delete('fotos');
        // --------------------------------

        $this->db->where('id',$idHisto);
        $this->db->delete('histo');

        $this->db->where('id_histo',$idHisto);
        $this->db->delete('trat_paciente');

        if ($this->db->error())
         return array('valido'=>false);

    }
    public function eliminar_paciente($idPaciente){

        //eliminar historia clinica y sus fotos
        $query = $this->db->query("select id from histo where id_paciente = ".$idPaciente);
        $histo = $query->result();

        foreach($histo as $valor){
            $this->eliminar_histo($valor->id);              
        };

        $this->db->where('id_paciente',$idPaciente);
        $this->db->delete('anamnesis');
        // if ($this->db->error()) 
        //     return array('valido'=>1);

        $this->db->where('id_paciente',$idPaciente);
        $this->db->delete('medidas');
        // if ($this->db->error()) 
        // return array('valido'=>2);

        $this->db->where('id_paciente',$idPaciente);
        $this->db->delete('turno');
        // if ($this->db->error()) 
        // return array('valido'=>3);

        $this->db->where('id_paciente',$idPaciente);
        $this->db->delete('trat_paciente');
        // if ($this->db->error()) 
        // return array('valido'=>4);        

        $this->db->where('id',$idPaciente);
        $this->db->delete('paciente');

        if (!$this->db->affected_rows()) {
            return array('valido'=>0);
        } else {
            return array('valido'=>7);
        }

    }
 
}
