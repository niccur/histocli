<?php

class Histo_model extends CI_Model{
   
	
     function __construct() 
    {
        parent::__construct();	
	    $this->load->database();
    }
	public function get_fotos_histo($idHisto){
       
        $query = $this->db->query("select nombre as nombreFoto from fotos where id_histo = ".$idHisto);
        $fotos = $query->result();
        return $fotos;
    }
    public function eliminar_foto_hc($idHisto,$nombreFoto){
        $targetFileFoto = '/histocli/resources/fotos/';
        $targetFileFoto = $_SERVER['DOCUMENT_ROOT'] . $targetFileFoto;
        $targetFileThumb = '/histocli/resources/fotos/thumbnail/';
        $targetFileThumb = $_SERVER['DOCUMENT_ROOT'] . $targetFileThumb;

        $this->db->where("id_histo = ".$idHisto." and nombre ='".$nombreFoto."'");
        $this->db->delete('fotos');

        if (file_exists($targetFileFoto . $nombreFoto)) 
                unlink($targetFileFoto . $nombreFoto);                     
        if (file_exists($targetFileThumb . $nombreFoto)) 
                unlink($targetFileThumb . $nombreFoto);

            if (!$this->db->affected_rows()) {
                return array('valido'=>false);
            } else {
                return array('valido'=>true);
            }
    }
    public function eliminar_histo($idHisto){
        //ELIMINAR FOTOS
        $targetFileFoto = '/histocli/resources/fotos/';
        $targetFileFoto = $_SERVER['DOCUMENT_ROOT'] . $targetFileFoto;
        $targetFileThumb = '/histocli/resources/fotos/thumbnail/';
        $targetFileThumb = $_SERVER['DOCUMENT_ROOT'] . $targetFileThumb;

        $query = $this->db->query("select nombre from fotos where id_histo = ".$idHisto);
        $fotos = $query->result();

        foreach($fotos as $foto){
            if (file_exists($targetFileFoto . $foto->nombre)) 
                unlink($targetFileFoto . $foto->nombre);                     
            if (file_exists($targetFileThumb . $foto->nombre)) 
                unlink($targetFileThumb . $foto->nombre);                                   
        };
        $this->db->where('id_histo',$idHisto);
        $this->db->delete('fotos');
        // --------------------------------

        $this->db->where('id in (select id_turno from histo where id = '.$idHisto.')');
        $this->db->delete('turno');
        
        $this->db->where('id_histo',$idHisto);
        $this->db->delete('trat_paciente');

        $this->db->where('id',$idHisto);
        $this->db->delete('histo');

        if (!$this->db->affected_rows()) {
            return array('valido'=>false);
        } else {
            return array('valido'=>true);
        }

    }
    public function get_anamnesis($idPaciente){
        
        $this->db->select("id,
                           id_paciente as idPaciente,
                           patologicos,
                           rm,
                           fisica,
                           alimentario,
                           tabaco,
                           alcohol,
                           sueno,
                           alergias,
                           observaciones,
                           sol
                          ",FALSE);
        $this->db->from("anamnesis");
        $this->db->where("id_paciente = ".$idPaciente);
        $query = $this->db->get();		
	return $query->row();      
        
    }
    
    public function get_medidas($idPaciente){
        $this->db->select("id, id_paciente,
                           date_format(fecha,'%d/%m/%Y') fecha,
                           brazo,busto,cintura,cadera_alta,cadera_baja,
                           gluteos,muslo_superior,muslo_inferior,rodilla,
                           pantorrilla,tobillo", FALSE);
        $this->db->order_by("fecha"); 
        $this->db->from("medidas");
        $this->db->where("id_paciente = ".$idPaciente);
        $query = $this->db->get();
	return $query->result();        
        
    }
    
    public function load_medida($idMedida){
        $this->db->select("id, id_paciente,
                           date_format(fecha,'%d/%m/%Y') fecha,
                           brazo,busto,cintura,cadera_alta,cadera_baja,
                           gluteos,muslo_superior,muslo_inferior,rodilla,
                           pantorrilla,tobillo", FALSE);      
        $this->db->from("medidas");
        $this->db->where("id = ".$idMedida);
        $query = $this->db->get();		
	return $query->row();         
    }
           
    public function get_filiatorio($idPaciente){
		
        $this->db->select("a.id as idPaciente, 
                           a.nombre, 
                           a.apellido, 
                           a.telefono,
                           a.celular,
                           date_format(a.fecha_nac,'%d/%m/%Y') fecha_nac,
                           date_format(a.fecha_ingreso,'%d/%m/%Y') fecha_ingreso,
                           a.notas,
                           a.email,
                           a.sexo,
                           IFNULL(a.edad,0) edad,
                           a.ocupacion,
                           a.direccion,
                           a.codpostal,
                           a.obrasocial,
                           a.nro_afiliado,
                           a.dni,
                           IFNULL(a.hijos,0) hijos,
                           CASE a.sexo
				WHEN 'F' THEN IFNULL(b.nombre,'female_profile.jpg')
                                WHEN 'M' THEN IFNULL(b.nombre,'male_profile.jpg')
				ELSE 'profilex.jpg'
			   END as foto", FALSE);
        $this->db->order_by("a.nombre, a.apellido"); 
        $this->db->from("paciente as a");
        $this->db->where("a.id = ".$idPaciente);
        $this->db->join('fotos as b', 'a.id_foto = b.id', 'left outer');
        $query = $this->db->get();
		
	return $query->row(); 
    }   
    
    public function update_filiatorio($data,$id)
        {

            $this->db->where('id', $id);    
            $resul = $this->db->update('paciente', $data); 	
            
             if($resul){
                return array('valido'=>true,
                               'id' => $id );
              }else return array('valido'=>false,'id' => $id);
        }   
    public function add_Filiatorio($data)
    {
		//array es de objetos con flechita $data->start;
		//array simple con corchetes corchetes  $data['start'];
        $insert_id = null;
	$this->db->trans_start();
        $this->db->insert('paciente',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
          
         if($insert_id != null){
            return array('valido'=>true,
                           'id' => $insert_id );
          }else return array('valido'=>false,'id' => 0);
	
    }  
    
    public function update_anamnesis($data,$id)
        {

            $this->db->where('id_paciente', $id);    
            $resul = $this->db->update('anamnesis', $data); 	
            
             if($resul){
                return array('valido'=>true,
                               'id' => $id );
              }else return array('valido'=>false,'id' => $id);
        }  
    public function asignar_fotos_histo($fotos,$idHisto,$idPaciente){
            $result = false;
            $repetidas = array();
            $conError = array();

            foreach ($fotos as $foto) {
                $this->db->where("nombre = '".$foto."'");
                $query = $this->db->get("fotos");
                $tot = $query->num_rows();

                if($tot == 0){
                    $insertar_foto = array('nombre' =>$foto,
                                            'id_paciente' => $idPaciente,
                                            'id_histo' => $idHisto
                                            );
                    $result = $this->db->insert('fotos', $insertar_foto);
                    if(!$result){
                         array_push($conError,$foto); //se producjo un error al intentar guardar la foto
                    }
                }else array_push($repetidas,$foto); //esta foto ya existe y no se insertará
            }   
            if(count($conError)==0){ //sin error
                return array('valido'=>true,'repetidas'=>$repetidas,'conError'=>$conError);
            }else return array('valido'=>false,'repetidas'=>$repetidas,'conError'=>$conError);
    }
    public function add_histo($data,$fotos,$idTrat){
        $result = false;
        $repetidas = array();
        $conError = array();

        $insert_id = null;
	    $this->db->trans_start();
        $this->db->insert('histo',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        if($insert_id != null){
            if($fotos){
                    foreach ($fotos as $foto) {
                        $this->db->where("nombre = '".$foto."'");
                        $query = $this->db->get("fotos");
                        $tot = $query->num_rows();

                        if($tot == 0){
                            $insertar_foto = array('nombre' =>$foto,
                                                    'id_paciente' => $data['id_paciente'],
                                                    'id_histo' => $insert_id
                                                    );
                            $result = $this->db->insert('fotos', $insertar_foto);
                            if(!$result){ //se producjo un error al intentar guardar la foto
                                array_push($conError,$foto);
                           }
                        }else array_push($repetidas,$foto); //esta foto ya existe y no se insertará
                    }
                } 
                //asignar la historia al tratamiento si es que la historia se cargo desde la pantalla de turnos
                if($idTrat != 0){
                    $this->db->where('id', $idTrat);    
                    $resul = $this->db->update('trat_paciente', array('id_histo'=>$insert_id)); 	
                }

                //aca hay que mostrar las fotos con error o repetidas en la view si es que las hubiera
                return array('valido'=>true,'repetidas'=>$repetidas,'id' => $insert_id,'conError'=>$conError);
            }else return array('valido'=>false,'repetidas'=>$repetidas,'id' => $insert_id,'conError'=>$conError);
    }

    public function add_anamnesis($data)
    {
		//array es de objetos con flechita $data->start;
		//array simple con corchetes corchetes  $data['start'];
        $insert_id = null;
	$this->db->trans_start();
        $this->db->insert('anamnesis',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         
//aca devolver el id del paciente 
         if($insert_id != null){
            return array('valido'=>true,
                           'id' => $insert_id );
          }else return array('valido'=>false,'id' => 0);
	
    } 
    public function add_medida($data)
    {
		//array es de objetos con flechita $data->start;
		//array simple con corchetes corchetes  $data['start'];
        $insert_id = null;
	$this->db->trans_start();
        $this->db->insert('medidas',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
          
         if($insert_id != null){
            return array('valido'=>true,
                           'id' => $insert_id );
          }else return array('valido'=>false,'id' => 0);
	
    } 
    public function update_medida($data,$id)
        {

            $this->db->where('id', $id);    
            $resul = $this->db->update('medidas', $data); 	
            
             if($resul){
                return array('valido'=>true,
                               'id' => $id );
              }else return array('valido'=>false,'id' => $id);
        }  
    
    public function update_histo($data,$id)
        {

            $this->db->where('id', $id);    
            $resul = $this->db->update('histo', $data); 	
            
             if($resul){
                return array('valido'=>true,
                               'id' => $id );
              }else return array('valido'=>false,'id' => $id);
        }          
        
    public function elimina_medida($data){
        $this->db->delete('medidas',$data); 
        
        if (!$this->db->affected_rows()) {
            return array('valido'=>false);
        } else {
            return array('valido'=>true);
        }
    }
    public function get_histo($id_paciente){
        $this->db->select("a.id,"
                         . "date_format(a.fecha,'%d/%m/%Y') fecha,"
                         . "a.descrip_intervencion,"
                         . "b.nombre profesional,"
                         . "a.id_profesional",false);
        $this->db->from("histo as a, profesional as b");
        $this->db->where("a.id_paciente = ".$id_paciente." and "
                       . "b.id = a.id_profesional");
        $this->db->order_by("a.fecha");
        $query = $this->db->get();	
	    $histo = $query->result();         
        return $histo;
        //return $this->db->last_query();	--mostrar como arma la consulta
    }
    public function get_histo_id($id_histo){
        $this->db->select("id, id_profesional,
                           date_format(fecha,'%d/%m/%Y') fecha,
                           descrip_intervencion,tratamiento,medicacion,
                           diagnostico", FALSE);      
        $this->db->from("histo");
        $this->db->where("id = ".$id_histo);
        $query = $this->db->get();		
	return $query->row();    
        //return $this->db->last_query();	--mostrar como arma la consulta
    }

}
