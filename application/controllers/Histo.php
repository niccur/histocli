<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Histo extends CI_Controller {
    
    function __construct() 
    {
		parent::__construct();		
		$this->load->library('session');				
		$this->load->helper('url');
        $this->load->model('Histo_model');
        $this->load->model('Fichapacientes_model');
		
		$user_logged = $this->session->userdata("usuario");
		if (isset($user_logged) and $user_logged!='') {							
		} else {
			redirect("login");
		}
		
    }
    
    function get_anamnesis(){
        $id = $this->input->post('idPaciente');  
        
        $anamnesis = $this->Histo_model->get_anamnesis($id); 
        echo json_encode($anamnesis); 
	 
    }
    function eliminar_foto_histo(){
        $idHisto = $this->input->post('idHisto');  
        $fotoNombre = $this->input->post('fotoNombre');
        $resultado = $this->Histo_model->eliminar_foto_hc($idHisto,$fotoNombre); 
       
        return json_encode($resultado);
  
    }
    function eliminar_histo($idHisto){
        
        $resultado = $this->Histo_model->eliminar_histo($idHisto); 
        return json_encode($resultado);   
        
       //echo '<pre>';
        //print_r(json_encode($filiatorio));        
       //var_dump(json_encode($filiatorio));
       //exit;	        
    }
    function get_filiatorio(){
        $id = $this->input->post('idPacienteJs');  
        
        $filiatorio = $this->Histo_model->get_filiatorio($id); 
        echo json_encode($filiatorio);   
        
       //echo '<pre>';
        //print_r(json_encode($filiatorio));        
       //var_dump(json_encode($filiatorio));
       //exit;	        
    }
    function get_medidas($idPaciente){
                
        $medidas = $this->Histo_model->get_medidas($idPaciente); 
        echo json_encode($medidas); 	 
    } 
    function get_histo($idHisto){
                
        $histo = $this->Histo_model->get_histo_id($idHisto); 
        echo json_encode($histo); 	 
    } 
    function ver_fotos_historia_ci(){
        $data["idHisto"] = $this->input->post('idHisto');  
        $data["nombre"] = $this->input->post('nombre');
        $data["apellido"] = $this->input->post('apellido');

        $data["fecha"] = $this->input->post('fecha');  

        $rand = rand(1,1000000);
        $rand = 'X'.$rand;
        $data["clave"] = $rand;
            
        $dataSession[$rand] = $data;
        $this->session->set_userdata($dataSession);

        echo json_encode(array("clave_random"=>$rand));
    }
    function ver_fotos_historia($clave){
        $data =  $this->session->userdata($clave); 
   
        $this->load->view('encabezado');
        $this->load->view('fotos_histo',$data);   
    }

     function load_histo($clave)
	{   
            $data =  $this->session->userdata($clave);        
	
            $paciente = $this->Fichapacientes_model->get_paciente_data($data["idPaciente"]);          
            $data["idPaciente"] = $data["idPaciente"];
            $data["paciente"] = $paciente;
     
            $this->load->view('encabezado');
            $this->load->view('histo',$data);               
         }
          
    function histoCI(){
         
            $data["idPaciente"] = $this->input->post('idPaciente');  
            $data["nombre"] = $this->input->post('nombre');     
            $data['apellido'] = $this->input->post('apellido'); 
            $data['modo'] = $this->input->post('modo'); 
            $data['idHisto'] = $this->input->post('idHisto'); 
            $data['action'] = $this->input->post('action'); 
            
            $rand = rand(1,1000000);
            $rand = 'X'.$rand;
            $data["clave"] = $rand;
             
            $dataSession[$rand] = $data;
            $this->session->set_userdata($dataSession);
    
            echo json_encode(array("clave_random"=>$rand));
    } 

    function abm_histo($clave)
	{           
            $data =  $this->session->userdata($clave); 
            $data["prof"] = 0;
            $data["idTrat"] = 0;
            $data["descrip"] = '';

            
            $this->load->view('encabezado');
            $this->load->view('histoABM',$data);               
    }
    function load_fotos_histo($idHisto){
       // $idHisto = $this->input->post('idHisto');

        $resultado = $this->Histo_model->get_fotos_histo($idHisto);

        echo json_encode($resultado); 
    }
    function abm_histo_edit($clave)
	{           
            $data =  $this->session->userdata($clave); 

            $this->load->view('encabezado');
            $this->load->view('histoABMEdit',$data); 
    }
    function update_filiatorio()
    {
        $id = $this->input->post('idPaciente');                             
        $data['nombre'] = $this->input->post('nombre');
        $data['apellido'] = $this->input->post('apellido'); 
        $data['telefono'] = $this->input->post('telefono');
        $data['celular'] = $this->input->post('celular');
        $fecha_temp = $this->input->post('fecha_nac');
        $fecha_temp2 = $this->input->post('fecha_ingreso');
        if(strlen($fecha_temp) > 0 ){
           $fecha_temp = date_create_from_format('d/m/Y', $fecha_temp);
           $data['fecha_nac'] = date_format($fecha_temp, 'Y-m-d');
        }else $data['fecha_nac'] = null;
        if(strlen($fecha_temp2) > 0 ){
           $fecha_temp2 = date_create_from_format('d/m/Y', $fecha_temp2);
           $data['fecha_ingreso'] = date_format($fecha_temp2, 'Y-m-d');
        }else $data['fecha_ingreso'] = null;        
        $data['notas'] = $this->input->post('notas'); 
        $data['email'] = $this->input->post('email');
        $data['sexo'] = $this->input->post('sexo');	
        $data['edad'] = $this->input->post('edad');
        $data['ocupacion'] = $this->input->post('ocupacion'); 
        $data['direccion'] = $this->input->post('direccion');
        $data['codpostal'] = $this->input->post('codpostal');
        $data['hijos'] = $this->input->post('hijos');
        $data['obrasocial'] = $this->input->post('obra_social');
        $data['nro_afiliado'] = $this->input->post('nro_afiliado');
        $data['dni'] = $this->input->post('dni');
//       echo '<pre>';
//        var_dump($data);
//            exit;
	               
        $resultado = $this->Histo_model->update_filiatorio($data,$id); 
        echo json_encode($resultado);
    }
    function asignar_fotos_histo(){
        $idHisto = $this->input->post('idHisto');   
        $fotos = $this->input->post('fotos');   
        $idPaciente = $this->input->post('idPaciente');   

        $resultado = $this->Histo_model->asignar_fotos_histo($fotos,$idHisto,$idPaciente); 
        echo json_encode($resultado);
    }
    function update_histo(){
        $fecha_temp = $this->input->post('fecha');
        if(strlen($fecha_temp) > 0 ){
           $fecha_temp = date_create_from_format('d/m/Y', $fecha_temp);
           $data['fecha'] = date_format($fecha_temp, 'Y-m-d');
        }else $data['fecha'] = null;  
        $data['id_profesional'] = $this->input->post('profesional');
        $data['tratamiento'] = $this->input->post('tratamiento');
        $data['diagnostico'] = $this->input->post('diagnostico');
        $data['medicacion'] = $this->input->post('medicamentos');
        $data['descrip_intervencion'] = $this->input->post('intervencion');
        $idHisto = $this->input->post('idHisto');

        //$fotos['fotos'] = $this->input->post('fotos');

        $resultado = $this->Histo_model->update_histo($data,$idHisto); 
        echo json_encode($resultado); 
    }
    function add_histo(){
        $fecha_temp = $this->input->post('fecha');
        $idTrat = $this->input->post('idTrat');
        if($fecha_temp){
           $fecha_temp = date_create_from_format('d/m/Y', $fecha_temp);
           $data['fecha'] = date_format($fecha_temp, 'Y-m-d');
        }else $data['fecha'] = null;  
        $data['id_paciente'] = $this->input->post('idPaciente');
        $data['id_profesional'] = $this->input->post('profesional');
        $data['tratamiento'] = $this->input->post('tratamiento');
        $data['diagnostico'] = $this->input->post('diagnostico');
        $data['medicacion'] = $this->input->post('medicamentos');
        $data['descrip_intervencion'] = $this->input->post('intervencion');

        $fotos = array();
        $fotos = $this->input->post('fotos');

        
        $resultado = $this->Histo_model->add_histo($data,$fotos,$idTrat); 

        echo json_encode($resultado);
    }
    function add_filiatorio()
    {
        $data['nombre'] = $this->input->post('nombre');
        $data['apellido'] = $this->input->post('apellido'); 
        $data['telefono'] = $this->input->post('telefono');
        $data['celular'] = $this->input->post('celular');
        $fecha_temp = $this->input->post('fecha_nac');
        $fecha_temp2 = $this->input->post('fecha_ingreso');
        if(strlen($fecha_temp) > 0 ){
           $fecha_temp = date_create_from_format('d/m/Y', $fecha_temp);
           $data['fecha_nac'] = date_format($fecha_temp, 'Y-m-d');
        }else $data['fecha_nac'] = null;  
        if(strlen($fecha_temp2) > 0 ){
           $fecha_temp2 = date_create_from_format('d/m/Y', $fecha_temp2);
           $data['fecha_ingreso'] = date_format($fecha_temp2, 'Y-m-d');
        }else $data['fecha_ingreso'] = null;          
        $data['notas'] = $this->input->post('notas'); 
        $data['email'] = $this->input->post('email');
        $data['sexo'] = $this->input->post('sexo');	
        $data['edad'] = $this->input->post('edad');
        $data['ocupacion'] = $this->input->post('ocupacion'); 
        $data['direccion'] = $this->input->post('direccion');
        $data['codpostal'] = $this->input->post('codpostal');
        $data['hijos'] = $this->input->post('hijos');
        $data['obrasocial'] = $this->input->post('obra_social');
        $data['nro_afiliado'] = $this->input->post('nro_afiliado');
        $data['dni'] = $this->input->post('dni');
        $resultado = $this->Histo_model->add_filiatorio($data); 
        echo json_encode($resultado);
    }    

    function update_anamnesis(){
        $idPaciente = $this->input->post('idPaciente');
        $data['patologicos'] = $this->input->post('patologicos');
        $data['rm'] = $this->input->post('rm');
        $data['alimentario'] = $this->input->post('alimentario');
        $data['alergias'] = $this->input->post('alergias');
        $data['sueno'] = $this->input->post('sueno');
        $data['observaciones'] = $this->input->post('observaciones');
        $vfisica = $this->input->post('fisica');
        if(strlen($vfisica) > 0 ){
            $data['fisica'] = $vfisica;
        }else $data['fisica'] = null;
        $valcohol = $this->input->post('alcohol');
        if(strlen($valcohol) > 0 ){
            $data['alcohol'] = $valcohol;
        }else $data['alcohol'] = null;
        $vtabaco = $this->input->post('tabaco');
        if(strlen($vtabaco) > 0 ){
            $data['tabaco'] = $vtabaco;
        }else $data['tabaco'] = null; 
        $vsol = $this->input->post('sol');
        if(strlen($vsol) > 0 ){
            $data['sol'] = $vsol;
        }else $data['sol'] = null;
        $resultado = $this->Histo_model->update_anamnesis($data,$idPaciente); 
        echo json_encode($resultado);        
    }
    
    function add_anamnesis(){
        $data['id_paciente'] = $this->input->post('idPaciente');
        $data['patologicos'] = $this->input->post('patologicos');
        $data['rm'] = $this->input->post('rm');
        $data['alimentario'] = $this->input->post('alimentario');
        $data['alergias'] = $this->input->post('alergias');
        $data['sueno'] = $this->input->post('sueno');
        $data['observaciones'] = $this->input->post('observaciones');
        $vfisica = $this->input->post('fisica');
        if(strlen($vfisica) > 0 ){
            $data['fisica'] = $vfisica;
        }else $data['fisica'] = null;
        $valcohol = $this->input->post('alcohol');
        if(strlen($valcohol) > 0 ){
            $data['alcohol'] = $valcohol;
        }else $data['alcohol'] = null;
        $vtabaco = $this->input->post('tabaco');
        if(strlen($vtabaco) > 0 ){
            $data['tabaco'] = $vtabaco;
        }else $data['tabaco'] = null; 
        $vsol = $this->input->post('sol');
        if(strlen($vsol) > 0 ){
            $data['sol'] = $vsol;
        }else $data['sol'] = null;
        $resultado = $this->Histo_model->add_anamnesis($data); 
        echo json_encode($resultado);        
    }
    
    function add_medida(){
        $data['id_paciente'] = $this->input->post('idPaciente');
        $fecha = $this->input->post('fecha');         
        if(strlen($fecha) > 0 ){
           $fecha = date_create_from_format('d/m/Y', $fecha);
           $data['fecha'] = date_format($fecha, 'Y-m-d');
        }else $data['fecha'] = null; 
        
        $vbrazo = $this->input->post('brazo');
        if (strlen($vbrazo) > 0 && $vbrazo > 0) {            
            $data['brazo'] = str_replace(",", ".",$vbrazo);
        } else {
            $data['brazo'] = null;
        }
        $vbusto = $this->input->post('busto');
        if(strlen($vbusto) > 0 && $vbusto > 0){
            $data['busto'] = str_replace(",", ".",$vbusto);
        }else $data['busto'] = null;   
        $vcintura = $this->input->post('cintura');
        if(strlen($vcintura) > 0 && $vcintura > 0){
            $data['cintura'] = str_replace(",", ".",$vcintura);
        }else $data['cintura'] = null;   
        $vcadera_alta = $this->input->post('cadera_alta');
        if(strlen($vcadera_alta) > 0 && $vcadera_alta > 0){
            $data['cadera_alta'] = str_replace(",", ".",$vcadera_alta);
        }else $data['cadera_alta'] = null; 
        $vcadera_baja = $this->input->post('cadera_baja');
        if(strlen($vcadera_baja) > 0 && $vcadera_baja > 0){
            $data['cadera_baja'] = str_replace(",", ".",$vcadera_baja);
        }else $data['cadera_baja'] = null;  
        $vgluteos = $this->input->post('gluteos');
        if(strlen($vgluteos) > 0 && $vgluteos > 0){
            $data['gluteos'] = str_replace(",", ".",$vgluteos);
        }else $data['gluteos'] = null; 
        $vmuslo_superior = $this->input->post('muslo_superior');
        if(strlen($vmuslo_superior) > 0 && $vmuslo_superior > 0){
            $data['muslo_superior'] = str_replace(",", ".",$vmuslo_superior);
        }else $data['muslo_superior'] = null;
        $vmuslo_inferior = $this->input->post('muslo_inferior');
        if(strlen($vmuslo_inferior) > 0 && $vmuslo_inferior > 0){
            $data['muslo_inferior'] = str_replace(",", ".",$vmuslo_inferior);
        }else $data['muslo_inferior'] = null;
        $vrodilla = $this->input->post('rodilla');
        if(strlen($vrodilla) > 0 && $vrodilla > 0){
            $data['rodilla'] = str_replace(",", ".",$vrodilla);
        }else $data['rodilla'] = null;  
        $vpantorrilla = $this->input->post('pantorrilla');
        if(strlen($vpantorrilla) > 0 && $vpantorrilla > 0){
            $data['pantorrilla'] = str_replace(",", ".",$vpantorrilla);
        }else $data['pantorrilla'] = null;  
        $vtobillo = $this->input->post('tobillo');
        if(strlen($vtobillo) > 0 && $vtobillo > 0){
            $data['tobillo'] = str_replace(",", ".",$vtobillo);
        }else $data['tobillo'] = null;
        
        $resultado = $this->Histo_model->add_medida($data); 
        echo json_encode($resultado); 
    } 
    function update_medida(){
        $idMedida = $this->input->post('idMedida');        
        //$data['id_paciente'] = $this->input->post('idPaciente');
        
        $fecha = $this->input->post('fecha');         
        if(strlen($fecha) > 0 ){
           $fecha = date_create_from_format('d/m/Y', $fecha);
           $data['fecha'] = date_format($fecha, 'Y-m-d');
        }else $data['fecha'] = null; 
        
        $vbrazo = $this->input->post('brazo');
        if (strlen($vbrazo) > 0 && $vbrazo > 0) {            
            $data['brazo'] = str_replace(",", ".",$vbrazo);
        } else {
            $data['brazo'] = null;
        }
        $vbusto = $this->input->post('busto');
        if(strlen($vbusto) > 0 && $vbusto > 0){
            $data['busto'] = str_replace(",", ".",$vbusto);
        }else $data['busto'] = null;   
        $vcintura = $this->input->post('cintura');
        if(strlen($vcintura) > 0 && $vcintura > 0){
            $data['cintura'] = str_replace(",", ".",$vcintura);
        }else $data['cintura'] = null;   
        $vcadera_alta = $this->input->post('cadera_alta');
        if(strlen($vcadera_alta) > 0 && $vcadera_alta > 0){
            $data['cadera_alta'] = str_replace(",", ".",$vcadera_alta);
        }else $data['cadera_alta'] = null; 
        $vcadera_baja = $this->input->post('cadera_baja');
        if(strlen($vcadera_baja) > 0 && $vcadera_baja > 0){
            $data['cadera_baja'] = str_replace(",", ".",$vcadera_baja);
        }else $data['cadera_baja'] = null;  
        $vgluteos = $this->input->post('gluteos');
        if(strlen($vgluteos) > 0 && $vgluteos > 0){
            $data['gluteos'] = str_replace(",", ".",$vgluteos);
        }else $data['gluteos'] = null; 
        $vmuslo_superior = $this->input->post('muslo_superior');
        if(strlen($vmuslo_superior) > 0 && $vmuslo_superior > 0){
            $data['muslo_superior'] = str_replace(",", ".",$vmuslo_superior);
        }else $data['muslo_superior'] = null;
        $vmuslo_inferior = $this->input->post('muslo_inferior');
        if(strlen($vmuslo_inferior) > 0 && $vmuslo_inferior > 0){
            $data['muslo_inferior'] = str_replace(",", ".",$vmuslo_inferior);
        }else $data['muslo_inferior'] = null;
        $vrodilla = $this->input->post('rodilla');
        if(strlen($vrodilla) > 0 && $vrodilla > 0){
            $data['rodilla'] = str_replace(",", ".",$vrodilla);
        }else $data['rodilla'] = null;  
        $vpantorrilla = $this->input->post('pantorrilla');
        if(strlen($vpantorrilla) > 0 && $vpantorrilla > 0){
            $data['pantorrilla'] = str_replace(",", ".",$vpantorrilla);
        }else $data['pantorrilla'] = null;  
        $vtobillo = $this->input->post('tobillo');
        if(strlen($vtobillo) > 0 && $vtobillo > 0){
            $data['tobillo'] = str_replace(",", ".",$vtobillo);
        }else $data['tobillo'] = null;
        
        $resultado = $this->Histo_model->update_medida($data,$idMedida); 
        echo json_encode($resultado); 
    } 
    function elimina_medida(){
        $data['id'] = $this->input->post('idMedida');
        $resultado = $this->Histo_model->elimina_medida($data); 
        echo json_encode($resultado); 
    }
    function load_medida(){
        $idMedida = $this->input->post('idMedida');
        $resultado = $this->Histo_model->load_medida($idMedida); 
        echo json_encode($resultado); 
    }
    function load_tratamientos($idPaciente){
        $resultado = $this->Histo_model->get_histo($idPaciente); 
        echo json_encode($resultado);
    }
    function load_editar_medida()
	{   
            $this->load->view('encabezado');
            $this->load->view('editar_medidas');               
         }
}
?>