<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Google_Calendar extends CI_Controller {
    
    function __construct() 
    {
		parent::__construct();		
		$this->load->library('session');				
		$this->load->helper('url');
    
        $user_logged = $this->session->userdata("usuario");
		if (isset($user_logged) and $user_logged!='') {							
		} else {
			redirect("login");
		}
        require_once BASEPATH.'../vendor/autoload.php';
        $this->google_client = new Google\Client();
        $this->google_client->setClientId('507452867839-kh32ok72c1d8gj249su9r7t5s1h5qpg5.apps.googleusercontent.com'); //Define your ClientID
        $this->google_client->setClientSecret('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'); //Define your Client Secret Key
        $this->google_client->setRedirectUri(base_url().'Google_Calendar/login'); //Define your Redirect Uri
        $this->google_client->addScope(Google\Service\Calendar::CALENDAR);

    }
 
    function getApiKey()
{
        $file = base_url(). '.apiKey';
   
        return file_get_contents($file);
            
    
}
    
    /*-------------------------------------------------------------------*/
    // public function oauth() {
    //     $code = $this->input->get('code', true);

    //     $this->oauthLogin($code);
    //     redirect('http://localhost/histocli/main');
    // }
    // public function oauthLogin($code) {
    //     $login =$this->google_client->authenticate($code);

    //     if ($login) {
    //         $token =$this->google_client->getAccessToken();
    //         $this->session->set_userdata('access_token', $token);
    //         $this->session->set_userdata('is_authenticate_user', TRUE);

    //         return true;
    //     }
    // }

    function login()
    {
   
     if(isset($_GET["code"]))
     {
      $token = $this->google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

      if(!isset($token["error"]))
      {
       $this->google_client->setAccessToken($token['access_token']);
   
       $this->session->set_userdata('access_token', $token['access_token']);
   
       $google_service = new Google\Service\Oauth2($this->google_client);
   
       $data = $google_service->userinfo->get();
   
       $current_datetime = date('Y-m-d H:i:s');
   
               //insert data
        $user_data = array(
         'login_oauth_uid' => $data['id'],
         'first_name'  => $data['given_name'],
         'last_name'   => $data['family_name'],
         'email_address'  => $data['email'],
         'profile_picture' => $data['picture'],
         'created_at'  => $current_datetime
        );
   
       $this->session->set_userdata('user_data', $user_data);
       redirect(base_url().'main');
      }
     }
     $login_button = '';
     $data['login_button'] = $login_button;
     if(!$this->session->userdata('access_token'))
     {
      $login_button = '<a href="'.$this->google_client->createAuthUrl().'"><img src="'.base_url().'img/google_login.jpg" /></a>';
      $data['login_button'] = $login_button;
      $this->load->view('google_login', $data);

     }
     else
     {
      //$this->load->view('google_login', $data);
      redirect(base_url().'main');

     }
    }
   
    function logout()
    {
     $this->session->unset_userdata('access_token');
   
     $this->session->unset_userdata('user_data');

     redirect('google_login/login');
    }
 

    /***----------------------------------------------------------------- */
    function calendar(){
        $token = $this->session->userdata('access_token');
        $this->google_client->setAccessToken($token);
        $this->google_client->setApplicationName("Calendario_Histocli");   
        $google_service = new Google\Service\Calendar($this->google_client);
       
        $calendarId = 'amh4ibgjt09q6tq8f9tamkuais@group.calendar.google.com';
        $timeMin = date("c", strtotime(date('Y-m-d ').' 00:00:00'));
        $timeMax = date("c", strtotime(date('Y-m-d ').' 23:59:59'));
        echo '<pre>';
        // //         //print_r($token);        
        var_dump($timeMax);
        $optParams = array(
            'maxResults'   => 50,
            'orderBy'      => 'startTime',
            'singleEvents' => true,
            'timeMin'      => $timeMin,
            'timeMax'      => $timeMax,
            'timeZone'     => 'Asia/Kolkata',
        );

        $events = $google_service->events->listEvents($calendarId, $optParams);
        echo '<pre>';
        // //         //print_r($token);        
        var_dump($events);
        exit;
                
    }
    function calendar2(){
        try{

            $token = $this->session->userdata('access_token');
            $this->google_client->setAccessToken($token);
            $this->google_client->setApplicationName("Calendario_Histocli");   
            $service = new Google\Service\Calendar($this->google_client);

            // Do stuff with the $service object
            $calendarList = $service->calendarList->listCalendarList();

            while(true) {
                foreach ($calendarList->getItems() as $calendarListEntry) {
                    echo $calendarListEntry->getSummary();
                }
                $pageToken = $calendarList->getNextPageToken();
                if ($pageToken) {
                    $optParams = array('pageToken' => $pageToken);
                    $calendarList = $service->calendarList->listCalendarList($optParams);
                } else {
                    break;
                }
            }
        } 
        catch (Google\Exception $e) {
                echo "Caught Google_ClientException:";
                print_r($e);
        }
        catch (Google\Service\Exception $e) {
                echo "Caught Google_ServiceException:";
        }
    }
    function editar(){
        $token = $this->session->userdata('access_token');
        $this->google_client->setAccessToken($token);
        $this->google_client->setApplicationName("Calendario_Histocli");   
        $service = new Google\Service\Calendar($this->google_client);

        $calendarId = 'primary';
        $eventId = '0u0rr66c3ccnv7m6nsqm45csmr_20220927T120000Z';

        $event = $service->events->get($calendarId, $eventId);
       // $event->setSummary('Prueba 1');
        $descripcion = $event->getDescription();
        $event->setDescription('Lucy'.$descripcion);


        $updatedEvent = $service->events->update('primary', $event->getId(), $event);
        echo $updatedEvent->getUpdated();

    }
    function hoy() { //$calendarId, $timeMin = false, $timeMax = false, $maxResults = 100){
        try{
            $timeMin = false;
            $timeMax = false;
            $maxResults = 100;

            $token = $this->session->userdata('access_token');
            $this->google_client->setAccessToken($token);
            $this->google_client->setApplicationName("Calendario_Histocli");   
            $service = new Google\Service\Calendar($this->google_client);

            $calendarId = 'primary';

            // Do stuff with the $service object
            if ( ! $timeMin) {
                $timeMin = date("c", strtotime(date('Y-m-d ').' 00:00:00'));
    
            } else {
                $timeMin = date("c", strtotime($timeMin));
            }
    
            if ( ! $timeMax) {
                $timeMax = date("c", strtotime(date('Y-m-d ').' 23:59:59'));
            } else {  
                $timeMax = date("c", strtotime($timeMax));
    
            }
            $optParams = array(
                'maxResults'   => $maxResults,
                'orderBy'      => 'startTime',
                'singleEvents' => true,
                'timeMin'      => $timeMin,
                'timeMax'      => $timeMax,
                'timeZone'     => 'Asia/Kolkata',
            );
    
            $results = $service->events->listEvents($calendarId, $optParams);
           
            $data = array();
           // $creator = new Google_Service_Calendar_EventCreator();
            foreach ($results->getItems() as $item) {
    
                if(!empty($item->getStart()->date) && !empty($item->getEnd()->date)) {
                    $startDate = date('d-m-Y H:i', strtotime($item->getStart()->date));
                    $endDate = date('d-m-Y H:i', strtotime($item->getEnd()->date));
                } else {
                    $startDate = date('d-m-Y H:i', strtotime($item->getEnd()->dateTime));
                    $endDate = date('d-m-Y H:i', strtotime($item->getEnd()->dateTime));
                }
                
                $created = date('d-m-Y H:i', strtotime($item->getCreated()));
                $updated = date('d-m-Y H:i', strtotime($item->getUpdated()));
                
                array_push(
                    $data,
                    array(
                        'id'          => $item->getId(),
                        'summary'     => trim($item->getSummary()),
                        'description' => trim($item->getDescription()),
                        'creator'     => $item->getCreator()->getEmail(),
                        'organizer'     => $item->getOrganizer()->getEmail(),
                        'creatorDisplayName'     => $item->getCreator()->getDisplayName(),
                        'organizerDisplayName'     => $item->getOrganizer()->getDisplayName(),
                        'created'         => $created,
                        'updated'       => $updated,
                        'start_date'       => $startDate,
                        'end_date'         => $endDate,
                        'status'          => $item->getStatus(),
                    )
                );
            }
            //return $data;
            echo json_encode($data);
        } 
        catch (Google\Exception $e) {
                echo "Caught Google_ClientException:";
                print_r($e);
        }
        catch (Google\Service\Exception $e) {
                echo "Caught Google_ServiceException:";
        }
    }

    function books(){
        $apiKey = $this->getApiKey();

        $this->google_client->setDeveloperKey($apiKey);
        
        $service = new Google\Service\Books($this->google_client);
        $query = 'Henry David Thoreau';
        $optParams = [
            'filter' => 'free-ebooks',
        ];
        $results = $service->volumes->listVolumes($query, $optParams);
        $this->google_client->setDefer(true);
        $query = 'Henry David Thoreau';
        $optParams = [
            'filter' => 'free-ebooks',
        ];
        $request = $service->volumes->listVolumes($query, $optParams);
        $resultsDeferred = $this->google_client->execute($request);
          
        echo '<pre>';
// //         //print_r($token);        
        var_dump($results);
        exit;	
    }
   function crear(){
    
        $event = new Google\Service\Calendar\Event(array(
            'summary' => 'Google I/O 2015',
            'location' => '800 Howard St., San Francisco, CA 94103',
            'description' => 'A chance to hear more about Google\'s developer products.',
            'start' => array(
            'dateTime' => '2022-09-28T09:00:00-07:00',
            'timeZone' => 'America/Los_Angeles',
            ),
            'end' => array(
            'dateTime' => '2022-09-28T17:00:00-07:00',
            'timeZone' => 'America/Los_Angeles',
            ),
            'recurrence' => array(
            'RRULE:FREQ=DAILY;COUNT=2'
            ),
            'attendees' => array(
            array('email' => 'lpage@example.com'),
            array('email' => 'sbrin@example.com'),
            ),
            'reminders' => array(
            'useDefault' => FALSE,
            'overrides' => array(
                array('method' => 'email', 'minutes' => 24 * 60),
                array('method' => 'popup', 'minutes' => 10),
            ),
            ),
        ));
        
        $calendarId = 'primary';

        $timeMin = date("c", strtotime(date('Y-m-d ').' 00:00:00'));
        $timeMax = date("c", strtotime(date('Y-m-d ').' 23:59:59'));
        $optParams = array(
            'maxResults'   => 10,
            'orderBy'      => 'startTime',
            'singleEvents' => true,
            'timeMin'      => $timeMin,
            'timeMax'      => $timeMax,
            'timeZone'     => 'Asia/Kolkata',
        );



        $token = $this->session->userdata('access_token');
        
        $this->google_client->setAccessToken($token);
        $this->google_client->setApplicationName("Calendario_Histocli");
        //$google_service = new Google\Service\Oauth2($this->google_client);

        $google_service = new Google\Service\Calendar($this->google_client);

       $results = $google_service->events->listEvents($calendarId, $optParams);

        $event = $google_service->events->insert($calendarId, $event);
       // printf('Event created: %s\n', $event->htmlLink);
  
   }

    
}
?>
