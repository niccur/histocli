<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tratamientos extends CI_Controller {
    
    function __construct() 
    {
		parent::__construct();		
		$this->load->library('session');				
		$this->load->helper('url');
                $this->load->model('Tratamientos_model');
                $this->load->model('Fichapacientes_model');
                $this->load->model('Calendar_model');
		
		$user_logged = $this->session->userdata("usuario");
		if (isset($user_logged) and $user_logged!='') {							
		} else {
			redirect("login");
		}
		
    }
 
    function index()
    {
		$this->load->view('encabezado');
		$this->load->view('tratamientos_pendientes');
               
    }
    
    function load_tratamientos()
    {      
        $estado = $this->input->post('estado');  
		$resultado = $this->Tratamientos_model->load_Tratamientos_cab();  
		echo json_encode($resultado);

       //echo '<pre>';
       // print_r($amex);        
       // var_dump(json_encode($resultado));
       // exit;		
     }
    function load_tratamiento_det($cabecera)
    {        
		
        $resultado = $this->Tratamientos_model->load_Tratamiento_det($cabecera);  
		echo json_encode($resultado);	
     }	 
    
    function load_tratamiento_det_precios()
    {      
        $id_det = $this->input->post('idDet');          
        $resultado = $this->Tratamientos_model->load_tratamiento_det_precio($id_det);  
	echo json_encode($resultado);

       //echo '<pre>';
       // print_r($amex);        
       //var_dump(json_encode($resultado));
       // exit;		
     }     
	  
    function tratamientos_paciente($idPaciente)
    {
        $paciente = $this->Fichapacientes_model->get_paciente_data($idPaciente); 
        $resultado = $this->Tratamientos_model->load_pendientes_paciente($idPaciente);  
        $data["paciente"] = $paciente;
        $data["pendientes"] = $resultado;
        
        //echo '<pre>';
       // print_r($amex);        
       //var_dump(json_encode($data));
        //exit;	
        $this->load->view('encabezado');
        $this->load->view('tratamientos_pendientes',$data);        
    } 
    
    function load_pendientes($idPaciente){
        $resultado = $this->Tratamientos_model->load_pendientes_paciente($idPaciente);  
        echo json_encode($resultado);        
    }
    function load_profesionales(){
        $resultado = $this->Tratamientos_model->load_profesionales();  
        echo json_encode($resultado);        
    }
    function load_pendientes_turnos($idPaciente){
        $resultado = $this->Tratamientos_model->load_pendientes_paciente_turnos($idPaciente);  
        echo json_encode($resultado);        
    }    
    function asignarCI(){
        $data["idPaciente"] = $this->input->post('id');  
        $data["nombre"] = $this->input->post('nombre');     
        $data["idTrat"] = null;
        $data["turno"] = null;
        $data["inter"] = null;
        $data["descrip"] = null;
        $data["prof"] = null;
        $rand = rand(1,1000000);
        $rand = 'X'.$rand;
        $data["clave"] = $rand;
        $dataSession[$rand] = $data;
        $this->session->set_userdata($dataSession);
        echo json_encode(array("clave_random"=>$rand));
    }   
     function asignarCITrat(){
         
        $data["idPaciente"] = $this->input->post('id');  
        $data["nombre"] = $this->input->post('nombre'); 
        $data["apellido"] = $this->input->post('apellido');     
        $data["idTrat"] = $this->input->post('idTrat');
        $data["turno"] = $this->input->post('turno');
        $data["descrip"] = $this->input->post('descrip');  
        $data["prof"] = $this->input->post('prof');  
        $data["action"] = $this->input->post('action');  
        $rand = rand(1,1000000);
        $rand = 'X'.$rand;
        $data["clave"] = $rand;
         
        $dataSession[$rand] = $data;
        $this->session->set_userdata($dataSession);

        echo json_encode(array("clave_random"=>$rand));
    } 
    function dar_turno($idPaciente){
        
        $data["idPaciente"] = $idPaciente;
	        
        $profs = $this->Calendar_model->get_Prof();  
	$data["profesionales"] = json_encode($profs);
                
        $this->load->view('encabezado');
        $this->load->view('calendar',$data); 
        
    }    
    function asignar($clave){
        $data =  $this->session->userdata($clave);        
       // $this->session->unset_userdata($clave);
//        
//          echo "<pre>";
//        //print_r($amex);        
//       var_dump(json_encode($data));
//        exit;	
        $this->load->view('encabezado');
        $this->load->view('asignar_tratamientos',$data);         
    }
    function asignar_histo($clave){
        $data =  $this->session->userdata($clave);  

        $this->load->view('encabezado');
        $this->load->view('histoABM',$data);  
    }

    function agregar_tratamientos()
    {
        $clave = $this->input->post('clave');       
        $turno = $this->input->post('turno');
        $turnoArray = explode(" ",$turno);
        $fechaArray = explode("/",$turnoArray[0]);
        $fecha = $fechaArray[2]."-".$fechaArray[1]."-".$fechaArray[0];
        $horaArray = explode(":",$turnoArray[1]);
        $hora = $horaArray[0];
        $hora = ($turnoArray[2] == 'PM' ?$hora+12:$hora);
        $min = $horaArray[1];
        $seg = $horaArray[2];
        $fecha = $fecha." ".$hora.":".$min.":".$seg;
        
        $data["profesional"] = $this->input->post('profesional');
        $data["descripcion"] = $this->input->post('descripcion');
        $data["turno"] = $fecha;
        $data["paciente"] = $this->session->userdata($clave);
        
        $this->session->unset_userdata($clave);
     
        $resultado = $this->Tratamientos_model->agregar_tratamiento_paciente($data);        
        return json_encode($resultado);

    }      
    function eliminar_tratamientos_paciente($idTrat){
        
        $resultado = $this->Tratamientos_model->eliminar_tratamientos_paciente($idTrat); 

        return json_encode($resultado);
    }  
    function editar_tratamientos_paciente($idTrat){
        $turno = $this->input->post('turno');
        $turnoArray = explode(" ",$turno);
        $fechaArray = explode("/",$turnoArray[0]);
        $fecha = $fechaArray[2]."-".$fechaArray[1]."-".$fechaArray[0];
        $horaArray = explode(":",$turnoArray[1]);
        $hora = $horaArray[0];
        $hora = ($turnoArray[2] == 'PM' ?$hora+12:$hora);
        $min = $horaArray[1];
        $seg = $horaArray[2];
        $fecha = $fecha." ".$hora.":".$min.":".$seg;
        $data["turno"] = $fecha;
        $data2["idTrat"] = $idTrat;
        $data["descripcion"] = $this->input->post('descrip');  
        $data["id_profesional"] = $this->input->post('prof'); 
                
        $resultado = $this->Tratamientos_model-> update_tratamientos_paciente($data2,$data); 

        return json_encode($resultado);
    }     
}
?>
