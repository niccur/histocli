<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller{

    function __construct() 
	{
        parent::__construct();
		//debug
		//$this->output->enable_profiler(TRUE);			
		$this->load->library('session');				
		$this->load->helper('url');
        require_once BASEPATH.'../vendor/autoload.php';
        $this->google_client = new Google\Client();
        $this->google_client->setClientId('507452867839-kh32ok72c1d8gj249su9r7t5s1h5qpg5.apps.googleusercontent.com'); //Define your ClientID
        $this->google_client->setClientSecret('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'); //Define your Client Secret Key
        $this->google_client->setRedirectUri(base_url().'Google_Calendar/login'); //Define your Redirect Uri
        $this->google_client->addScope(Google\Service\Calendar::CALENDAR);
    
    }
	
    function index()
	{
       // $this->load->view('encabezado');
		//$this->load->view('login');
		//$this->load->view('pie');
      
	   $this->load->view('login2');
	   
    }

    function get_acceso()
	{
		$tmp=$this->session->userdata('acceso');
        
		if (empty($tmp))
		{
			echo (0);
		}
        else
        { 
			echo ($this->session->userdata('acceso'));
		}    
	}

    public function validarLogin()
	
	{   
        $username = $this->input->post('txtusuario');
        $clave = $this->input->post('txtclave');
        $this->load->model('usuario_model');
        $resultado = $this->usuario_model->validarUsuario($username, $clave);	
				
        echo json_encode($resultado);
    }

	function cambio_clave()
	{
	
	    $this->load->view('encabezado');
		$this->load->view('cambio_clave');
		$this->load->view('pie');
	
	}
    function actualizar() 
	{				
		$oldpass = $this->input->post('vieja');
		$newpass = $this->input->post('nueva');
		
		$this->load->model('usuario_model');
		
		$resultado = $this->usuario_model->actualizar($oldpass,$newpass);
		
        echo json_encode($resultado);  
	}
	
	// Función logout. Elimina las variables de sesión y redirige al controlador principal
    function logout()
    {
		$token = $this->session->userdata('access_token');
		$this->google_client->setAccessToken($token);

		$this->google_client->revokeToken();
        $this->session->unset_userdata('access_token');
       
		$this->session->sess_destroy();
		redirect("login");
		  
	}

}

?>
