<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
    
	function __construct() 
	{
		parent::__construct();		
		$this->load->library('session');				
		$this->load->helper('url');
        $this->load->model('main_model');
        $this->load->model('Tratamientos_model');
		
		$user_logged = $this->session->userdata("usuario");
		if (isset($user_logged) and $user_logged!='') {							
		} else {
			redirect("login");
		}
		
    }
    function index(){
        $data["acceso"] = $this->session->userdata('usuario')->acceso;

        $this->load->view('encabezado');
        $this->load->view('fichapacientes',$data);
                
    }
    function agenda(){        
        $this->load->view('encabezado');
        $this->load->view('agenda');
    }
    function profesional(){        
        $this->load->view('encabezado');
        $this->load->view('listaprof');
    }
    function profesionalForm(){        
        $this->load->view('encabezado');
        $this->load->view('profesional');
    }
    function get_turnos_main(){
         $usuario = $this->session->userdata('usuario')->username;

         $turnos = $this->main_model->get_turnos_profesional_main($usuario);
  
           // echo "<pre>";
           // var_dump(json_encode($turnos));
          // exit;	
         echo json_encode($turnos);
    }
    function get_turnos_main_profesional($idProf){
        $turnos = $this->main_model->get_turnos_profesional($idProf);
 
        echo json_encode($turnos);
   }
    function load_profesionales_session(){
        $data['profesionales'] = $this->Tratamientos_model->load_profesionales(); 
        $usuario = $this->session->userdata('usuario')->username;
        $data['idProf'] = $this->main_model->get_id_profesional_session($usuario);
        echo json_encode($data);        
    }
    function load_profesionales(){
        $resultado = $this->main_model->load_profesionales();  
        echo json_encode($resultado);        
    }
    function mainCI(){
         
        $data["username"] = $this->input->post('username');  
        $data["acceso"] = $this->input->post('acceso');  

        $rand = rand(1,1000000);
        $rand = 'X'.$rand;
        $data["clave"] = $rand;
         
        $dataSession[$rand] = $data;
        $this->session->set_userdata($dataSession);

        echo json_encode(array("clave_random"=>$rand));
    } 
    function eliminar_turno($idTurno){
        $resultado = $this->main_model->eliminar_turno($idTurno); 
        echo json_encode($resultado);
    }  
    function eliminar_profesional($idProf){
        $resultado = $this->main_model->eliminar_profesional($idProf); 
        echo json_encode($resultado);
    }  
    function new_profesional()
	{              
        $data["nombre"] = $this->input->post('nombre');
        $data["apellido"] = $this->input->post('apellido');
        
        $resultado = $this->main_model->agregar_profesional($data);        
        echo json_encode($resultado);
             
   } 
    function en_construccion()
        {
		$this->load->view('encabezado');
		$this->load->view('enconstruccion');
          
            
        } 
	
}
?>
