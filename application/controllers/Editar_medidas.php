<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Editar_medidas extends CI_Controller {
    
    function __construct() 
    {
		parent::__construct();		
		$this->load->library('session');				
		$this->load->helper('url');
	
		$user_logged = $this->session->userdata("usuario");
		if (isset($user_logged) and $user_logged!='') {							
		} else {
			redirect("login");
		}
		
    }
       
    function medidaCI(){
        $data["idMedida"] = $this->input->post('id');  
        $data["modo"] = $this->input->post('modo');  
        $data["idPaciente"] = $this->input->post('idPaciente');  
        $rand = rand(1,1000000);
		$rand = 'X'.$rand;
        $data["claveMedida"] = $rand;
        $dataSession[$rand] = $data;
    	$this->session->set_userdata($dataSession);
        echo json_encode(array("clave_random"=>$rand));

    }   

	function load_editar_medidas($claveMedida)
	{
		$data =  $this->session->userdata($claveMedida);   
		$this->load->view('encabezado');
		$this->load->view('editar_medidas',$data);
               
    }    
     function index()
	{
		$this->load->view('encabezado');
		$this->load->view('editar_medidas',$data);               
    }        
}
?>
