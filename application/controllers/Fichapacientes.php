<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fichapacientes extends CI_Controller {
    
    function __construct() 
    {
		parent::__construct();		
		$this->load->library('session');				
		$this->load->helper('url');
      $this->load->model('Fichapacientes_model');
		
		$user_logged = $this->session->userdata("usuario");
		if (isset($user_logged) and $user_logged!='') {							
		} else {
			redirect("login");
		}
		
    }
 
     function index()
	{
      $data["acceso"] = $this->session->userdata('usuario')->acceso;

		$this->load->view('encabezado');
		$this->load->view('fichapacientes',$data);
               
    }
    
    function load_pacientes_paging()
    {      
            $paging = $this->input->post('paging');  
            $pagenum = $this->input->post('pagenum');  
		            
            $start = $paging * $pagenum;
            
            $resultado = $this->Fichapacientes_model->get_pacientes_paging($start,$paging);  
            echo json_encode($resultado);
     }
	
	function load_pacientes()
    {      
            $resultado = $this->Fichapacientes_model->get_Pacientes();  
			echo json_encode($resultado);

       //echo '<pre>';
       // print_r($amex);        
        //var_dump($amex);
        //exit;		
     }

  function load_filiatorio($idPaciente)
    {            
        $data["idPaciente"] = $idPaciente;  
        $this->load->view('encabezado');
        $this->load->view('filiatorio',$data);               
     } 
     
  function new_filiatorio()
	{              
            $data['idPaciente'] = 0;
            
            $this->load->view('encabezado');
            $this->load->view('filiatorio',$data);               
   }     
         
    function pacienteCI(){
        $data["idPaciente"] = $this->input->post('id');  
        $data["nombre"] = $this->input->post('nombreApe');  
        $rand = rand(1,1000000);
        $rand = 'X'.$rand;
        $data["clave"] = $rand;
        $dataSession[$rand] = $data;
        $this->session->set_userdata($dataSession);
        echo json_encode(array("clave_random"=>$rand));
    }      
  function load_anamnesis($clave)
    {            
        $data =  $this->session->userdata($clave); 
        $this->load->view('encabezado');
        $this->load->view('anamnesis',$data);               
     } 
  function load_medidas($clave)
    {            
        $data =  $this->session->userdata($clave); 
        $this->load->view('encabezado');
        $this->load->view('medidas',$data);               
     }      
   function eliminar_paciente($idPaciente){
      $resultado = $this->Fichapacientes_model->eliminar_paciente($idPaciente); 
      return json_encode($resultado);
  }
}
?>
