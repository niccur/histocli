<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prueba2 extends CI_Controller {
    
    function __construct() 
    {
		parent::__construct();		
		$this->load->library('session');				
		$this->load->helper('url');
    
        $user_logged = $this->session->userdata("usuario");
		if (isset($user_logged) and $user_logged!='') {							
		} else {
			redirect("login");
		}
        require_once BASEPATH.'../vendor/autoload.php';
        $this->google_client = new Google\Client();

    }  
    
    function calendar2(){
        try{

            $this->google_client->setApplicationName('pruebaCalendario');
            $this->google_client->setAuthConfig('/home/nico/work/histocli/.google_service_account.json');
            $this->google_client->setScopes(['https://www.googleapis.com/auth/calendar']);
            $this->google_client->setSubject('niccur@gmail.com');
            $this->google_client->setAccessType('offline');

            $service = new Google\Service\Calendar($this->google_client);

            // Do stuff with the $service object
            $calendarList = $service->calendarList->listCalendarList();

            while(true) {
                foreach ($calendarList->getItems() as $calendarListEntry) {
                    echo $calendarListEntry->getSummary();
                }
                $pageToken = $calendarList->getNextPageToken();
                if ($pageToken) {
                    $optParams = array('pageToken' => $pageToken);
                    $calendarList = $service->calendarList->listCalendarList($optParams);
                } else {
                    break;
                }
            }
        } 
        catch (Google\Exception $e) {
                echo "Caught Google_ClientException:";
                print_r($e);
        }
        catch (Google\Service\Exception $e) {
                echo "Caught Google_ServiceException:";
        }
    }

    function calendar(){

        try{

            $client = new Google\Client(array('use_objects' => true));
            
            $client->setAuthConfig('/home/nico/work/histocli/.google_service_account.json');

            $service = new Google\Service\Calendar($client);

            $event = new Google\Service\Calendar\Event;

            $event->setSummary('Event nuevo');
            $event->setLocation('en un sitio');

            $start = new Google\Service\Calendar\EventDateTime();
            $start->setDateTime('2022-10-02T19:00:00.000+01:00');
            $start->setTimeZone('Europe/Madrid');
            $event->setStart($start);

            $end = new Google\Service\Calendar\EventDateTime();
            $end->setDateTime('2022-10-02T19:25:00.000+01:00');
            $end->setTimeZone('Europe/Madrid');
            $event->setEnd($end);

            $new_event = null;
            $new_event_id = "";

            $new_event = $service->events->insert('primary', $event);

            if($new_event!=null){

                $new_event_id= $new_event->getId();
                $event = $service->events->get('primary', $new_event_id);

                if ($event != null) {
                echo "<br/>Inserted:";
                echo "<br/>EventID=".$event->getId();
                echo "<br/>Summary=".$event->getSummary();
                echo "<br/>Status=".$event->getStatus();
                }
                    else{
                    echo "No se ha podido obtener la información del evento";
                        }			   
                }else{
                        echo "No se ha podido insertar el evento";
                }

        } 
        catch (Google\Exception $e) {
                echo "Caught Google_ClientException:";
                print_r($e);
        }
        catch (Google\Service\Exception $e) {
                echo "Caught Google_ServiceException:";
                echo "<pre>".print_r($e,true)."</pre>";
        }
        }
}
?>