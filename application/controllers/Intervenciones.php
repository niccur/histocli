<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Intervenciones extends CI_Controller {
    
    function __construct() 
    {
		parent::__construct();		
		$this->load->library('session');				
		$this->load->helper('url');
                $this->load->model('Intervenciones_model');
		
		$user_logged = $this->session->userdata("usuario");
		if (isset($user_logged) and $user_logged!='') {							
		} else {
			redirect("login");
		}
		
    }
 
    function index()
    {
		$this->load->view('encabezado');
		$this->load->view('intervenciones');
               
    }
    
	function load_departamentos()
    {      
		$resultado = $this->Intervenciones_model->load_departamentos();  
		echo json_encode($resultado);
	}
	
    function load_intervenciones()
    {      
		$resultado = $this->Intervenciones_model->load_intervenciones();  
		echo json_encode($resultado);

       //echo '<pre>';
       // print_r($amex);        
       // var_dump(json_encode($resultado));
       // exit;		
     }
   
    function load_tratamiento_det_precios()
    {      
        $id_det = $this->input->post('idDet');          
        $resultado = $this->Tratamientos_model->load_tratamiento_det_precio($id_det);  
	echo json_encode($resultado);

       //echo '<pre>';
       // print_r($amex);        
       //var_dump(json_encode($resultado));
       // exit;		
     }     
	  
 
        
}
?>
