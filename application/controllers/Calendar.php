<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends CI_Controller {
    
    function __construct() 
    {
	parent::__construct();		
	$this->load->library('session');				
	$this->load->helper('url');
	$this->load->model('Calendar_model');

	$user_logged = $this->session->userdata("usuario");
	if (isset($user_logged) and $user_logged!='') {							
	} else {
		redirect("login");
	}
		
    }
    
    function estados_turnos(){
         $resultado = $this->Calendar_model->get_EstadosTurnos();
         echo json_encode($resultado);
    }
    
  function index()
    {
	    $data["idPaciente"] = 0;	
	    $profs = $this->Calendar_model->get_Prof();  
	    $data["profesionales"] = json_encode($profs);
	    $this->load->view('encabezado');
	    $this->load->view('calendar',$data);
	    //$this->load->view('calendar_responsive',$data);
	    //$this->load->view('pie');           
    } 
		
  function turno_paciente($idPaciente)
        {
		$data["idPaciente"] = $idPaciente;
                $profs = $this->Calendar_model->get_Prof();  
		$data["profesionales"] = json_encode($profs);	
               
		$this->load->view('encabezado');
		$this->load->view('calendar',$data);		
		} 	
	 
     function get_paciente($idPaciente)
    {
             $resultado = $this->Calendar_model->get_Paciente($idPaciente);  
		    echo json_encode($resultado);
     }
	 
    function load_pacientes()
    {
             $resultado = $this->Calendar_model->get_Pacientes();  
		    echo json_encode($resultado);
     }
       
    function load_prof()
    {
                    $resultado = $this->Calendar_model->get_Prof();  
		    echo json_encode($resultado);
     }
	
//    function colorProf()
//      {
//                    $id  = $this->input->post('idProf');
//                    $resultado = $this->Calendar_model->get_ColorProf($id);  
//				
//		    echo $resultado;
//      }
//          
    function load_turnos()
    {
             $data['start'] = $this->input->post('f1');
             $data['end'] = $this->input->post('f2'); 
             $data['sele'] = $this->input->post('selections');
             $data['estado'] = $this->input->post('estado');
             $data['idPaciente'] = $this->input->post('paciente');
             $data['ver_paciente'] = $this->input->post('verPaciente');
		                
        //echo '<pre>';
      //print_r('fdesde '.$fdesde.' fhasta '.$fhasta.' grupo '.$sele);
    //     exit;
		                
            $resultado = $this->Calendar_model->get_Turnos($data);  
            echo json_encode($resultado);
     }
	
    function add_turno()
	{
//                $dataV['start']  = $this->input->post('start');
//                $dataV['end']  = $this->input->post('end');
//                $dataV['id']  = 0;
//                $dataV['id_prof']  = $this->input->post('idProf');
                
                $data['id_paciente']  = $this->input->post('id_paciente');
                $data['id_profesional']  = $this->input->post('id_profesional');           
                $data['start']  = $this->input->post('start');
                $data['end']  = $this->input->post('end');
                $data['descrip']  = $this->input->post('descrip');
                $data['id_estadoturno']  = $this->input->post('id_estadoturno');
                $data['editable']  = $this->input->post('editable');
                $data['ocupado']  = $this->input->post('ocupado');
                $tratamientos  = $this->input->post('tratamientos');
                    
                
//                if($this->Calendar_model->validar_Sobreturno($dataV)){
//                    echo 'false';
//                }else{
                        $resultado = $this->Calendar_model->add_Turno($data,$tratamientos);                        
                        echo json_encode($resultado);
//                }         
               
//              echo '<pre>';
//       print_r($data);
//       exit;

        }       

     function update_turno()
	{
                $dataU['start']  = $this->input->post('start');
                $dataU['end']  = $this->input->post('end');
                $dataU['id_paciente']  = $this->input->post('id_paciente');
                $dataU['id_profesional']  = $this->input->post('id_profesional');   
                $dataU['descrip']  = $this->input->post('descrip');
                $dataU['id_estadoturno']  = $this->input->post('id_estadoturno');
                $dataU['editable']  = $this->input->post('editable');
                $dataU['ocupado']  = $this->input->post('ocupado');
                
                $id  = $this->input->post('id');
//                $dataV['start']  = $this->input->post('start');
//                $dataV['end']  = $this->input->post('end');
//                $dataV['id']  = $this->input->post('id');
//                $dataV['id_prof']  = $this->input->post('idProf');                
//                 
//                if($this->Calendar_model->validar_Sobreturno($dataV)){
//                    echo 'false';
//                }else{
                        $resultado = $this->Calendar_model->update_Turno($dataU,$id);             
                        echo json_encode($resultado);
//                } 
        } 
        
        
        
    function load_agendas()
    {
             $data['start'] = $this->input->post('f1');
             $data['end'] = $this->input->post('f2'); 
             $data['sele'] = $this->input->post('selections');
			 $vista = $this->input->post('vista');
			 
			 if($vista == 'agendaDay'){

					//$fecha = strtotime($data['start']);				
					//$fd = $fecha->format('Y-m-d');
					//$anio = $fecha->format('Y');
					
					$semana = date('W', strtotime($data['start']));
					$anio = substr($data['start'],0,4);
					
					function getStartAndEndDate($week, $year) {
						  $dto = new DateTime();
						  $dto->setISODate($year, $week);
						  $ret['week_start'] = $dto->format('Y-m-d');
						  $dto->modify('+7 days');
						  $ret['week_end'] = $dto->format('Y-m-d');
						  return $ret;
						}
								
					$rango= getStartAndEndDate($semana,$anio);
					
					$data['start'] = $rango['week_start'];
					$data['end']= $rango['week_end'];
			 }
		                		                
            $resultado = $this->Calendar_model->get_Agendas($data);  
            echo json_encode($resultado);
     }
	
    function add_agenda()
	{
//                $dataV['start']  = $this->input->post('start');
//                $dataV['end']  = $this->input->post('end');
//                $dataV['id']  = 0;
//                $dataV['id_prof']  = $this->input->post('idProf');
                
                $data['id_profesional']  = $this->input->post('id_profesional');           
                $data['start']  = $this->input->post('start');
                $data['end']  = $this->input->post('end');
                $data['descrip']  = $this->input->post('descrip');
                $data['editable']  = $this->input->post('editable');
                $data['ocupado']  = $this->input->post('ocupado');
                    
                
//                if($this->Calendar_model->validar_Sobreturno($dataV)){
//                    echo 'false';
//                }else{
                        $resultado = $this->Calendar_model->add_Agenda($data);                        
                        echo json_encode($resultado);
//                }         
               
//              echo '<pre>';
//       print_r($data);
//       exit;

        }       

     function update_agenda()
	{
                $dataU['start']  = $this->input->post('start');
                $dataU['end']  = $this->input->post('end');
                $dataU['id_profesional']  = $this->input->post('id_profesional');   
                $dataU['descrip']  = $this->input->post('descrip');
                $dataU['editable']  = $this->input->post('editable');
                $dataU['ocupado']  = $this->input->post('ocupado');
                
                $id  = $this->input->post('id');
//                $dataV['start']  = $this->input->post('start');
//                $dataV['end']  = $this->input->post('end');
//                $dataV['id']  = $this->input->post('id');
//                $dataV['id_prof']  = $this->input->post('idProf');                
//                 
//                if($this->Calendar_model->validar_Sobreturno($dataV)){
//                    echo 'false';
//                }else{
                        $resultado = $this->Calendar_model->update_Agenda($dataU,$id);             
                        echo json_encode($resultado);
//                } 
        } 
        
}
?>
