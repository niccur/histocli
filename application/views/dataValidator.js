function isNumberKey(evt) {                
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
        return true;
    }      
}
function isNumberKeyPhone(evt) {                
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            if(charCode == 40 ||charCode == 41 ||charCode == 45) {
                return true;
            }else return false;      
        }                
}
function isNumberKeyTime(evt) {                
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        if(charCode == 58) {
            return true;
        }else return false;      
    }                
}      
function isNumberDecimal(evt) {                
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        if(charCode === 44) {
            return true;
        }else return false;      
    }                
}   
function dateValidator(evt){
    var ExpiryDate = evt.val(), //$("#fec_nac").val(),
            objDate,  // date object initialized from the ExpiryDate string 
            mSeconds, // ExpiryDate in milliseconds 
            day,      // day 
            month,    // month 
            year,
            result;     // year 
    // date length should be 10 characters (no more no less) 
    result = true; 
    if (ExpiryDate.length !== 10) { 
        //alert('no es 10');
        result = false; 
    } 
    // third and sixth character should be '/' 
    if ((ExpiryDate.substring(2, 3) !== '/' || ExpiryDate.substring(5, 6) !== '/') && ExpiryDate.length > 0) { 

        result = false; 
    } 
    // extract month, day and year from the ExpiryDate (expected format is mm/dd/yyyy) 
    // subtraction will cast variables to integer implicitly (needed 
    // for !== comparing) 
    month = ExpiryDate.substring(3, 5) - 1; // because months in JS start from 0 
    day = ExpiryDate.substring(0, 2) - 0; 
    year = ExpiryDate.substring(6, 10) - 0; 
    // test year range 
    if ((year < 1000 || year > 3000) && ExpiryDate.length > 0) { 
        result = false; 
    } 
    // convert ExpiryDate to milliseconds 
    mSeconds = (new Date(year, month, day)).getTime(); 
    // initialize Date() object from calculated milliseconds 
    objDate = new Date(); 
    objDate.setTime(mSeconds); 
    // compare input date and parts from Date() object 
    // if difference exists then date isn't valid 
    if ((objDate.getFullYear() !== year || 
        objDate.getMonth() !== month || 
        objDate.getDate() !== day) && ExpiryDate.length > 0) { 
        result = false; 
    } 
    if (ExpiryDate.length === 0) { 
        //alert('no es 10');
        result = true; 
    }  
    return result;
}
function dateValidatorNN(evt){
    var ExpiryDate = evt.val(), //$("#fec_nac"),
            objDate,  // date object initialized from the ExpiryDate string 
            mSeconds, // ExpiryDate in milliseconds 
            day,      // day 
            month,    // month 
            year,
            result;     // year 
    // date length should be 10 characters (no more no less) 
    result = true; 
    if (ExpiryDate.length !== 10) { 
        //alert('no es 10');
        result = false; 
    } 
    // third and sixth character should be '/' 
    if ((ExpiryDate.substring(2, 3) !== '/' || ExpiryDate.substring(5, 6) !== '/') && ExpiryDate.length > 0) { 

        result = false; 
    } 
    // extract month, day and year from the ExpiryDate (expected format is mm/dd/yyyy) 
    // subtraction will cast variables to integer implicitly (needed 
    // for !== comparing) 
    month = ExpiryDate.substring(3, 5) - 1; // because months in JS start from 0 
    day = ExpiryDate.substring(0, 2) - 0; 
    year = ExpiryDate.substring(6, 10) - 0; 
    // test year range 
    if ((year < 1000 || year > 3000) && ExpiryDate.length > 0) { 
        result = false; 
    } 
    // convert ExpiryDate to milliseconds 
    mSeconds = (new Date(year, month, day)).getTime(); 
    // initialize Date() object from calculated milliseconds 
    objDate = new Date(); 
    objDate.setTime(mSeconds); 
    // compare input date and parts from Date() object 
    // if difference exists then date isn't valid 
    if ((objDate.getFullYear() !== year || 
        objDate.getMonth() !== month || 
        objDate.getDate() !== day) && ExpiryDate.length > 0) { 
        result = false; 
    } 
    if (ExpiryDate.length === 0) { 
        //alert('no es 10');
        result = false; 
    }  
    return result;
}

function substr_count(haystack, needle, offset, length) {
  //  discuss at: http://phpjs.org/functions/substr_count/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Onno Marsman
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Thomas
  //   example 1: substr_count('Kevin van Zonneveld', 'e');
  //   returns 1: 3
  //   example 2: substr_count('Kevin van Zonneveld', 'K', 1);
  //   returns 2: 0
  //   example 3: substr_count('Kevin van Zonneveld', 'Z', 0, 10);
  //   returns 3: false

  var cnt = 0;

  haystack += '';
  needle += '';
  if (isNaN(offset)) {
    offset = 0;
  }
  if (isNaN(length)) {
    length = 0;
  }
  if (needle.length == 0) {
    return false;
  }
  offset--;

  while ((offset = haystack.indexOf(needle, offset + 1)) != -1) {
    if (length > 0 && (offset + needle.length) > length) {
      return false;
    }
    cnt++;
  }

  return cnt;
}
