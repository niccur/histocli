<!DOCTYPE html>
<html>
<head>          
    <link href='<?=base_url();?>css/calendar.css' rel='stylesheet' />
    <link href='<?=base_url();?>js/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link href='<?=base_url();?>js/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />

    <link rel="stylesheet" href="<?=base_url();?>css/messi/messi.min.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url();?>css/ajaxLoader/ajaxloader.css" type="text/css">

    <link href='<?=base_url();?>js/jqwidgets/styles/jqx.ui-redmond.css' rel='stylesheet' type="text/css" />        
    <link href='<?=base_url();?>css/temas/redmond/css/redmond/jquery-ui-1.10.4.custom.min.css' rel='stylesheet' type="text/css" />  

    <link href='<?=base_url();?>css/jquery.qtip.min.css' rel='stylesheet' type="text/css" />   

    <script src='<?=base_url();?>js/fullcalendar/lib/moment.min.js'></script>
    <!--<script src='<?=base_url();?>js/fullcalendar/lib/jquery.min.js'></script> -->
    <script src='<?=base_url();?>js/fullcalendar/fullcalendar.min.js'></script>
    <script src='<?=base_url();?>js/jquery.qtip.min.js'></script>	   
    <script type="text/javascript" src="<?=base_url();?>js/messi/messi.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/ajaxLoader/jquery.ajaxloader.1.5.1.min.js"></script>		
<script type="text/javascript">
  
    var profs = []; 
    var idPacienteJs;
    var tratPendientes;
    var tratSeleccion = [];
    $(document).ready(function() {
            		
    //crear la lista de profesionales            
        var estadoTurno=1;
        var verPaciente=2; //1:paciente 2: todos
        var estados;
        var pacientes;
        var profesionales;
        var listBox = $('#selector');
        var lstProfesionalesTurno = $('#profesionalesTurno');
        var lstPacientes = $('#pacientes');
        var source = null;
        var fechaTurno = $('#fechaTurno');
        var turnoDescrip = $('#turnoDescrip');
        var fechaAgenda = $('#fechaAgenda');
        var lstProfesionalesAgenda = $('#profesionalesAgenda');
        var agendaDescrip = $('#agendaDescrip');
        var rangoStart;
        var rangoEnd;
        idPacienteJs =  <?php echo $idPaciente ;?>; 
	profesionales = <?php echo $profesionales ;?>;
        var origen = "<?php echo $_SERVER["HTTP_REFERER"]?>"; 
        console.log(origen);
        origen = origen.lastIndexOf("tratamientos_paciente");
        console.log(origen);
        if(origen < 0){
            $('#volver').hide();
        }else{
            $('#volver').show();            
        };
        
        var get_paciente = function(){
            if(idPacienteJs == 0){
                    return lstPacientes.val();
            }else{
                    return idPacienteJs;
            }
        };
			
        if(idPacienteJs != 0){
            var paciente_turno;
            $.ajax({
                async: false,
                url: "<?=base_url();?>calendar/get_paciente/"+idPacienteJs,
                success: function (data, status, xhr) {
                            paciente_turno = $.parseJSON(data); 								
                    }
            });
            $('#nombre-turno').html(paciente_turno.nombre+" "+paciente_turno.apellido);
	    $('#nombre-turno2').html(paciente_turno.nombre+" "+paciente_turno.apellido);
            $('#fotoPerfil').attr('src', '<?=base_url();?>img/'+paciente_turno.foto);
            $('#ver_paciente').show();
        }else{
            $('#ver_paciente').hide();
        }
        $('#volverBtn').on('click', function () {
            parent.history.back();
        });
        
        $('#btnConfirmarTurno').on('click', function () {
            $('#turnos').jqxValidator('validate');
        });
        $('#btnCancelarTurno').on('click', function () {		
            $('#calendar').fullCalendar('unselect');
            $('#ventanaEvento').jqxWindow('close');
        });	
        $('#btnConfirmarAgenda').on('click', function () {
            $('#agendas').jqxValidator('validate');
        });
        $('#btnCancelarAgenda').on('click', function () {		
            $('#calendar').fullCalendar('unselect');
            $('#ventanaEvento').jqxWindow('close');
        });					

        $('#btnAceptarTurno').on('click', function () {
            tratSeleccion = [];            
            $.each($('#tratSelect :selected'), function(){            
                tratSeleccion.push($(this).val());
            });   
            console.log("tamanio "+tratSeleccion.length);
            $('#formTurnoPaciente').jqxValidator('validate');
        });

        $('#formTurnoPaciente').jqxValidator({
            rules: [                
                { input: '#profecionalTurno', message: 'Debe seleccionar un profesional!', action: 'keyup, blur',
                    rule: function(input, commit){
                            if($('#profecionalTurno :selected').val()){
                                return true;
                            }return false;
                        }					
                },
                { input: '#tratSelect', message: 'Debe seleccionar al menos un tratamiento', action: 'keyup, blur',
                    rule: function(input, commit){
                            if(tratSeleccion.length > 0){
                                return true;
                            }return false;
                        }					
                },
                 { input: '#turnoDescrip2', message: 'Debe ingresar comentario!', action: 'keyup, blur',
                    rule: function(input, commit){
                            if($('#turnoDescrip2').val()){
                                return true;
                            }return false;
                        }					
                }
            ],
            onSuccess: function(){	

                var parametros = {
                    "start" : rangoStart,
                    "end" : rangoEnd,
                    "id_paciente" : get_paciente(),
                    "id_profesional" : $('#profecionalTurno :selected').val(),
                    "descrip" : $("#turnoDescrip2").val().trim(),
                    "id_estadoturno" : 1,
                    "tratamientos": tratSeleccion,
                    "editable" : 'true',
                    "ocupado" : 'false'
                };                                    
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?=base_url();?>calendar/add_turno",
                    data: parametros,
                    beforeSend: function () {
                        $('#formTurnoPaciente').ajaxloader();
                    },
                    success:  function (data) {
                        $('#calendar').fullCalendar('unselect');
                        if (data.valido == false){
                            $('#formTurnoPaciente').ajaxloader('hide');
                            new Messi('No se pudo ingresar el turno en el sistema.', 
                                    {modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
                            );
                        }else{ $('#formTurnoPaciente').ajaxloader('hide');                              
                               $('#turno-paciente').modal('hide');
                               $('#calendar').fullCalendar( 'refetchEvents' );															   
                        }
                    }
                });
            }                                
        });
        //---------------------------------------
        $('#turnos').jqxValidator({
            rules: [
                { input: '#pacientes', message: 'Debe seleccionar un paciente!', action: 'keyup, blur', 
                    rule: function(input, commit){
                            if(get_paciente() != 0 || (get_paciente == 0 && lstPacientes.val())){
                                return true;
                            }return false;
                        }
                },
                { input: '#profesionalesTurno', message: 'Debe seleccionar un profesional!', action: 'keyup, blur',
                    rule: function(input, commit){
                            if(lstProfesionalesTurno.val()){
                            return true;
                            }return false;
                        }					
                }					
            ],
            onSuccess: function(){	

                var parametros = {
                    "start" : rangoStart,
                    "end" : rangoEnd,
                    "id_paciente" : get_paciente(),
                    "id_profesional" : lstProfesionalesTurno.val(),
                    "descrip" : turnoDescrip.val().trim(),
                    "id_estadoturno" : 1,
                    "editable" : 'true',
                    "ocupado" : 'false'
                };                                    
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?=base_url();?>calendar/add_turno",
                    data: parametros,
                    beforeSend: function () {
                        $('#turnos').ajaxloader();
                    },
                    success:  function (data) {
                        $('#calendar').fullCalendar('unselect');
                        if (data.valido == false){
                            $('#turnos').ajaxloader('hide');
                            new Messi('No se pudo ingresar el turno en el sistema.', 
                                    {modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
                            );
                        }else{ $('#turnos').ajaxloader('hide');
                               $('#ventanaEvento').jqxWindow('close');
                               $('#calendar').fullCalendar( 'refetchEvents' );															   
                        }
                    }
                });
            }                                
        });
        
        $('#agendas').jqxValidator({
            rules: [
                { input: '#profesionalesAgenda', message: 'Debe seleccionar un profesional!', action: 'keyup, blur',
                    rule: function(input, commit){
                            if(lstProfesionalesAgenda.val()){
                                return true;
                            }return false;
                    }					
                }					
            ],
            onSuccess: function(){	
                    var parametros = {
                    "start" : rangoStart,
                    "end" : rangoEnd,
                    "id_profesional" : lstProfesionalesAgenda.val(),
                    "descrip" : agendaDescrip.val().trim(),
                    "editable" : 'true',
                    "ocupado" : 'false'
                    };                                    
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?=base_url();?>calendar/add_agenda",
                        data: parametros,
                        beforeSend: function () {
                            $('#agendas').ajaxloader();
                        },
                        success:  function (data) {
                            $('#calendar').fullCalendar('unselect');
                            if (data.valido == false){
                                $('#agendas').ajaxloader('hide');
                                new Messi('No se pudo ingresar la agenda en el sistema.', 
                                    {modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
                                );
                            }else{ $('#agendas').ajaxloader('hide');
                                $('#ventanaEvento').jqxWindow('close');
                                $('#calendar').fullCalendar( 'refetchEvents' );															   
                            }
                        }
                    });
            }                                
        });		
			
        turnoDescrip.jqxInput({
            placeHolder: "Agregar descripcion",
            height: 50,
            width: 270
        });

        agendaDescrip.jqxInput({
            placeHolder: "Agregar descripcion",
            height: 50,
            width: 270
        });   
        //traigo la lista de tratamientos pendientes
        $.ajax({
            async: false,
            url: "<?=base_url();?>tratamientos/load_pendientes_turnos/"+get_paciente(),
            success: function (data, status, xhr) {
                tratPendientes = $.parseJSON(data);
            }
        });
        //FIN lista de tratamientos pendientes
        //armo la dropdown de tratamientos con la api de bootstrap-select
	var htmlSelect ='' ;
        for(var key in tratPendientes){
            var inter = tratPendientes[key].intervencion;
            var sesion = tratPendientes[key].sesion;
            console.log(inter, sesion);
            if(tratPendientes[key].promo){
                htmlSelect = htmlSelect + "<option value="+tratPendientes[key].id+">("+tratPendientes[key].promo+")"+tratPendientes[key].intervencion+" #"+tratPendientes[key].sesion+"</option>";   
            }else{
                htmlSelect = htmlSelect + "<option value="+tratPendientes[key].id+">"+tratPendientes[key].intervencion+" #"+tratPendientes[key].sesion+"</option>";   
            };
	}
	htmlSelect = "<select class='selectpicker' multiple data-done-button='true' data-size='5' title='Seleccionar tratamiento'>"+htmlSelect +'</select>';
	$('#tratSelect').html(htmlSelect);
  //$('#tratSelect :selected').val();

        //FIN dropsown ------------------------        
        
        
            //traigo la lista de pacientes
        $.ajax({
            async: false,
            url: "<?=base_url();?>calendar/load_pacientes",
            success: function (data, status, xhr) {
                pacientes = $.parseJSON(data);
                source = {
                    datatype: "json",
                    datafields: [
                        { name: 'nombre' },
                        { name: 'apellido' },
                        { name: 'email' },
                        { name: 'idPaciente' },
                        { name: 'foto' }
                    ],
                    id: 'id',
                    localdata: $.parseJSON(data)                                                   
                };
            }
        });
        var pacientesDA = new $.jqx.dataAdapter(source);
						
        lstPacientes.jqxDropDownList({ 
            placeHolder: "Seleccionar paciente:",
            source: pacientesDA, 
            displayMember: "nombre", 
            valueMember: "idPaciente", 
            itemHeight: 70, 
            height: 25, 
            width: 270,
            filterable: true,
            filterPlaceHolder: "Buscar...",
            renderer: function (index, label, value) {
                var datarecord = pacientes[index];
                var imgurl = '<?=base_url();?>/img/'+datarecord.foto;
                var img = '<img height="50" width="45" src="' + imgurl + '"/>';
                var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td>' + datarecord.nombre + " " + datarecord.apellido + '</td></tr><tr><td>' + datarecord.email + '</td></tr></table>';
                return table;
            }	  															 

        });             
              //traigo la lista de profesionales
//        $.ajax({
//            async: false,
//            url: "<?=base_url();?>calendar/load_prof",
//            success: function (data, status, xhr) {
//                profesionales = $.parseJSON(data);
//                source = {
//                    datatype: "json",
//                    datafields: [
//                        { name: 'nombre' },
//                        { name: 'idProf' }
//                    ],
//                    id: 'id',
//                    localdata: $.parseJSON(data)
//                };
//            }
//        });
//        var profesionalesDA = new $.jqx.dataAdapter(source);                
	var source =
	{
	    datatype: "json",
	    datafields: [
		{ name: 'nombre' },
		{ name: 'idProf' }
	    ],
	    id: 'id',
	    localdata: profesionales,
	    async: true
	};   
	var profesionalesDA = new $.jqx.dataAdapter(source);      
        //cargo la lisbox con los profesionales
        listBox.jqxListBox({multiple: true, displayMember: "nombre", valueMember: "idProf", width: 200, autoHeight:true });               
        listBox.jqxListBox({ source: profesionalesDA });
                
        // cargo los radiobuttons de estaods de turnos
        $.ajax({
            async: false,
            url: "<?=base_url();?>calendar/estados_turnos",
            success: function (data, status, xhr) {
                estados = $.parseJSON(data);                                        
            }
        });
                
        var estadoHtml ="";  
        var ch =" ";             
                                
        for(var k in estados) {
            if(estados[k].id == "1"){
                ch = " checked ";  
            }else ch =" ";

            estadoHtml = estadoHtml + "<div class='radio'><label><input type='radio' name='eturnos' id='estadoOp"+estados[k].id+"' value= "+estados[k].id+ch+">"+estados[k].nombre+"</label></div>";
        }
        $('#estados').html(estadoHtml);

        $('input[type=radio][name=eturnos]').change(function() {
            estadoTurno = this.value;
            $('#calendar').fullCalendar('refetchEvents' );
        });
        $('input[type=radio][name=verPaciente]').change(function() {
            verPaciente = this.value; 
            $('#calendar').fullCalendar('refetchEvents' );
        });
        
	//Armo la lista desplegable de profesionales con la api de bootstrap-select
	var htmlSelect ='' ;
	var medicoMarca = profesionales[0].medico;
	for(var i in profesionales) {
	    if(profesionales[i].medico !== medicoMarca){
		medicoMarca = profesionales[i].medico;
		htmlSelect = htmlSelect + "<option data-divider='true'></option>";
	    }
	    htmlSelect = htmlSelect + "<option value="+profesionales[i].idProf+">"+profesionales[i].nombre+"</option>";   
	    
	}
	htmlSelect = "<select class='selectpicker' data-size='5' title='Seleccionar profesional'>"+htmlSelect +'</select>';
	$('#profecionalTurno').html(htmlSelect);
       //$('#profecionalTurno :selected').val();
    ///------------------------------------------
	
        lstProfesionalesTurno.jqxDropDownList({ 
            placeHolder: "Seleccionar profesional:",
            source: profesionalesDA, 
            displayMember: "nombre", 
            valueMember: "idProf", 
            itemHeight: 70, 
            height: 25, 
            width: 270,
            filterable: true,
            filterPlaceHolder: "Buscar...",
            renderer: function (index, label, value) {
                var datarecord = profesionales[index];
                var imgurl = '<?=base_url();?>/img/'+datarecord.foto;
                var img = '<img height="50" width="45" src="' + imgurl + '"/>';
                var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td>' + datarecord.nombre + '</td></tr><tr><td>' + datarecord.email + '</td></tr></table>';
                return table;
            }
        });
        lstProfesionalesAgenda.jqxDropDownList({ 
            placeHolder: "Seleccionar profesional:",
            source: profesionalesDA, 
            displayMember: "nombre", 
            valueMember: "idProf", 
            itemHeight: 70, 
            height: 25, 
            width: 270,
            filterable: true,
            filterPlaceHolder: "Buscar...",
            renderer: function (index, label, value) {
                var datarecord = profesionales[index];
                var imgurl = '<?=base_url();?>/img/'+datarecord.foto;
                var img = '<img height="50" width="45" src="' + imgurl + '"/>';
                var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td>' + datarecord.nombre + '</td></tr><tr><td>' + datarecord.email + '</td></tr></table>';
                return table;
            }
        });	    
                //filtrar el calendario segun cada profesional
        listBox.on('change', function () {
            var items = $("#selector").jqxListBox('getSelectedItems');
            window.profs = [];
            for (var i = 0; i < items.length; i++) {
                window.profs[i] = items[i].value;           
            }
            $('#calendar').fullCalendar( 'refetchEvents' );

        });	
                //en la renderizacion de la lista le meto el cuadradito del color 
                //identificativo a cada profesional																					                      
        listBox.jqxListBox({ renderer: function (index, label, value) {	
                var color='';
                for (var i = 0; i < profesionales.length; i++) {
                    if(profesionales[i].idProf == value){														                      			
                        color = profesionales[i].color;
                        return "<span style='background-color:"+ color+";'>" +'&nbsp;&nbsp;&nbsp;'+ "</span>"+"<span>&nbsp;"+label+"</span>";
                    }           
                }																                    
            } 
        });
        
        listBox.jqxListBox('refresh');                									
    
		//crear el calendario/agenda
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next   today',
                center: 'prevYear title nextYear',
                right: 'month,agendaWeek,agendaDay'
            },
            eventLimit: true,
            aspectRatio: 1.8,
            editable: false,
            allDaySlot: true,
            selectable: true,
            selectHelper: true,
            hiddenDays: [], // ocultar dias
            slotDuration: '00:15:00',   
            lang: 'ES',
            theme: true,
            allDayText: 'Todo el dia',
            minTime: '06:00:00',
            themeButtonIcons: {
                prev: 'triangle-1-w',
                next: 'triangle-1-e',
                prevYear: 'seek-prev',
                nextYear: 'seek-end'
            },
            buttonText: {
                today:    'Hoy',
                month:    'Mes',
                week:     'Semana',
                day:      'Dia'
            },
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
                'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles','Jueves', 'Viernes', 'Sabado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],

            //el evento del calendario de cuando pincha y arrastra un nuevo evento
            select: function( start, end, jsEvent, view ) {
                if ((view.name == 'month' && ((end - start)> 86400000))|| (view.name != 'month'))  { 
                    // var title = prompt('Event Title:');
                    // var idProf = prompt('Id del profesional:');
                    //var url = prompt('Url:');
                    // var eventData;
                    rangoStart = start.format();
                    rangoEnd = end.format();
                    var fDesde = new Date(Date.parse(rangoStart));
                    var fHasta = new Date(Date.parse(rangoEnd));
                    var horaFormat = function(fecha){
                        var lpad = function lpad(originalstr, ancho, strToPad) {
                            var x = originalstr;
                            while (x.length < ancho)
                                x = strToPad + x;
                            return x;
                        };		                                        									
                        var xfecha = new Date(fecha);
                        var h = xfecha.getHours();
                        var m = xfecha.getMinutes();
                        return lpad(h.toString(),2,'0')+':'+lpad(m.toString(),2,'0')+"Hs";
                    }; 
                    var fechaFormat = function(fecha,suma){
                        var lpad = function lpad(originalstr, ancho, strToPad) {
                            var x = originalstr;
                            while (x.length < ancho)
                                x = strToPad + x;
                            return x;
                        };		                                        									
                        var xfecha = new Date(fecha);
                        if(suma){
                            xfecha.setDate(xfecha.getDate()+1);
                        }
                        var d = xfecha.getDate();														
                        var m = xfecha.getMonth()+1; 
                        var y = xfecha.getFullYear(); 
                        return lpad(d.toString(),2,'0')+'/'+lpad(m.toString(),2,'0')+'/'+y;
                    }; 
                    switch((fHasta.getDate() - fDesde.getDate())) {
                        case 0:
                            var html = fechaFormat(fDesde,false);
                            $("#fechaTurno").html(html);															
                            $("#fechaAgenda").html(html);
                            $("#fechaPaciente").html(html+ '&nbsp&nbsp' +horaFormat(fDesde)+' - '+horaFormat(fHasta));
                            html = 'de '+horaFormat(fDesde)+' a '+horaFormat(fHasta);															
                            $("#horarioTurno").html(html);
                            $("#horarioAgenda").html(html);
                            break;
                        case 1:
                            var html = fechaFormat(fDesde,true);
                            $("#fechaTurno").html(html);
                            $("#fechaAgenda").html(html);
                            $("#fechaPaciente").html(html+ ' todo el dia');
                            html = 'Todo el dia';
                            $("#horarioTurno").html(html);
                            $("#horarioAgenda").html(html);
                            break;
                        default:
                            var html = 'Desde '+fechaFormat(fDesde,true)+' hasta '+fechaFormat(fHasta,false);
                            $("#fechaTurno").html(html);
                            $("#fechaAgenda").html(html);
                            $("#fechaPaciente").html(html+ ' todo el dia');
                            html = 'Todo el dia';
                            $("#horarioTurno").html(html);
                            $("#horarioAgenda").html(html);

                     }
                    turnoDescrip.val('');
                    $("#turnoDescrip2").val('');
                    agendaDescrip.val('');	
                    lstProfesionalesAgenda.val('');
                    lstProfesionalesTurno.val('');
                    lstPacientes.val('');		

                    if(idPacienteJs == 0){
                        $('#ventanaEvento').jqxWindow('open');
                    }else{
                        $('.selectpicker').selectpicker('deselectAll');
                        $("#turno-paciente").modal();                        
                    }

                }
               $('#calendar').fullCalendar( 'refetchEvents' );
            },
        //el evento del calendario de cuando arrastra un evento existente
            eventDrop: function(event, delta, revertFunc) {  
                if (!confirm("Confirma modificar el evento?")) {
                    revertFunc();
                } else{                                                                     
                    if(event.tipo == 'turno'){
                        $.ajax({  
                            url: "<?=base_url();?>calendar/update_turno",
                            data: 'id='+ event.id +
                                '&start='+ event.start.format() +
                                '&end='+ event.end.format()+
                                '&id_profesional='+event.idProfesional+
                                '&id_paciente='+event.idPaciente+
                                '&descrip='+event.descrip+
                                '&id_estadoturno='+event.idEstadoTurno+
                                '&editable='+event.editable+
                                '&ocupado='+event.ocupado,
                            type: "POST",
                            success: function(json) {
                                if(json =='false'){  
                                    alert('No se permite solapamiento en un evento existente');
                                }      
                            }
                        }); 
                    }else{
                        $.ajax({  
                            url: "<?=base_url();?>calendar/update_agenda",
                            data: 'id='+ event.id +
                                '&start='+ event.start.format() +
                                '&end='+ event.end.format()+
                                '&id_profesional='+event.idProfesional+
                                '&descrip='+event.descrip+
                                '&editable='+event.editable+
                                '&ocupado='+event.ocupado,
                            type: "POST",
                            success: function(json) {
                                if(json =='false'){  
                                    alert('No se permite solapamiento en un evento existente');
                                }      
                            }
                        });														  
                    }                                 
                }
            },
            //el evento del calendario de cuando 'agrando/achico' un evento existente y se extiende o acorta el periodo
            eventResize: function(event, delta, revertFunc) {   
                if (!confirm("Confirma modificar el evento?")) {
                    revertFunc();
                } else{
                    if(event.tipo == 'turno'){
                        $.ajax({  
                            url: "<?=base_url();?>calendar/update_turno",
                            data: 'id='+ event.id +
                                '&start='+ event.start.format() +
                                '&end='+ event.end.format()+
                                '&id_profesional='+event.idProfesional+
                                '&id_paciente='+event.idPaciente+
                                '&descrip='+event.descrip+
                                '&id_estadoturno='+event.idEstadoTurno+
                                '&editable='+event.editable+
                                '&ocupado='+event.ocupado,
                            type: "POST",
                            success: function(json) {
                                if(json =='false'){  
                                    alert('No se permite solapamiento en un evento existente');
                                }      
                            }
                        }); 
                    }else{
                        $.ajax({  
                            url: "<?=base_url();?>calendar/update_agenda",
                            data: 'id='+ event.id +
                                '&start='+ event.start.format() +
                                '&end='+ event.end.format()+
                                '&id_profesional='+event.idProfesional+
                                '&descrip='+event.descrip+
                                '&editable='+event.editable+
                                '&ocupado='+event.ocupado,
                            type: "POST",
                            success: function(json) {
                                if(json =='false'){  
                                    alert('No se permite solapamiento en un evento existente');
                                }      
                            }
                        });														  
                    }                                        
                }
            },
             //Cargo el calendario con los eventos registrados en la base de datos
             //Se traen los datos y se carga el calendario solo con los eventos necesarios segun
             //la visibilidad seleccionada del calendario y segun el profesional seleccionado
            eventSources: [ 
            // traigo los turnos
                function(start, end, timezone, callback){                     
                    $.ajax({
                        url: "<?=base_url();?>calendar/load_turnos",																	            
                        data: 'f1='+start.format()+'&f2='+end.format()+'&selections='+window.profs+'&estado='+estadoTurno+'&paciente='+idPacienteJs+'&verPaciente='+verPaciente,
                        type: 'POST',
                        dataType: 'json',
                        success: function(doc, status, xhr ) {
                            var events = [];
                            $(doc).each(function() {
                                events.push({
                                    id: $(this).attr('id'),
                                    title: $(this).attr('title'),
                                    start: $(this).attr('start'),
                                    end: $(this).attr('end'),
                                    color: $(this).attr('color'),                                                                                                               
                                    idProfesional: $(this).attr('id_profesional'),
                                    idEstadoTurno: $(this).attr('id_estadoturno'),
                                    idPaciente: $(this).attr('id_paciente'),
                                    ocupado: $(this).attr('ocupado'),
                                    descrip: $(this).attr('descrip'),
                                    editable: $(this).attr('editable'),
                                    tipo: 'turno'
                                });
                            });
                            callback(events);
                        }
                    })
                },
                //traigo las agendas
                function(start, end, timezone, callback){
                    var view = $('#calendar').fullCalendar('getView');
                    $.ajax({
                        url: "<?=base_url();?>calendar/load_agendas",																	            
                        data: 'f1='+start.format()+'&f2='+end.format()+'&selections='+window.profs+'&vista='+view.name,
                        type: 'POST',
                        dataType: 'json',
                        success: function(doc, status, xhr ) {
                            var events = [];
                            $(doc).each(function() {                                																	                	
                                events.push({
                                    id: $(this).attr('id'),
                                    title: $(this).attr('title'),
                                    start: $(this).attr('start'),
                                    end: $(this).attr('end'),
                                    editable: $(this).attr('editable'),
                                    ocupado: $(this).attr('ocupado'),
                                    color: $(this).attr('color'),                                                                                                               
                                    idProfesional: $(this).attr('id_profesional'),                                                                                                                                                                                                                                
                                    descrip: $(this).attr('descrip'),
                                    idPaciente: null,
                                    tipo: 'agenda'
                                });
                            });
                            callback(events);
                        }
                    }) 
                }                                                                                                                       
            ], 
        //Al hacer clic en una fecha en modo MES, pongo la visualizacion
        //del DIA
            dayClick: function(date, jsEvent, view) {                               
                if (view.name == 'month') { 
                    $('#calendar').fullCalendar( 'changeView', 'agendaDay'); 
                    $('#calendar').fullCalendar( 'gotoDate', date); 
                }
            },
        //En la renderizacion de cada evento le meto el widget qtip2 del cartelito
        //con la animacioncita de desplegacion
            eventRender: function( event, element, view ) { 
                var start = Date.parse(event.start.format());
                var end = Date.parse(event.end.format());                                        
                var fechaFormat = function(fecha){
                    var lpad = function lpad(originalstr, ancho, strToPad) {
                        var x = originalstr;
                        while (x.length < ancho)
                            x = strToPad + x;
                        return x;
                    };		                                        									
                    var xfecha = new Date(fecha);
                    var h = xfecha.getHours();
                    var m = xfecha.getMinutes();
                    return lpad(h.toString(),2,'0')+':'+lpad(m.toString(),2,'0')+"Hs";
                }; 
                var horaComienzo = fechaFormat(start);
                var horaFin = fechaFormat(end);
                var tipo = function(t){
                    if(t == 'turno'){
                        return 'Turno:';
                    }else return 'Agenda:';
                };
                var text = '<span class="letraTip2">'+tipo(event.tipo)+'</span></br><p>'+event.descrip+' </p><br/><p><span class="letraTip2">Comienzo: </span>'+horaComienzo+'<br/><span class="letraTip2">Fin: </span>'+horaFin+'</p>';
                element.qtip({																								          
                    content: {title: event.title,
                        text: text},
                    position: {
                        target: 'mouse', // Track the mouse as the positioning target
                        adjust: { x: 5, y: 5 } // Offset it slightly from under the mouse
                     },
                    show: {
                        effect: function() {
                            $(this).slideDown();
                        }
                    },
                    hide: {
                        effect: function() {
                            $(this).slideUp();
                        }
                    },
                    style: {
                        classes: 'qtip-light qtip-rounded qtip-shadow letraTip',	
                        widget: true
                    }																														   		
                });																			    
            }
        }); //FIN FULLCALENDAR
	
        $('#ventanaEvento').jqxWindow({
            isModal: true,
            animationType: 'fade',
            autoOpen: false,
            modalOpacity : 0.5,
            theme: 'ui-redmond',
            maxHeight: 480, 
            maxWidth: 390, 
            minHeight: 480, 
            minWidth: 380, 
            height: 480, 
            width: 380,
            initContent: function () {
                $('#tab').jqxTabs({ height: '100%', width:  '100%'});
                $('#ventanaEvento').jqxWindow('focus');
            }
        });	
  });

</script>   
</head>
<style>    
    div.container { max-width: 1200px }  
    .turno-paciente{
        max-width: 200px;
    }
   .modal-dialog {
        max-width: 450px;
    }
    @media(max-width:768px) {
	.modal-dialog {
            max-width: 300px;
        }
    }
    #volver{
        width:200px;
        padding-bottom: 30px;
    }
    #volverBtn{
        min-width: 200px;
    }  
</style>
<body style="margin-top: 0px;">
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div id='mainCont'>
                    <div class="col-lg-2"> 
                        <div class="lista-pacientes">
                            <div id="volver">
                                <button id="volverBtn" type="button" class="btn btn-primary">Volver</button>
                            </div>
                            <div id='selector'> </div>   
                        </div>
                        <div id='estados' class="form-group lista-estados"></div> 
                        <div class='turno-paciente'>
                            <h4><span id='nombre-turno'></span></h4>
                            <img class="img-responsive img-rounded" id="fotoPerfil">
                            <div id="ver_paciente">
                                <div class="radio">
                                  <label><input type="radio" name="verPaciente" value="1">Paciente</label>
                                </div>
                                <div class="radio">
                                  <label><input type="radio" name="verPaciente" value="2" checked>Todos</label>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">   
                        <div id='calendar' class="fc fc-ltr fc-unthemed"></div>	
                    </div> 						
                </div>
            </div>
        </div>
        <div  id="mainDemoContainer">
            <div id="ventanaEvento">
                <div id="windowHeader">
                    <span><img src="<?=base_url();?>img/calendarIcon.png" alt="" style="margin-right: 15px" />Evento</span>
                </div>
                <div style="overflow: hidden;" id="windowContent">
                    <div id="tab">
                        <ul style="margin-left: 30px;">
                            <li>Nuevo Turno</li>
                            <li>Nueva Agenda</li>
                        </ul>
                        <div>
                            <form id="turnos" action="./">
                                <table class="turnos-table">
                                    <tr><td></br></td></tr>
                                    <tr>
                                        <td>Horario:</td>
                                        <td><div id="fechaTurno"></div></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div id="horarioTurno"></div>
                                        </td>                                                   
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>Paciente:</td>
                                        <td><div id="pacientes"></div></td>
                                    </tr>
                                    <tr>
                                        <td>Profesional:</td>
                                        <td><div id="profesionalesTurno"></div></td>
                                    </tr>
                                    <tr>
                                        <td>Descripcion:</td>
                                        <td><textarea  id="turnoDescrip">                                                                             
                                            </textarea >
                                        </td>
                                    </tr>
                                    <tr><td></br></td></tr>
                                    <tr>
                                        <td>&nbsp; </td>
                                        <td> 
                                            <input type="button" value="Confirmar" id="btnConfirmarTurno" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="button" value="Cancelar" id="btnCancelarTurno" />
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <div>
                            <form id="agendas" action="./">
                                <table class="turnos-table">
                                    <tr><td></br></td></tr>
                                    <tr>
                                        <td>Horario:</td>
                                        <td><div id="fechaAgenda"></div></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div id="horarioAgenda"></div>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>Profesional:</td>
                                        <td><div id="profesionalesAgenda"></div></td>
                                    </tr>
                                    <tr>
                                        <td>Descripcion:</td>
                                        <td><textarea  id="agendaDescrip"></textarea >
                                        </td>
                                    </tr>
                                    <tr><td></br></td></tr>
                                    <tr>
                                        <td>&nbsp; </td>
                                        <td>
                                            <input type="button" value="Confirmar" id="btnConfirmarAgenda" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="button" value="Cancelar" id="btnCancelarAgenda" />
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="turno-paciente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content well well-lg col-lg-12 col-sm-12 col-xs-12">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="row">
			    <div class="col-lg-12">
				<h4 class="modal-title">Nuevo Turno</h4>
			    </div>			    
			</div>			
                    </div>
                    <div class="modal-body">
			<div class="row">
			    <div class="col-lg-12 col-sm-12 col-xs-12">
				<h5><span id='nombre-turno2'></span></h5>
			    </div>			    
			</div>
                        <div class="row">
                            <div class="well well-sm well-lg">
                                <form id="formTurnoPaciente" role="form">
                                    <fieldset>
                                        <div class="form-group col-lg-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-xs-6">						
                                                    <div id="fechaPaciente" style="color: #b32f2f"></div> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-4 col-xs-12">Profesional:</div>
                                                <div id="profecionalTurno" class="col-lg-8 col-sm-8 col-xs-12"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-4 col-xs-12">Tratamientos:</div>
                                                <div id="tratSelect" class="col-lg-8 col-sm-8 col-xs-12"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-4 col-xs-12">Descripci&oacute;n:</div>
                                                <div class="col-lg-8 col-sm-8 col-xs-12"><textarea class="col-lg-12 col-sm-12 col-xs-12" id="turnoDescrip2"></textarea ></div>                                           
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnAceptarTurno" type="button" class="btn btn-primary">Aceptar</button>
                        <button id="btnCancelTurno" type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>  		
    </div>
</div>
</body>
</html>