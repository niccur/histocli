<script type="text/javascript">

	$(document).ready(function () 
	{
		var theme2 = 'energyblue';
		
		$('#cerrar_sesion').jqxButton({ width: 120, height: 30 , theme:theme2});
		$('#clave').jqxButton({ width: 120, height: 30 , theme:theme2});
		
		$('#cerrar_sesion').bind('click', function () {
				location.replace("<?=base_url();?>login/logout");
		});
		
		$('#clave').bind('click', function () {
				location.replace("<?=base_url();?>login/cambio_clave");
		});
		
            // Create jqxTree
            var source = [
                { icon: "<?=base_url();?>img/folder.png", value: "main/en_construccion", label: "Mantenimiento", expanded: true, items: [
                    { icon: "<?=base_url();?>img/folder.png", value: "main/en_construccion", label: "Area", selected: true },
                    { icon: "<?=base_url();?>img/folder.png", value: "main/en_construccion", label: "Departamentos" },
                    { icon: "<?=base_url();?>img/folder.png", value: "main/en_construccion", label: "Diagnosticos" },
                    { icon: "<?=base_url();?>img/folder.png", value: "main/en_construccion", label: "Obras Sociales" },
                    { icon: "<?=base_url();?>img/folder.png", value: "main/en_construccion", label: "Pacientes" },
                    { icon: "<?=base_url();?>img/folder.png", value: "main/en_construccion", label: "Usuarios" },
                    { icon: "<?=base_url();?>img/folder.png", value: "main/en_construccion", label: "Profesionales" },
                    { icon: "<?=base_url();?>img/calendarIcon.png", value: "main/en_construccion", label: "Horarios de Profesionales" },
                   ]
                },
                { icon: "<?=base_url();?>img/calendarIcon.png", value: "main/load_calendar", label: "Calendario", expanded: true, items: [
                   { icon: "<?=base_url();?>img/calendarIcon.png", value: "main/en_construccion", label: "Nuevo Turno" },
                   { icon: "<?=base_url();?>img/calendarIcon.png", value: "main/en_construccion", label: "Agendas" },
                ]
                },
                { icon: "<?=base_url();?>img/notepad.png", value: "main/en_construccion", label: "Reportes", expanded: true, items: [
                   { icon: "<?=base_url();?>img/notepad.png", value: "main/en_construccion", label: "Historias Clinicas" }
                ]
                },
             ];

            // create jqxTree
            $('#jqxTree').jqxTree({ source: source, width: '250px', height: '400px'});
            $('#jqxTree').on('select', function (event) {
                var args = event.args;
                var item = $('#jqxTree').jqxTree('getItem', args.element);
                //$('#Events').jqxPanel('prepend', '<div style="margin-top: 5px;">Selected: ' + item.label + '</div>');
               
                location.replace("<?=base_url();?>"+item.value);
            });
             
             
             
	});
</script>

<style> 
        #main { margin: 0 auto; } 
        #mainButtons {width: 300px; height: 130px; margin-left:0px} 
</style> 
		
<body>
	<div id="main">  
            <div align="left" id="titulo" >MENU PRINCIPAL </div>               
            <div align="left" id="jqxTree"> </div>
		<div align="center" id="mainButtons"> 
				<div align="left"> <input type="button" value="Salir" id="cerrar_sesion"  /> </div>
				<div align="left"> <input type="button" value="Cambiar Clave" id="clave"  /> </div>
		</div>
	</div>
</body>