<!DOCTYPE html>
<html lang="en">
<head>
	
<meta charset='utf-8' />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">	

		<!-- Custom CSS -->
                                
		<link href="<?=base_url();?>css/encabezado.css" rel="stylesheet" type="text/css">

		<link rel="stylesheet" href="<?=base_url();?>css/bootstrap.min.css" >
		<link rel="stylesheet" href="<?=base_url();?>js/bootstrap3-dialog-master/dist/css/bootstrap-dialog.min.css" >
			
	        <link rel="stylesheet" href="<?=base_url();?>js/jqwidgets/styles/jqx.base.css" type="text/css" />
		<link href="<?=base_url();?>js/jqwidgets/styles/jqx.bootstrap.css" rel="stylesheet"> 
			
		<!-- MetisMenu CSS -->
		<link href="<?=base_url();?>css/metisMenu.min.css" rel="stylesheet" type="text/css">

		<!-- Custom Fonts -->
		<link rel="stylesheet" href="<?=base_url();?>css/font-awesome-4.5.0/css/font-awesome.css">
		
		<script type="text/javascript" src="<?=base_url();?>js/jquery-1.10.2.min.js"></script> 

		<!-- Bootstrap Core JavaScript -->
		<script type="text/javascript" src="<?=base_url();?>js/bootstrap.min.js"></script> 
		<script type="text/javascript" src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script> 
		<script type="text/javascript" src="<?=base_url();?>js/metisMenu.min.js"></script>
	    <script type="text/javascript" src="<?=base_url();?>js/sb-admin-2.js"></script>
		
	 	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxcore.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxdata.js"></script>
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxscrollbar.js"></script>
		<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxtree.js"></script>
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxbuttons.js"></script>
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxpanel.js"></script>  
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxlistbox.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxradiobutton.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxwindow.js"></script>      
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxtabs.js"></script>     			
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxdropdownlist.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxinput.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxnumberinput.js"></script>
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxtooltip.js"></script>       
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxdatetimeinput.js"></script> 
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxcalendar.js"></script> 
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/globalization/globalize.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxrangeselector.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxvalidator.js"></script>
	
	<!-- bootstrap-select https://silviomoreto.github.io/bootstrap-select/ -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="<?=base_url();?>js/bootstrap-select-1.12.4/dist/css/bootstrap-select.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="<?=base_url();?>js/bootstrap-select-1.12.4/js/bootstrap-select.js"></script>

    
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
		
	<script type="text/javascript"> 
		var calendario;
		$(document).ready(function () {
			$.ajax({
                        async: false,
                        url: "http://localhost/histocli/pruebas/getEvents",
                        success: function (data, status, xhr) {
                            calendario = $.parseJSON(data);  
							console.log(calendario);                          
                        }
                    });     
		});

	</script>

</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

		<p>If you would like to edit this page you'll find it located at:</p>
		<code>application/views/welcome_message.php</code>

		<p>The corresponding controller for this page is found at:</p>
		<code>application/controllers/welcome.php</code>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>

</body>
</html>