<!DOCTYPE html>
<html>  	
    <body>
	<div id="wrapper">             
            <div id="page-wrapper">                    
                <div class="panel panel-default" id="filiatorios">
                    <div class="panel-heading">
                        <h4><spam id="nombreH4"></spam>  </h4>
                    </div>
                    <div class="panel-body">
                        <div id="rowFiliatorio"  class="row">                                        
                                <form id="formFiliatorio" role="form">
                                    <fieldset>                                                
                                        <div class="form-group">
                                            <div class="col-md-12">   
                                                <div class ="row">
                                                    <div class="col-md-6">
                                                        <label>Fecha Ingreso:</label>
                                                        <div class="input-group date">
                                                          <input id="fec_ingreso" name="fec_ingreso" class="form-control input-append date" data-date-format="dd/mm/yyyy"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                        </div>  
                                                    </div>
                                                    <div class="col-md-6">
                                                            <div class="col-md-4">
                                                                <label>DNI:</label>
                                                                <input name="dni" id="nroDni" class="form-control" name="dni" value="" onkeypress="return isNumberKey(event)">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Obra Social:</label>
                                                                <input name="obrasocial" id ="txtObrasocial" class="form-control"> 
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Nro. Afiliado:</label>
                                                                <input name="afiliado" id ="nroAfiliado" class="form-control"> 
                                                            </div>                                                            
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="col-lg-6">   
                                                <label>Nombre:</label>
                                                <input name="nombre" id ="txtNombre" class="form-control">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Apellido:</label>
                                                <input name="apellido" id ="txtApellido" class="form-control">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Fecha Nacimiento:</label>
                                                <div class="input-group date">
                                                  <input id="fec_nac" name="fec_nac" class="form-control input-append date" data-date-format="dd/mm/yyyy"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Edad:</label>
                                                <input name="edad" id="txtEdad" class="form-control" name="edad" min="1" max="120" value="" onkeypress="return isNumberKey(event)">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Ocupacion:</label>
                                                <input name="ocupacion" id ="txtOcupacion" class="form-control">                                                            
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Hijos: </label>
                                                <input name="hijos" id="txtHijos" class="form-control" name="hijos" min="0" max="20" value="" onkeypress="return isNumberKey(event)">                                                
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Direcci&oacute;n:</label>
                                                <input name="direccion" id ="txtDireccion" class="form-control">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Codigo Postal:</label>
                                                <input name="codpostal" id ="txtCodPostal" class="form-control">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Tel&eacute;fono:</label>
                                                <input name="telefono" id ="txtTelefono" type="tel" class="form-control" onkeypress="return isNumberKeyPhone(event)">  
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Celular:</label>
                                                <input name="celular" id ="txtCelular" type="tel" class="form-control" onkeypress="return isNumberKeyPhone(event)">                                                             
                                            </div> 
                                            <div class="col-lg-6">
                                                <label>Notas: </label>
                                                <textarea name="notas" id="txtNotas" class="form-control" rows="3"></textarea>                                                       
                                            </div> 
                                            <div class="col-lg-6">                                                    
                                                <label>Email:</label>                                                            
                                                <input name="email" id ="txtEmail" name="email" class="form-control">
                                            </div>
                                            <div class="col-lg-6">  
                                                <label>Sexo: </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="sexo" id="opcionMasculino" value="M">Masculino
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="sexo" id="opcionFemenino" value="F">Femenino
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>                                                  
                                </form>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button id="btnFiliatorio" type="button" class="btn btn-primary">Guardar</button>
                        <button id="btnCancelFiliatorio" type="button" class="btn btn-primary">Volver</button>                        
                    </div>
                </div>
            </div>
        </div>
    </body>  
      <script type="text/javascript" src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>       
        <script type="text/javascript" src="<?=base_url();?>js/bootstrap-datepicker-master/js/bootstrap-datepicker.js"></script>       
        <script type="text/javascript" src="<?=base_url();?>js/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.es.min.js"></script>   
        <script type="text/javascript" src="<?=base_url();?>application/views/histofiliatorio.js"></script>  
        <script type="text/javascript" src="<?=base_url();?>application/views/dataValidator.js"></script>  
         
        <script type="text/javascript">    
             var cambioFiliatorio = false;             
             var jsonFiliatorio = null;            
             var vSexo;
             var urlBase = "<?php echo base_url(); ?>";
             var idPacienteJs = 0;
             var urlFiliatorioRun = null;            
             var valido_fecha = true;
             $("#formFiliatorio").jqxValidator({
                    hintType: 'label',
                    animationDuration: 500,        
                    rules: [
                        {input: '#txtNombre', 
                        message: 'Debe ingresar el nombre !', 
                        action: 'keyup, blur', 
                        rule: 'required'
                        },
                        {input: '#txtEmail',
                        message: 'Debe ingresar un mail valido !', 
                        action: 'keyup, blur', 
                        rule: 'email'                            
                        },
                        {input: '#txtApellido', 
                        message: 'Debe ingresar el apellido !', 
                        action: 'keyup, blur', 
                          rule: 'required'
                        }, 
                        {input: '#nroDni', 
                        message: 'Debe ingresar el dni !', 
                        action: 'keyup, blur', 
                          rule: 'required'
                        },  
                        {input: '#opcionMasculino', 
                        message: 'Debe seleccionar el sexo !', 
                        action: 'keyup, blur', 
                            rule: function(input, commit){
                                  if($("#opcionMasculino :checked").length === 0 && $("#opcionFemenino :checked").length === 0 ){      
                                         return true;
                                     }
                                        return false;
                                }
                        },
                        {input: '#fec_nac', 
                        message: 'Fecha Incorrecta!', 
                        action: 'keyup, blur', 
                            rule: function(input, commit){                                
                                     return valido_fecha;
                                }
                        },
                        {input: '#fec_ingreso', 
                        message: 'Fecha Incorrecta!', 
                        action: 'keyup, blur', 
                            rule: function(input, commit){                                
                                     return valido_fecha;
                                }
                        }          
                        ], 
                    onSuccess: function(){          
                        var parametros = { 
                            "idPaciente" :idPacienteJs ,
                            "nombre" :  $("#txtNombre").val().trim(),
                            "apellido" : $("#txtApellido").val().trim(),
                            "telefono" : $("#txtTelefono").val(),
                            "celular" : $("#txtCelular").val(),
                            "fecha_nac" : $("#fec_nac").val(),
                            "fecha_ingreso" : $("#fec_ingreso").val(),
                            "notas" : $("#txtNotas").val(),
                            "email" :  $("#txtEmail").val(),
                            "sexo" : vSexo,
                            "edad":  $("#txtEdad").val(),
                            "ocupacion": $("#txtOcupacion").val(),
                            "direccion": $("#txtDireccion").val(),
                            "codpostal": $("#txtCodPostal").val(),
                            "hijos": $("#txtHijos").val(),
                            "obra_social": $("#txtObrasocial").val(),
                            "nro_afiliado": $("#nroAfiliado").val(),
                            "dni": $("#nroDni").val(),
                            "foto": "falta la foto.jpg" 
                        };                                    
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: urlBase + urlFiliatorioRun,
                            data: parametros,
                            success:  function (data) {                                    
                                if(!data.valido){
                                    console.log("error");
                                    idPacienteJs = 0;
                                    cargarFiliatorio();
                                    BootstrapDialog.show({
                                            type: BootstrapDialog.TYPE_DANGER,
                                            title: 'Error!!',
                                            message: 'Los cambios no pudieron ser salvados',
                                            draggable: true,								
                                            buttons: [{
                                                        label: 'Ok',
                                                        action: function(dialogRef){dialogRef.close();}
                                                      }]
                                    });
                                }else{ //$('#turnos').ajaxloader('hide');
                                    console.log("anduvo ok");
                                    idPacienteJs = data.id;                                        
                                    cambioFiliatorio = false;
                                    cargarFiliatorio();
                                    BootstrapDialog.show({
                                            onshown: function(dialogRef){
                                                            setTimeout(function(){
                                                                dialogRef.close();                                                
                                                            }, 2000);
                                                    },
                                            onhide: function(dialogRefh){
                                                //window.location = urlBase+'fichapacientes';
                                                location.replace(document.referrer); //haciendo así hace el back con el refresco de la pantalla
                                            },
                                            type: BootstrapDialog.TYPE_SUCCESS,
                                            title: 'Informaci&oacute;n',
                                            message: 'Los cambios fueron salvados satisfactoriamente',
                                            draggable: true,								
                                            buttons: [{
                                                        label: 'Ok',
                                                        action: function(dialogRef){
                                                            dialogRef.close(); 
                                                        }
                                                    }]
                                    });                     
                                 }
                           },
                           error: function(xhr,status,error){
                               console.log("Hay error "+error);
                           }
                        });
                  },
                  onError: function (){
                      console.log("hay error onError");
                  }
                });
            $(document).ready(function () { 
                                
                idPacienteJs =  <?php echo $idPaciente ;?>;                    
                
                cargarFiliatorio();
            //FORM FILIATORIO------------------------------------------
                $('input[name="sexo"]').change(function(){
                   //var genderValue = $(this).next('label').text()
                   vSexo = $(this).val();
                });                  
            
               $('#rowFiliatorio input').change(function() { 
                    if(cambioFiliatorio===false){
                        cambioFiliatorio = true;
//                        $("#btnCanelFiliatorio").removeClass("disabled");
//                        $("#btnFiliatorio").removeClass("disabled");
                    }    
               }); 
               $('#rowFiliatorio textarea').change(function() { 
                    if(cambioFiliatorio===false){
                        cambioFiliatorio = true;
                    }    
               });   
               
                $('#fec_nac').change(function(){
                    valido_fecha = dateValidator($(this));
                    $('#formFiliatorio').jqxValidator('validateInput', '#fec_nac'); 
                });
               
                $('#fec_ingreso').change(function(){
                    valido_fecha = dateValidatorNN($(this));
                    $('#formFiliatorio').jqxValidator('validateInput', '#fec_ingreso'); 
                });
                
               $("#btnCancelFiliatorio").click(function() {                   
                    cargarFiliatorio();
                    window.history.back();
                });

                $('#btnFiliatorio').on('click', function(){	
                    if(cambioFiliatorio){
                        $('#formFiliatorio').jqxValidator('validate');                        
                    }                    
                });
                
                    $('.input-group.date').datepicker({
                        format: "dd/mm/yyyy",
                        language: "es",
                        clearBtn: true,
                        autoclose: true,
                        startDate: '01/01/1900',
                        endDate: '01/01/5000'
                    });
		
                
            });
        </script>
</html>	
      