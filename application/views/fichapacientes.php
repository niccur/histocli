 <!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/media/css/jquery.dataTables.css" >
    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/css/dataTables.responsive.css" >             	
    
    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/js/dataTables.responsive.min.js"></script>

    <script>
        var anterior = 0;
        var paciente = 0;
        var pacienteNombre = "";
        var urlBase = "<?php echo base_url(); ?>"; 
        var accesoUsuario = "<?php echo $acceso; ?>"; 
        var botonera = '';
        $(document).ready(function() {	
            if(accesoUsuario==4){
                botonera = '<div class= "row">\n\
                                    <div class="col-lg-11 col-sm-11 col-xs-12">\n\
                                        <button type="button" class="historia btn btn-outline col-lg-4 col-sm-4 col-xs-4" data-toggle="tooltip" title="Historia clínica"><i class="fa fa-hospital-o fa-lg" style="color:#286090"></i></button>\n\
                                        <button type="button" class="tratamientos btn btn-outline col-lg-4 col-sm-4 col-xs-4" data-toggle="tooltip" title="Turnos"><i class="fa fa-medkit fa-lg" style="color:#286090"></i></button>\n\
                                        </div>\n\
                                </div>'
            }else botonera = '<div class= "row">\n\
                                    <div class="col-lg-11 col-sm-11 col-xs-12">\n\
                                        <button type="button" class="historia btn btn-outline col-lg-4 col-sm-4 col-xs-4" data-toggle="tooltip" title="Historia clínica"><i class="fa fa-hospital-o fa-lg" style="color:#286090"></i></button>\n\
                                        <button type="button" class="tratamientos btn btn-outline col-lg-4 col-sm-4 col-xs-4" data-toggle="tooltip" title="Turnos"><i class="fa fa-medkit fa-lg" style="color:#286090"></i></button>\n\
                                        <button type="button" class="btn-eliminar btn btn-outline col-lg-4 col-sm-4 col-xs-4" data-toggle="tooltip" title="Eliminar paciente"><i class="fa fa-trash-o fa-lg" style="color:red"></i></button>\n\
                                        </div>\n\
                                </div>';



            $('#altas').on('click', function(){
                var url = "<?php echo base_url().'fichapacientes/new_filiatorio';?>";
                $(location).attr('href',url); 
            });
            var table = $('#pacientes').DataTable( {
                "sAjaxSource": "<?=base_url();?>fichapacientes/load_pacientes",
                "sAjaxDataProp": "",
                "info": false,
                select: {
                    style: 'os'
                },
                language: {
                    "sProcessing":     "Procesando...",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "No hay registros" ,
                    "search": "_INPUT_",	
                    "searchPlaceholder": "Buscar..."													
                },
                "responsive": false,
                "scrollY": 300,     
                "paging": false,
                "columns": [
                    {
                     "className": 'details-control',
                     "orderable": false,
                     "data": null,
                     "defaultContent": '',
                     "orderable": false,
                     "render": function () {
                         return '<i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i>';
                     },
                     "width":"15px"
                     },
                    { "data": "idPaciente"},    
                    { "data": "nombre" },
                    { "data": "apellido" },
                    { "data": "telefono", "orderable": false },
                    { "orderable": false, 
                        data: null,
                        width: 90,
                        className: "center",
                        defaultContent: botonera
                    }
                ],
                "columnDefs": [
                    {
                        "targets": [ 1 ],
                        "visible": false,
                        "searchable": false
                    }
                ]
            }) ;
            $('#pacientes tbody').on( 'click', 'button.btn-eliminar', function (e) {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                idPaciente = row.data().idPaciente;
                BootstrapDialog.show({
                 type: BootstrapDialog.TYPE_WARNING,
                 title: '¡Atenci&oacute;n!',
                 message: "El paciente puede contener historias clínicas, si se elimina se perderan todos sus datos. ¿Confirma eliminar el paciente seleccionado?",
                 draggable: true,								
                 buttons: [{
                         label: 'Si',
                         action: function(dialogRef){   
                             var url = urlBase + "fichapacientes/eliminar_paciente/"+idPaciente;                                                     
                             $.ajax({
                                 type: "POST",
                                 dataType: "json",
                                 url: url,
                                 error: function(err){
                                     if(err.status == 200){
                                        console.log('hay error1');
                                     }else{
                                        console.log('hay error2');
                                     } 
                                     console.log(err); 
                                    location.reload();
                                 },                          
                                 success:  function (data) {
                                    console.log(data);
                                     if(data.valido!=7){
                                         BootstrapDialog.show({
                                             type: BootstrapDialog.TYPE_DANGER,
                                             title: 'Error!!',
                                             message: 'No se pudo eliminar el paciente',
                                             draggable: true,								
                                             buttons: [{
                                                     label: 'Ok',
                                                     action: function(dialogRef){dialogRef.close();}
                                                 }]
                                         });  
                                     }else{                                                                   
                                         location.reload();
                                     }                                                            
                                 }
                             });
                             dialogRef.close();
                         }
                     },
                     {
                         label: 'No',
                         action: function(dialogRef){dialogRef.close();}
                     }
                 ]
             });
        
         }); 

             $('#pacientes tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var tdi = tr.find("i.glyphicon");
                var row = table.row(tr);
                paciente = row.data().idPaciente;
                pacienteNombre = row.data().nombre + " " + row.data().apellido;
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                    tdi.first().removeClass('glyphicon-arrow-up');
                    tdi.first().addClass('glyphicon-arrow-down');
                }
                else {
                    // Open this row
                    if(anterior != 0 && anterior != tr){
                        var rowant = table.row(anterior);
                        var tdiant = anterior.find("i.glyphicon");
                        rowant.child.hide();                        
                        anterior.removeClass('shown');
                        tdiant.first().removeClass('glyphicon-arrow-up');
                        tdiant.first().addClass('glyphicon-arrow-down');                    
                    }; 
                    anterior = tr;
                    row.child(format(row.data())).show();                    
                    tr.addClass('shown');
                    tdi.first().removeClass('glyphicon-arrow-down');
                    tdi.first().addClass('glyphicon-arrow-up');
                }
             });

            table.on("user-select", function (e, dt, type, cell, originalEvent) {
             if ($(cell.node()).hasClass("details-control")) {
                 e.preventDefault();
             }
            });
            function format(d){        
                // `d` is the original data object for the row
                var retorno ='<div class="panel panel-default">';
                if(d.notas){
                    retorno = retorno + '<div class="panel-body">'+d.notas+'</div>';          
                }  
                    retorno = retorno + '<div class="panel-footer">\n\
                                            <div class="btn-group btn-group-justified btn-group-md">\n\
                                                <div class="btn-group"><button type="button" class="btn btn-success btn-datos">Datos</button></div>\n\
                                                <div class="btn-group"><button type="button" class="btn btn-success btn-anamnesis">Anamnesis</button></div>\n\
                                                <div class="btn-group"><button type="button" class="btn btn-success btn-medidas">Medidas</button></div>\n\
                                            </div>\n\
                                        </div>';              
                return retorno+'</div>';             
                
           }            
         $('#pacientes tbody').on('click', '.btn-datos', function (e) {
             window.location.href = "<?php echo base_url().'fichapacientes/load_filiatorio/';?>"+paciente;
             return false;
         });  
         $('#pacientes tbody').on('click', '.btn-anamnesis', function (e) {
             
             var pacienteArray = {"id":paciente,"nombreApe":pacienteNombre}; 
             var url = urlBase + "fichapacientes/pacienteCI";
             $.ajax({type: "POST",
                 url: url,
                 data: pacienteArray,
                 success: function(data){
                     window.location.href = "<?php echo base_url().'fichapacientes/load_anamnesis/';?>"+data.clave_random;
                 },
                 dataType: "json"
             });
                     
            return false;
         }); 
         $('#pacientes tbody').on('click', '.btn-medidas', function (e) {
            var pacienteArray = {"id":paciente,"nombreApe":pacienteNombre}; 
             var url = urlBase + "fichapacientes/pacienteCI";
             $.ajax({type: "POST",
                 url: url,
                 data: pacienteArray,
                 success: function(data){
                     window.location.href = "<?php echo base_url().'fichapacientes/load_medidas/';?>"+data.clave_random;
                 },
                 dataType: "json"
             });
             
            return false;
         }); 
         
        });
	</script>	
</head>
<style>
    div.container { max-width: 1200px }  
    .modal-dialog {
        max-width: 350px;
    }
    @media(max-width:768px) {
        .modal-dialog {
            max-width: 300px;
        }
    }  
    td.details-control {
            text-align:center;
            color:forestgreen;
    cursor: pointer;
    }
    tr.shown td.details-control {
        text-align:center; 
        color:red;
    }
    table {
        table-layout:fixed;
      }
    table td {
      word-wrap: break-word;
      max-width: 400px;
    }
    #pacientes td {
      white-space:inherit;
    }
    .table-responsive{
        height: 65%;
    }

</style>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="col-lg-12">
                <div class="panel panel-default" id="fichaTabla">
                    <div class="panel-heading">
                        <h4>Ficha de Pacientes  </h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="pacientes" class="table hover table-bordered responsive nowrap compact" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Tel&eacute;fono</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button id="altas" type="button" class="btn btn-primary" >Nuevo Paciente</button>
                    </div>
                </div>
            </div>           
        </div>
    </div>
</body>
<script>
    var paciente;
         
    $('#pacientes tbody').on( 'click', 'button.historia', function (e) { 
        var tabla = $('#pacientes').DataTable();
        var parentRow = $(this).parent().parent().parent().parent();
        var rowData = tabla.row( parentRow ).data();      
        var histoParam = {"idPaciente":rowData['idPaciente'],"nombre":'xx',"apellido":'xx'}; 
        var urlCI = urlBase + "histo/histoCI";         

                $.ajax({type: "POST",
                    url: urlCI,
                    data: histoParam,
                    error: function(err){
                                     if(err.status == 200){
                                         console.log('no hay error'); 
                                     }else{
                                        console.log('hay error ');
                                        console.log(err);
                                     } 
                                 },  
                    success: function(data){
                        window.location.href = "<?php echo base_url().'histo/load_histo/';?>"+data.clave_random;

                    },
                    dataType: "json"
                });
            });

    $('#pacientes tbody').on( 'click', 'button.tratamientos', function (e) { 
        
        var tabla = $('#pacientes').DataTable();
        var parentRow = $(this).parent().parent().parent().parent();
        var rowData = tabla.row( parentRow ).data(); 
        window.location.href = "<?php echo base_url().'tratamientos/tratamientos_paciente/';?>"+rowData['idPaciente'];
    }); 
     
</script>
</html>	
      