 <!DOCTYPE html>
 <html>
     <head>
         
         <link rel="stylesheet" href="<?=base_url();?>js/bootstrap3-dialog-master/dist/css/bootstrap-dialog.min.css" >              
         <script src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>   
        
         <script>    
             var urlBase = "<?php echo base_url(); ?>";          
             var idPacienteJs;
             var nombrePaciente;        
             $(document).ready(function() {
                 idPacienteJs =  <?php echo $paciente->idPaciente;?>; 
                 //console.log(idPacienteJs);
                 nombrePaciente = "<?php echo $paciente->nombre.' '.$paciente->apellido;?>"; 
                 //console.log(nombrePaciente);
                 $('#nuevo_tratamiento').on('click', function(){  
                     var arrayPaciente = {"id":idPacienteJs,"nombre":nombrePaciente}; 
                     var url = urlBase + "tratamientos/asignarCI";
                     $.ajax({type: "POST",
                         url: url,
                         data: arrayPaciente,
                         success: function(data){
                             window.location.href = "<?php echo base_url().'tratamientos/asignar/';?>"+data.clave_random;
                         },
                         dataType: "json"
                     });
                 }); 
                 $('#volver').on('click', function () {                
                    window.location.href = "<?php echo base_url().'fichapacientes';?>";
                });
             });
         </script>	
     </head>
     <style>
         div.container { max-width: 1200px }
         button{
             min-width: 100px;
             max-width: 150px;
         }
         table {
            table-layout: fixed;
            word-wrap: break-word;
        }
         
     </style>
     <body>
         <div id="wrapper">
             <div id="page-wrapper">
                 <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
                 <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">   
                     <div class="panel panel-default" id="fichaTabla">
                         <div class="panel-heading">
                             <h4>Tratamientos pendientes de <b><?php echo $paciente->nombre.' '.$paciente->apellido;?></b> </h4>
                         </div>
                         <div class="panel-body">
                             <div>
                                 <table class="table table-hover" id="pendientes">
                                     <thead>
                                         <tr>
                                             <th class="col-xs-2">Profesional</th>
                                             <th class="col-xs-2">Turno</th>
                                             <th class="col-xs-2">Descripcion</th>
                                             <th class="col-xs-1"></th>
                                             <th class="col-xs-1"></th>
                                             <th class="col-xs-1"></th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                   <?php
                                   if(count($pendientes) == 0){
                                    echo "<tr>";
                                    echo "<td valign='top' colspan='4' class='dataTables_empty'>No hay registros de turnos pendientes</td>";
                                    echo "</tr>";
                                   } else{
                                            foreach ($pendientes as $value) {
                                                if($value->estado == "warning"){
                                                    echo "<tr class='danger'>";   
                                                }else{ 
                                                    echo "<tr>";
                                                }
                                                    echo    "<td>".$value->profesional ."</td>"    ;
                                                    echo    "<td>".$value->turno."</td>";
                                                    echo    "<td>".$value->descripcion."</td>"; 
                                                    $ids="";
                                                    echo    "<td class='idTrat' style='display:none;'>".$value->id."</td>";
                                                    echo    "<td class='idProf' style='display:none;'>".$value->id_profesional."</td>";
                                                    echo    "<td class='turno' style='display:none;'>".$value->turno_original."</td>";
                                                    echo    "<td class='descrip' style='display:none;'>".$value->descripcion."</td>";
                                                    echo    "<td class='inter' style='display:none;'>".$ids."</td>";
                                                    echo    "<td><button type='button' class='historia btn btn-outline' data-toggle='tooltip' title='Pasar a historia clínica'><i class='fa fa-hospital-o fa-lg' style='color:#286090'></i></button></td>"
                                                    . "<td><button type='button' class='eliminar btn btn-outline' data-toggle='tooltip' title='Eliminar'><i class='fa fa-trash-o fa-lg' style='color:#286090'></i></button></td>"
                                                    . "<td><button type='button' class='editar btn btn-outline' data-toggle='tooltip' title='Editar'><i class='fa fa-pencil-square-o fa-lg' style='color:#286090'></i></button></td>";                                       

                                                    echo "</tr>";
                                                }; 
                                            }
                                   ?>
                                     </tbody>
                                 </table>
                             </div>                        
                         </div>
                         <div class="panel-footer"> 
                             <div class="row">
                                 <div class="col-lg-4 col-lg-4 col-sm-4 col-xs-12">
                                     <button id="nuevo_tratamiento" type="button" class="btn btn-primary" >Agregar</button>
                                     <button id="volver" type="button" class="btn btn-primary" >Volver</button>
                                 </div>
                                 <div class="col-lg-4 col-lg-4 col-sm-4 col-xs-12"></div>              
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>  
             </div>
         </div>
     </body>
     <script>
         $('#pendientes tbody').on( 'click', 'button.eliminar', function (e) { 
             var idTrat = $(this).closest('tr').children('td.idTrat').text();
             BootstrapDialog.show({
                 type: BootstrapDialog.TYPE_WARNING,
                 title: 'Atenci&oacute;n!!',
                 message: "Confirma eliminar el tratamiento seleccionado?",
                 draggable: true,								
                 buttons: [{
                         label: 'Si',
                         action: function(dialogRef){   
                             var url = urlBase + "tratamientos/eliminar_tratamientos_paciente/"+idTrat;                                                     
                             $.ajax({
                                 type: "DELETE",
                                 dataType: "json",
                                 url: url,
                                 error: function(err){
                                     if(err.status == 200){
                                         console.log('no hay error'); 
                                         location.reload();
                                     }else console.log('hay error');
                                 },                          
                                 success:  function (data) {
                                     if(!data.valido){
                                         BootstrapDialog.show({
                                             type: BootstrapDialog.TYPE_DANGER,
                                             title: 'Error!!',
                                             message: 'No se pudo eliminar el tratamiento',
                                             draggable: true,								
                                             buttons: [{
                                                     label: 'Ok',
                                                     action: function(dialogRef){dialogRef.close();}
                                                 }]
                                         });  
                                     }else{                                                                   
                                         location.reload();
                                     }                                                            
                                 }
                             });
                             dialogRef.close();
                         }
                     },
                     {
                         label: 'No',
                         action: function(dialogRef){dialogRef.close();}
                     }
                 ]
             });
        
         }); 
         $('#pendientes tbody').on( 'click', 'button.editar', function (e) {        
             var idTrat = $(this).closest('tr').children('td.idTrat').text();
             var prof = $(this).closest('tr').children('td.idProf').text();
             var descrip = $(this).closest('tr').children('td.descrip').text();
             var turno = $(this).closest('tr').children('td.turno').text(); 
             var idPacienteId =  <?php echo $paciente->idPaciente;?>; 
             var nombre = "<?php echo $paciente->nombre;?>"; 
             var apellido = "<?php echo $paciente->apellido;?>"; 
             var url = urlBase + "tratamientos/asignarCITrat";         
             var trat = {"id":idPacienteId,"nombre":nombre,"apellido":apellido,"prof":prof,"descrip":descrip,"turno":turno,"idTrat":idTrat}; 

             $.ajax({type: "POST",
                 url: url,
                 data: trat,
                 success: function(data){
                     window.location.href = "<?php echo base_url().'tratamientos/asignar/';?>"+data.clave_random;
                 },
                 dataType: "json"
             });
         });
         $('#pendientes tbody').on( 'click', 'button.historia', function (e) {
             var idTrat = $(this).closest('tr').children('td.idTrat').text();
             var prof = $(this).closest('tr').children('td.idProf').text();
             var descrip = $(this).closest('tr').children('td.descrip').text().replace(/\n/g, "<br />");
             var turno = $(this).closest('tr').children('td.turno').text(); 
             var idPacienteId =  <?php echo $paciente->idPaciente;?>; 
             var nombre = "<?php echo $paciente->nombre;?>"; 
             var apellido = "<?php echo $paciente->apellido;?>"; 
             var url = urlBase + "tratamientos/asignarCITrat";  
             var action = "asignar"       
             var trat = {"action":action,"id":idPacienteId,"nombre":nombre,"apellido":apellido,"prof":prof,"descrip":descrip,"turno":turno,"idTrat":idTrat}; 
             
             $.ajax({type: "POST",
                 url: url,
                 data: trat,
                 success: function(data){
                     window.location.href = "<?php echo base_url().'tratamientos/asignar_histo/';?>"+data.clave_random;
                 },
                 dataType: "json"
             });
         });

     </script>
 </html>	
      