<!DOCTYPE html>
<html>  	
    <body>
	<div id="wrapper">             
            <div id="page-wrapper">                    
                <div class="panel panel-default" id="filiatorios">
                    <div class="panel-heading">
                        <h4>Alta de profesional</h4>
                    </div>
                    <div class="panel-body">
                        <div id="rowFiliatorio"  class="row">                                        
                                <form id="formProf" role="form">
                                    <fieldset>                                                
                                        <div class="form-group">
                                            <div class="col-lg-6">   
                                                <label>Nombre:</label>
                                                <input name="nombre" id ="txtNombre" class="form-control">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Apellido:</label>
                                                <input name="apellido" id ="txtApellido" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>                                                  
                                </form>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button id="btnGuardar" type="button" class="btn btn-primary">Guardar</button>
                        <button id="btnVolver" type="button" class="btn btn-primary">Volver</button>                        
                    </div>
                </div>
            </div>
        </div>
    </body>  
      <script type="text/javascript" src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>       
          
        <script type="text/javascript">    
             var urlBase = "<?php echo base_url(); ?>";
             $("#formProf").jqxValidator({
                    hintType: 'label',
                    animationDuration: 500,        
                    rules: [
                                {input: '#txtNombre', 
                                message: 'Debe ingresar el nombre !', 
                                action: 'keyup, blur', 
                                rule: 'required'
                                },
                                {input: '#txtApellido', 
                                message: 'Debe ingresar el apellido !', 
                                action: 'keyup, blur', 
                                rule: 'required'
                                }                       
                        ], 
                    onSuccess: function(){          
                        var parametros = { 
                            "nombre" :  $("#txtNombre").val().trim(),
                            "apellido" : $("#txtApellido").val().trim()
                        };                                    
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: urlBase + "/main/new_profesional",
                            data: parametros,
                            success:  function (data) {                                    
                                if(!data.valido){
                                    console.log("error");
                                    BootstrapDialog.show({
                                            type: BootstrapDialog.TYPE_DANGER,
                                            title: 'Error!!',
                                            message: 'Los cambios no pudieron ser salvados',
                                            draggable: true,								
                                            buttons: [{
                                                        label: 'Ok',
                                                        action: function(dialogRef){dialogRef.close();}
                                                      }]
                                    });
                                }else{ 
                                    console.log("anduvo ok");
                                    BootstrapDialog.show({
                                            onshown: function(dialogRef){
                                                            setTimeout(function(){
                                                                dialogRef.close();                                                
                                                            }, 2000);
                                                    },
                                            onhide: function(dialogRefh){
                                                //window.location = urlBase+'fichapacientes';
                                                location.replace(document.referrer); //haciendo así hace el back con el refresco de la pantalla
                                            },
                                            type: BootstrapDialog.TYPE_SUCCESS,
                                            title: 'Informaci&oacute;n',
                                            message: 'Los cambios fueron salvados satisfactoriamente',
                                            draggable: true,								
                                            buttons: [{
                                                        label: 'Ok',
                                                        action: function(dialogRef){
                                                            dialogRef.close(); 
                                                        }
                                                    }]
                                    });                     
                                 }
                           },
                           error: function(xhr,status,error){
                               console.log("Hay error "+error);
                           }
                        });
                  },
                  onError: function (){
                      console.log("hay error onError");
                  }
                });

                $("#btnVolver").click(function() {                   
                    window.history.back();
                });

                $('#btnGuardar').on('click', function(){	
                    $('#formProf').jqxValidator('validate');                        
                });
        </script>
</html>	
      