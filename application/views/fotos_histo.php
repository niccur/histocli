<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
</head>
<script type="text/javascript"> 
        var idHisto = "<?php echo $idHisto;?>";
        var listaFotos;
        var urlBase = "<?php echo base_url(); ?>";
        var targetFileFoto = urlBase+"resources/fotos/";   
        var targetFileThumb = urlBase+"resources/fotos/thumbnail/";      
        
        $(document).ready(function(){
            var listaFotos;
            $.ajax({
                async: false,
                url: "<?=base_url();?>histo/load_fotos_histo/"+idHisto,
                success: function (data, status, xhr) {
                    listaFotos = $.parseJSON(data); 
                }
            });

            var content="";
            $.each(listaFotos,function(key, value){ 
                content += "<div class='col-xs-12 col-md-3'>";
                content += "<div class='row'>";
                content +=      "<div class='col-xs-12 col-md-12'>";
                content +=          "<div class='thumbnail'>";
                content +=              "<a href='"+targetFileFoto+value.nombreFoto+"' target='_blank'><img src='"+targetFileThumb+value.nombreFoto+"' alt='"+value.nombreFoto+"'><a>"
                content +=          "</div>";
                content +=      "</div>";
                content += "</div>"; 
                content += "</div>";                   
            });
            if(!content){
            content = "<div>No se registran fotos</div>";  
            }
            $("#fotosLista").append(content);

        });
        
</script>

<body>
	    <div id="wrapper">             
            <div id="page-wrapper">                    
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4>Fotos en historia clínica de <b><?php echo $fecha.' - '.$nombre.' '.$apellido;?></b> </h4>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">                        
                        <div class="panel panel-default">
                            <div class="panel-body" id="fotosLista">  
                            </div>
                        </div>
                    </div>
                        <div class="panel-footer">
                            <button id="btnVolver" type="button" class="btn btn-primary" onclick='location.replace(document.referrer)'>Volver</button>
                        </div>                                
                    </div>                 
            </div>
        </div>
    </body>
</html>