<!DOCTYPE html>
<html>
    <body>
	<div id="wrapper">             
            <div id="page-wrapper">                    
                <div class="panel panel-default" id="medidas">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4><span id="medidasTitle"></span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="tablaMedidas" class="compact cell-border table-hover dt-head-left" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Fecha</th>
                                                <th>Brazo</th>
                                                <th>Busto</th>
                                                <th>Cintura</th>
                                                <th>Cadera Alta</th>
                                                <th>Cadera Baja</th>
                                                <th>Gluteos</th>
                                                <th>Muslo Superior</th>
                                                <th>Muslo Inferior</th>
                                                <th>Rodilla</th>
                                                <th>Pantorrilla</th>
                                                <th>Tobillo</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer"> 
                        <div class="row">
                            <div class="col-lg-4 col-lg-4 col-sm-4 col-xs-12">
                                <button id="nuevaMedida" type="button" class="btn btn-primary" >Agregar</button>
                                <button id="volver" type="button" class="btn btn-primary" >Volver</button>
                            </div>
                            <div class="col-lg-4 col-lg-4 col-sm-4 col-xs-12"></div>              
                        </div>
                    </div>                                 
                </div>                                 
            </div>            
        </div> 
    
    </body>
        <script type="text/javascript" src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>       
        <script type="text/javascript" src="<?=base_url();?>application/views/histomedidas.js"></script>  
        <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/media/js/jquery.dataTables.js"></script>
	    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/media/css/jquery.dataTables.css" >
	    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/css/dataTables.responsive.css" >             	
        
        <script type="text/javascript">             
             var urlBase = "<?php echo base_url(); ?>";             
             var idMedida; 
             var modo;

            $(document).ready(function () { 
                idPacienteJs =  <?php echo $idPaciente ;?>;
                var nombre = "<?php echo $nombre;?>";  
                $("#medidasTitle").html("Medidas de "+nombre+ " (en cm)");
                
                
                //FORM MEDIDAS ------------------------------------------------------------
                tableMedidas = $('#tablaMedidas').DataTable( {
                        "sAjaxSource":urlBase + "histo/get_medidas/" + idPacienteJs,
                        "sAjaxDataProp": "",
                        "info": false,
                        language: {
                                    "sProcessing":     "Procesando...",
                                    "sZeroRecords":    "No se encontraron resultados",
                                    "sEmptyTable":     "No hay registros de medidas cargados"                                    
                                  },
                        "responsive": false,
                        "scrollY": 150,
                        "scrollX": 100,
                        "paging": false,
                        "bFilter": false,
                        "bSort": false,
                        "columns": [
                                    { "data": "fecha" },
                                    { "data": "brazo" },
                                    { "data": "busto" },
                                    { "data": "cintura" },
                                    { "data": "cadera_alta" },                                               
                                    { "data": "cadera_baja" },                                    
                                    { "data": "gluteos" },
                                    { "data": "muslo_superior" },
                                    { "data": "muslo_inferior" },
                                    { "data": "rodilla" },
                                    { "data": "pantorrilla" },
                                    { "data": "tobillo" },
                                    {
                                        data: null,
                                        className: "center",
                                        defaultContent: '<button type="button" class="editarMedida btn btn-primary"><i class="fa fa-pencil"></i></button>'
                                    },
                                    {
                                        data: null,
                                        className: "center",
                                        defaultContent: '<button type="button" class="eliminarMedida btn btn-primary"><i class="fa fa-trash-o"></i></button>'
                                    }                                            
                                ]                                                                                      
                    });   
                    $("#volver").click(function() {
                        window.history.back();
                        //location.replace(document.referrer);
                    });
                   $("#nuevaMedida").click(function() {
                        if(idPacienteJs !==0){
                            idMedida = 0;     
                            modo = "agregar";
                            //cargar la pagina de edicion
                            var medidaArray = {"id":idMedida,"modo":modo,"idPaciente":idPacienteJs}; 
                            var url = urlBase + "editar_medidas/medidaCI";

                            $.ajax({type: "POST",
                                url: url,
                                data: medidaArray, 
                                dataType: "json",
                                success: function(dataResul){
                                        console.log(dataResul);
                                        window.location.href = urlBase +'editar_medidas/load_editar_medidas/'+dataResul.clave_random;
                                },
                                error: function(xhr,status,error){
                                    console.log("Hay error "+error);
                                }
                            });                                                       
                        }else{
                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_DANGER,
                                title: 'Imposible agregar medidas.',
                                message: 'Se deben tener los datos filiatorios salvados primero.',
                                draggable: true,								
                                buttons: [{
                                            label: 'Ok',
                                            action: function(dialogRef){dialogRef.close();}
                                          }]
                            });
                        }                        
                    }); 
                    $('#tablaMedidas tbody').on( 'click', 'button.editarMedida', function (e) {    
                        e.preventDefault();                          
                        var table = $('#tablaMedidas').DataTable();
                        var parentRow = $(this).parent().parent();
                        var rowData = table.row( parentRow ).data();
                        modo = "editar"
                        idMedida = rowData["id"];
                        //cargar la pagina de edicion
                        var medidaArray = {"id":idMedida,"modo":modo,"idPaciente":idPacienteJs}; 
                        var url = urlBase + "editar_medidas/medidaCI";
                        $.ajax({type: "POST",
                            url: url,
                            data: medidaArray, 
                            dataType: "json",
                            success: function(dataResul){
                                console.log(dataResul);
                                window.location.href = urlBase +'editar_medidas/load_editar_medidas/'+dataResul.clave_random;
                            },
                            error: function(xhr,status,error){
                               console.log("Hay error "+error);
                            }
                        });
                        tableMedidas.ajax.reload(null);                                        

                        return false;
                    });     
            });
        </script>	
</html>	
      