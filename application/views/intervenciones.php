 <!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/media/css/jquery.dataTables.css" >
    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/css/dataTables.responsive.css" >             	
    <link rel="stylesheet" href="<?=base_url();?>js/bootstrap3-dialog-master/dist/css/bootstrap-dialog.min.css" >              

    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>   
        
    <script>
        $(document).ready(function() {	
                                
            $('#altas').on('click', function(){
                $("#nuevo_tratamiento").modal();
            });     
			
			
			var lista_deptos = $('#lstDpto');
			var txtNombre = $("#txtNombre");
			var txtDescripcion = $("#txtDescripcion");
			//completo la lista de departamentoes
            $.ajax({
                    async: false,
                    url: "<?=base_url();?>intervenciones/load_departamentos",
                    success: function (data, status, xhr) {
                                        var dptos = $.parseJSON(data);
										lista_deptos.append($("<option />.val('').text('')"));
										$.each(dptos, function() {
										   //console.log(this);
											lista_deptos.append($("<option />").val(this.id_departamento).text(this.nombre));
										});											
                                }
                });		
		    //-completo la lista de departamentoes FIN--------------------------------------------------------------------------------------------- 
			 
            var dt_det = $('#intervenciones').DataTable( {                
                "sAjaxSource": "<?=base_url();?>intervenciones/load_intervenciones", 
                "sAjaxDataProp": "",
                "info": false,
                language: {
                            "sProcessing":     "Procesando...",
                            "sZeroRecords":    "No se encontraron resultados",
                            "sEmptyTable":     "No hay registros",
                            "search": "_INPUT_",
                            "searchPlaceholder": "Buscar..."						
                            }, 						 
                "responsive": true,
                "scrollY": 300,     
                "paging": false,
                "columns": [                    
                    { "data": "nombre" },
					{ "data": "departamento" }
                ]
                } );
                
          
            //--Mostrar intervenciones------------------------------------------------------------------------------------------------
              $('#intervenciones tbody').on( 'click', 'tr', function () {                  
                    dt_det.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    var aPos = $('#intervenciones').dataTable().fnGetPosition(this);
                    var aData = $('#intervenciones').dataTable().fnGetData();                    
                    txtNombre.val(aData[aPos].nombre);
					lista_deptos.val(aData[aPos].id_departamento);
					txtDescripcion.val(aData[aPos].descrip);
                    //dt_det.ajax.reload(null);					
                } );			
            //--------------------------------------------------------------------------------------------------------------------------------						
            //-- COMPOSICION DE TRATAMIENTOS   fin-----------------------------------------------------------------------------------------------------------
			
        });
	</script>	
</head>
<style>
    div.container { max-width: 1200px }  
</style>
<body>
	<div id="wrapper">
		<div id="page-wrapper">
                    <div class="col-lg-12">
                        <div class="panel panel-default" id="fichaTabla">
                            <div class="panel-heading">
                                <h3> Intervenciones </h3>
                            </div>
                            <div class="panel-body well well-sm">
                                <div class="col-lg-6">
									<form id="formFiliatorio" role="form">
                                        <fieldset>                                                
											<div class="form-group">    																						
												<div class="col-lg-12">
													<label>Nombre:</label>
													<input name="nombre" id ="txtNombre" class="form-control"> 
												</div>
												<div class="col-lg-12">
													<label>Departamento:</label>
													<select id="lstDpto" class="form-control"></select>
												</div>	
												<div class="col-lg-12">
													<label>Descripci&oacute;n:</label>
													<textarea id="txtDescripcion" class="form-control" rows="3"></textarea>                                                       
												</div>											
											</div>
										</fieldset>  
									</form>		
                                </div>   
                                <div class="col-lg-6">
                                    <div class="table-responsive">
										<table id="intervenciones" class="display responsive nowrap compact" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>                                                
                                                    <th>Departamento</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>                                
                            </div>
                            <div class="panel-footer"> 
                                <button id="nuevo_tratamiento" type="button" class="btn btn-primary" >Nuevo Tratamiento</button>
                                <button id="ver_tratamiento" type="button" class="btn btn-primary" >Ver Tratamiento</button>
                            </div>
                        </div>
                    </div>                                  
                </div>
	</div>
</body>
</html>	
      