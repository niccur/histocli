<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="<?= base_url(); ?>js/bootstrap-datetimepicker-4.17.47/build/css/bootstrap-datetimepicker.min.css" >   
        <script src="<?= base_url(); ?>js/moment-master/moment.js"></script>   
        <script src="<?= base_url(); ?>js/bootstrap-datetimepicker-4.17.47/build/js/bootstrap-datetimepicker.min.js"></script>   

        <script>
            var urlBase = "<?php echo base_url(); ?>";
            var urlTratDet;
            var idPacienteJs;
            var clave;
            var profesionales;
            var source;
            var trat;
            var prof;
            var inter;
            var descrip;
            var urlAjax;
            var parametrosAjax;
            $(document).ready(function () {
                trat = "<?php echo $idTrat; ?>";
                idPacienteJs = <?php echo $idPaciente; ?>;
                prof = "<?php echo $prof; ?>";                
                descrip = "<?php echo preg_replace("/\r\n|\r|\n/",'\n',$descrip); ?>";
                turno = "<?php echo $turno; ?>";
                clave = "<?php echo $clave; ?>";
            
                //Obtener lista de profesionales ajax
                 $.ajax({
                        async: false,
                        url: "<?=base_url();?>tratamientos/load_profesionales",
                        success: function (data, status, xhr) {
                            profesionales = $.parseJSON(data);                            
                        }
                    });               
                //cargar la lista de profesinales
                $.each(profesionales,function(key, value){
                    $("#profesional").append('<option value=' + value.idProf + '>' + value.nombre + '</option>');
                });
              
                $('#cancelar').on('click', function () {                
                    parent.history.back();
                });
                 if(trat){
                    $("#descripcion").val(descrip);;
                    $('#turno').datetimepicker({
                        date: new Date(turno),
                        format: 'DD/MM/YYYY hh:mm A'
                    });
                 } 
                 
                $('#turno').datetimepicker({
                    format: 'DD/MM/YYYY hh:mm A'
                });
                
              
                $('#aceptar').on('click', function () {  
                    if(!trat){
                        urlAjax = urlBase + "tratamientos/agregar_tratamientos";
                        parametrosAjax = {"clave": clave,"profesional":$("#profesional").val(),"turno":$("#turno").find("input").val(),"descripcion":$("#descripcion").val()};
                    }else{
                        urlAjax = urlBase + "tratamientos/editar_tratamientos_paciente/"+trat;
                        parametrosAjax = {"prof":$("#profesional").val(),"turno":$("#turno").find("input").val(),"descrip":$("#descripcion").val()};
                    }    
                     if(!$("#turno").find("input").val() || !$("#profesional").val() ||!$("#descripcion").val()){
                          BootstrapDialog.show({
                            type:BootstrapDialog.TYPE_WARNING,
                            title: 'Atenci&oacute;n!',
                            message:'Debe completar los datos.'
                        });
                    }else{
                            $.ajax({                         
                                type: "POST",
                                 url: urlAjax,
                                 data: parametrosAjax,                          
                                 dataType: "json",
                                 async: false,
                                 error: function(err){
                                     if(err.status == 200){
                                         console.log('no hay error1');
                                         window.location.href = urlBase +'tratamientos/tratamientos_paciente/'+idPacienteJs;
                                     }else console.log('hay error2');
                                 },                          
                                 success: function(data){                              
                                       window.location.href = urlBase +'tratamientos/tratamientos_paciente/'+data.idPaciente;
                                      }                          
                             }); 
                         }
                }); 
            });
        </script>	
    </head>
    <style>
        div.container { max-width: 1200px }

    </style>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="col-lg-1 col-md-1 col-sm-1"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">                       
                    <div class="panel panel-default" id="fichaTabla">
                        <div class="panel-heading">
                            <h4>Agregar tratamientos para <b><?php echo $nombre; ?></b> </h4>
                        </div>
                        <div class="panel-body"> 
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Profesional:</label>
                                    <select id="profesional" class="form-control selectpicker" data-live-search="true" title="Seleccione un Profesional..." data-width="70%">                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Descripci&oacute;n:</label>
                                       <textarea name="descripcion" id="descripcion" class="form-control" rows="3"></textarea>                                                       
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Turno:</label> 
                                     <div class='input-group date' id='turno'>
                                            <input type='text' class="form-control"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                     </div>   
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer"> 
                            <div>
                                <button id="aceptar" type="button" class="btn btn-primary">Guardar</button>
                                <button id="cancelar" type="button" class="btn btn-primary" >Volver</button>
                            </div>
                        </div>
                    </div>                      
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1"></div> 
            </div>
        </div>
    </body>
</html>	
