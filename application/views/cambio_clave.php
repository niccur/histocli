
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Historias Clinicas</title>

<script type="text/javascript">

	$(document).ready(function () 
	{    
		var theme = 'energyblue';
         //***********************************************************************************
        $('#ingresarButton').jqxButton({ width: 120, height: 30 , theme:theme});
		$('#cancelarButton').jqxButton({ width: 120, height: 30 , theme:theme});
        $("#txtclave1").jqxInput({placeHolder: "Clave Vieja", height: 25, width: 200, minLength: 1 });
		$("#txtclave2").jqxInput({placeHolder: "Clave Nueva", height: 25, width: 200, minLength: 1 });
		$("#txtclave3").jqxInput({placeHolder: "Confirmar Clave Nueva", height: 25, width: 200, minLength: 1 });
		
		$('#ingresarButton').bind('click', function () {
			if ($("#txtclave2").val() == $("#txtclave3").val()){
			
				$('#claveForm').jqxValidator('validate');
			} else {
				new Messi('La clave a confirmar es distinta.', 
							{modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
						);
			}
        });
		
		$('#cancelarButton').bind('click', function () {
					var url = '<?=base_url();?>main';
					$(location).attr('href',url);
        });

										
										
										
         
		if (theme.length > 0) {
			$('.text-input').addClass('jqx-input-' + theme);
		}
        
        $('#claveForm').jqxValidator({
			hintType: 'label',
            rules: [
				{ input: '#txtclave1', message: 'Ingrese la antigua clave', action: 'keyup, blur', rule: 'required',  arrow: false},
				{ input: '#txtclave2', message: 'Ingrese la nueva clave', action: 'keyup, blur', rule: 'required', arrow: false },
				{ input: '#txtclave3', message: 'Confirme la nueva clave', action: 'keyup, blur', rule: 'required', arrow: false },
			], 
			theme: theme, 
			onSuccess: function()
			{
						var parametros = {
							"vieja" : $("#txtclave1").val(),
							"nueva" : $("#txtclave2").val()
						};

						$.ajax(
						{
							type: "POST",
							dataType: "json",
							url: "<?=base_url();?>login/actualizar",
							data: parametros,
							beforeSend: function () {
								$('#ventLogin').ajaxloader();
							},					
							success:  function (data) {
								if (data.valido == false)
								{
									$('#ventLogin').ajaxloader('hide');
									new Messi('Nombre de usuario o contraseña incorrecta', 
										{modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
									);
								}
								else if (data.valido == true)
								{
									if (data.estado == 0){ 							
										$('#ventLogin').ajaxloader('hide');
										new Messi('El usuario se encuentra inactivo', 
											{modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
										)
									} else {
										var url = '<?=base_url();?>main';
										//$.post(url, $("#loginForm").serialize() );
										$(location).attr('href',url);
									}
								}
							}
						});		
			}
			});
			
			
		$('#txtusuario').focus();
	});  

</script>
</head>

<body>
	<div align="center" id="ventLogin"> 
		<form id="claveForm" action="./" >
				<div align="center"> 			
					<input type="password" id="txtclave1" />
				</div>
				<div align="center"> 				
					<input type="password" id="txtclave2" />   
				</div>
				<div align="center">		
					<input type="password" id="txtclave3"  />  
				</div>
				<div align="center"> <input type="button" value="Aceptar" id="ingresarButton"  /></div>	
				<div align="center"> <input type="button" value="Cancelar" id="cancelarButton"  /></div>						
		</form>
	</div>

</body>

