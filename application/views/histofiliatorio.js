function newFiliatorio(){
    valido_fecha = true;
    cambioFiliatorio = false;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
    today = dd+'/'+mm+'/'+yyyy;
    
    urlFiliatorioRun = 'histo/add_filiatorio';
    $("#txtNombre").val(null);
    $("#txtApellido").val(null);
    $("#fec_nac").val(null);                                  
    $("#txtEdad").val(0);                    
    $("#txtOcupacion").val(null);
    $("#txtHijos").val(0);
    $("#txtDireccion").val(null);
    $("#txtCodPostal").val(null);
    $("#txtTelefono").val(null);
    $("#txtCelular").val(null);
    $("#txtNotas").val(null);
    $("#txtEmail").val(null);
    $("#txtObrasocial").val(null);
    $("#nroAfiliado").val(null);
    $("#nroDni").val(null);

    vSexo = null;
    $("#opcionMasculino").prop('checked', false);
    $("#opcionFemenino").prop('checked', false);
    urlFotoPerfil =  urlBase + 'img/profilex.jpg';
    $("#fotoPerfil").attr('src',urlFotoPerfil);                  
    //$("#fotoPerfil").html("<img src='"+urlFotoPerfil+"' alt='...'>");    
    $("#fec_ingreso").val(today);
}
function loadFiliatorio(){
    valido_fecha = true;
    urlFiliatorioRun = 'histo/update_filiatorio';
    //traigo los datos filiatorios del paciente
    $.ajax({async: false,
            type: "POST",
            dataType: "json",
            data:{"idPacienteJs": idPacienteJs},
            url: urlBase + 'histo/get_filiatorio',
            success: function (datax, status, xhr) {
                                jsonFiliatorio = datax;
                        }
        });

    cambioFiliatorio = false;
    $("#nombreH4").html(jsonFiliatorio.nombre+' '+jsonFiliatorio.apellido);
    $("#txtNombre").val(jsonFiliatorio.nombre);
    $("#txtApellido").val(jsonFiliatorio.apellido);
    $("#fec_nac").val(jsonFiliatorio.fecha_nac);   
    $("#fec_ingreso").val(jsonFiliatorio.fecha_ingreso);
    $("#txtEdad").val(jsonFiliatorio.edad);                    
    $("#txtOcupacion").val(jsonFiliatorio.ocupacion);             
    $("#txtHijos").val(jsonFiliatorio.hijos);
    $("#txtDireccion").val(jsonFiliatorio.direccion);
    $("#txtCodPostal").val(jsonFiliatorio.codpostal);
    $("#txtTelefono").val(jsonFiliatorio.telefono);
    $("#txtCelular").val(jsonFiliatorio.celular);
    $("#txtNotas").val(jsonFiliatorio.notas);
    $("#txtEmail").val(jsonFiliatorio.email);
    $("#txtObrasocial").val(jsonFiliatorio.obrasocial);
    $("#nroAfiliado").val(jsonFiliatorio.nro_afiliado);
    $("#nroDni").val(jsonFiliatorio.dni);

    vSexo = jsonFiliatorio.sexo;
    if(vSexo === 'M'){
        $("#opcionMasculino").prop('checked', true);
        $("#opcionFemenino").prop('checked', false);
    }else{
        $("#opcionFemenino").prop('checked', true);
        $("#opcionMasculino").prop('checked', false);
     }    
     urlFotoPerfil = urlBase + 'img/'+jsonFiliatorio.foto;
    
     $("#fotoPerfil").attr('src',urlFotoPerfil);                     
//                     $("#btnFiliatorio").addClass("disabled");
//                     $("#btnCanelFiliatorio").addClass("disabled");
}

function cargarFiliatorio(){
    if(idPacienteJs === 0){
        newFiliatorio();
    }else loadFiliatorio();  
}; 