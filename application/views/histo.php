<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/media/css/jquery.dataTables.css" >
	<link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/css/dataTables.responsive.css" >             	
        <link rel="stylesheet" href="<?=base_url();?>js/bootstrap3-dialog-master/dist/css/bootstrap-dialog.min.css" >              
        <link rel="stylesheet" href="<?=base_url();?>js/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.css" >              


</head>
<style>
    .navbar-static-top{
        position:fixed;
        z-index: 1;
        top:81px;
        background-color:#9a9393;
    }
    .foto-img{
        clear: both;
        margin-top: 40px;
    }
    .table-responsive{
        height: 65%;
    }
</style>
    <body>
	<div id="wrapper">             
		<div id="page-wrapper">              
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default" id="tratamientos">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h4>Historia Clínica de <b><?php echo $paciente->nombre.' '.$paciente->apellido;?></b> </h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="pendientes" style="width: 60%;" align="center">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Descripci&oacute;n</th>
                                                    <th>Profesional</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="tratBody"></tbody>
                                        </table>
                                    </div>
                                </div>  
                                <div class="panel-footer"> 
                                    <div class="row">
                                        <div class="col-lg-4 col-lg-4 col-sm-4 col-xs-12">
                                            <button id="nueva_historia" type="button" class="btn btn-primary" >Agregar</button>
                                            <button id="volver" type="button" class="btn btn-primary" >Volver</button>
                                        </div>
                                        <div class="col-lg-4 col-lg-4 col-sm-4 col-xs-12"></div>              
                                    </div>
                                </div>                            
                            </div> 
                        </div>
                    </div>                        
                </div>
	</div>
    
</body>
        <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/media/js/jquery.dataTables.js"></script>
	    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>       

        <script type="text/javascript"> 
             var urlBase = "<?php echo base_url(); ?>";
             var nombre = "<?php echo $paciente->nombre; ?>";
             var apellido = "<?php echo $paciente->apellido;?>";
             var idPacienteJs =  <?php echo $idPaciente ;?>;                    
             var tratamientos;
                       
            $('#volver').on('click', function () {                
                    window.location.href = "<?php echo base_url().'fichapacientes';?>";
            });
            
            $('#tratamientos tbody').on( 'click', 'button.eliminar', function (e) { 
             var idHisto = $(this).closest('tr').children('td.idHisto').text();
             BootstrapDialog.show({
                 type: BootstrapDialog.TYPE_WARNING,
                 title: 'Atenci&oacute;n!!',
                 message: "¿Confirma eliminar la historia clínica seleccionada?",
                 draggable: true,								
                 buttons: [{
                         label: 'Si',
                         action: function(dialogRef){   
                             var url = urlBase + "histo/eliminar_histo/"+idHisto;                                                     
                             $.ajax({
                                 type: "DELETE",
                                 dataType: "json",
                                 url: url,
                                 error: function(err){
                                     if(err.status == 200){
                                         console.log('no hay error'); 
                                         location.reload();
                                     }else{
                                        console.log('hay error');
                                        BootstrapDialog.show({
                                             type: BootstrapDialog.TYPE_DANGER,
                                             title: 'Error!!',
                                             message: 'No se pudo eliminar la historia clínica',
                                             draggable: true,								
                                             buttons: [{
                                                     label: 'Ok',
                                                     action: function(dialogRef){dialogRef.close();}
                                                 }]
                                         });  
                                     } 
                                 },                          
                                 success:  function (data) {
                                     if(!data.valido){
                                         BootstrapDialog.show({
                                             type: BootstrapDialog.TYPE_DANGER,
                                             title: 'Error!!',
                                             message: 'No se pudo eliminar la historia clínica',
                                             draggable: true,								
                                             buttons: [{
                                                     label: 'Ok',
                                                     action: function(dialogRef){dialogRef.close();}
                                                 }]
                                         });  
                                     }else{   
                                         $('#pendientes').html(response);
                                     }                                                            
                                 }
                             });
                             dialogRef.close();
                         }
                     },
                     {
                         label: 'No',
                         action: function(dialogRef){dialogRef.close();}
                     }
                 ]
             });
        
         }); 
                
            $(document).ready(function () { 
                                
                     $.ajax({type:"GET",
                        async: false,
                        url: urlBase + "histo/load_tratamientos/"+idPacienteJs,
                         error: function(err){
                                     if(err.status == 200){
                                         console.log('no hay error'); 
                                     }else{
                                        console.log('hay error ');                                        
                                     } 
                                 },   
                        success: function (data, status, xhr) { 
                            tratamientos = $.parseJSON(data);  
                        }
                    }); 
                //cargar la tabla de tratamientos
                var content="";
         
                //console.log('tratamientos ' + JSON.stringify(tratamientos) );
                
                $.each(tratamientos,function(key, value){                    
                    content += "<tr>";
                    content +="<td class='fecha'>"+value.fecha+"</td>";
                    content +="<td>"+value.descrip_intervencion+"</td>";
                    content +="<td>"+value.profesional+"</td>"
                    content +="<td class='idHisto' style='display:none;'>"+value.id+"</td>";
                    content +="<td class='idProf' style='display:none;'>"+value.id_profesional+"</td>";
                    content +="<td><button type='button' class='eliminar btn btn-outline' data-toggle='tooltip' title='Eliminar'><i class='fa fa-trash-o fa-lg' style='color:#286090'></i></button></td>";
                    content +="<td><button type='button' class='editar btn btn-outline' data-toggle='tooltip' title='Editar'><i class='fa fa-pencil-square-o fa-lg' style='color:#286090'></i></button></td>";                                       
                    content +="<td><button type='button' class='verFotos btn btn-outline' data-toggle='tooltip' title='Ver fotos'><i class='fa fa-file-image-o fa-lg' style='color:#286090'></i></button></td>";                                       
                    content +="</tr>";                    
                });
                if(!content){
                  content = "<div>No se registran tratamientos realizados</div>";  
                }
                $("#tratBody").append(content);

            function abm_histo(histoParam,cod){
                var urlCI = urlBase + "histo/histoCI";
                     
                $.ajax({type: "POST",
                    url: urlCI,
                    data: histoParam,
                    error: function(err){
                                     if(err.status == 200){
                                         console.log('no hay error'); 
                                     }else{
                                        console.log('hay error ');
                                        console.log(err);
                                     } 
                                 },  
                    success: function(data){
                        if(cod === 1){
                            window.location.href = "<?php echo base_url().'histo/abm_histo/';?>"+data.clave_random;
                        }else window.location.href = "<?php echo base_url().'histo/abm_histo_edit/';?>"+data.clave_random;
                    },
                    dataType: "json"
                });
            }
            
            $("#nueva_historia" ).click(function() {
                histoParam = {"action":'alta',"idHisto":0,"idPaciente":idPacienteJs,"nombre":nombre,"apellido":apellido}; 
                abm_histo(histoParam,1);                
            });

            $('#tratamientos tbody').on( 'click', 'button.editar', function (e) { 
                var idHisto = $(this).closest('tr').children('td.idHisto').text();
                histoParam = {"idHisto":idHisto,"idPaciente":idPacienteJs,"nombre":nombre,"apellido":apellido}; 
   
                abm_histo(histoParam,2);                
            });

            $('#tratamientos tbody').on( 'click', 'button.verFotos', function (e) { 
                var idHisto = $(this).closest('tr').children('td.idHisto').text();
                var fecha = $(this).closest('tr').children('td.fecha').text();

                var histoParam = {"idHisto":idHisto,"nombre":nombre,"apellido":apellido,"fecha":fecha}; 
                var urlCI = urlBase + "histo/ver_fotos_historia_ci";
              
                $.ajax({type: "POST",
                    url: urlCI,
                    data: histoParam,
                    error: function(err){
                                     if(err.status == 200){
                                         console.log('no hay error'); 
                                         console.log(err);
                                     }else{
                                        console.log('hay error ');
                                     } 
                                 },  
                    success: function(data){
                        window.location.href = "<?php echo base_url().'histo/ver_fotos_historia/';?>"+data.clave_random;
                    },
                    dataType: "json"
                });
            });
                
                
            });
        </script>	
</html>	
      