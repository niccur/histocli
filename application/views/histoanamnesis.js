function newAnamnesis(){
    //var urlBase = "<?php echo base_url(); ?>";
    console.log("nueva anamnesis urlbase "+urlBase);
    cambioAnamnesis = false;
    urlAnamnesisRun = urlBase + 'histo/add_anamnesis';
    $("#txtPatologicos").val(null);
    $("#txtRm").val(null);
    $("#txtAlimentario").val(null); 
    $("#txtAlergias").val(null);
    $("#txtSueno").val(null);
    $("#txtObservaciones").val(null);
    vFisica = null;
    vAlcohol = null;
    vTabaco = null;
    vSol = null;
    $("#opFisica1").prop('checked', false);
    $("#opFisica2").prop('checked', false);
    $("#opFisica3").prop('checked', false);
    $("#opFisica4").prop('checked', false);
    $("#opAlcohol1").prop('checked', false);
    $("#opAlcohol2").prop('checked', false);
    $("#opAlcohol3").prop('checked', false);
    $("#opAlcohol4").prop('checked', false);
    $("#opTabaco1").prop('checked', false);
    $("#opTabaco2").prop('checked', false);
    $("#opSol1").prop('checked', false);
    $("#opSol2").prop('checked', false);
    $("#opSol3").prop('checked', false);
    $("#opSol4").prop('checked', false);
};

function loadAnamnesis(){
    console.log("load");
    //traigo los datos anamnesis del paciente
    $.ajax({async: false,
            type: "POST",
            dataType: "json",
            data:{"idPaciente": idPacienteJs},
            url: urlBase + 'histo/get_anamnesis',
            success: function (datax, status, xhr) {
                                jsonAnamnesis = datax;
                        }
        });

    if(jsonAnamnesis){
        urlAnamnesisRun = urlBase + 'histo/update_anamnesis';
        cambioAnamnesis = false;
        $("#txtPatologicos").val(jsonAnamnesis.patologicos);
        $("#txtRm").val(jsonAnamnesis.rm);
        $("#txtAlimentario").val(jsonAnamnesis.alimentario);   
        $("#txtAlergias").val(jsonAnamnesis.alergias);
        $("#txtSueno").val(jsonAnamnesis.sueno);                    
        $("#txtObservaciones").val(jsonAnamnesis.observaciones);         
        if(jsonAnamnesis.fisica !== null){
            vFisica = jsonAnamnesis.fisica;
            $("input[name=fisica][value=" + vFisica + "]").prop('checked', true);
        }else{
            vFisica = null;
            $("#opFisica1").prop('checked', false);
            $("#opFisica2").prop('checked', false);
            $("#opFisica3").prop('checked', false);
            $("#opFisica4").prop('checked', false);            
        }
        if(jsonAnamnesis.alcohol !== null){
            vAlcohol =  jsonAnamnesis.alcohol;
            $("input[name=alcohol][value=" + vAlcohol + "]").prop('checked', true);
        }else{
            vAlcohol = null;
            $("#opAlcohol1").prop('checked', false);
            $("#opAlcohol2").prop('checked', false);
            $("#opAlcohol3").prop('checked', false);
            $("#opAlcohol4").prop('checked', false);            
        }
        if(jsonAnamnesis.tabaco !== null){
            vTabaco = jsonAnamnesis.tabaco;
            $("input[name=tabaco][value=" + vTabaco + "]").prop('checked', true);
        }else{
            vTabaco = null;
            $("#opTabaco1").prop('checked', false);
            $("#opTabaco2").prop('checked', false);            
        }        
        if(jsonAnamnesis.sol !== null){
            vSol = jsonAnamnesis.sol;
            $("input[name=sol][value=" + vSol + "]").prop('checked', true);
        }else{
            vSol = null;
            $("#opSol1").prop('checked', false);
            $("#opSol2").prop('checked', false);
            $("#opSol3").prop('checked', false);
            $("#opSol4").prop('checked', false);            
        }
        
    }else{
        console.log("nueva anam");
        newAnamnesis();
    }
};
function cargarAnamnesis(){
    if(idPacienteJs === 0){
        console.log("nuevo");
        newAnamnesis();
    }else{
        console.log("refresh");
        loadAnamnesis();
    }  
};