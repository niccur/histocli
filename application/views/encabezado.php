<!--<!DOCTYPE html>-->
<html>
    <head>
     
		<meta charset='utf-8' />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">	

		<!-- Custom CSS -->
                                
		<link href="<?=base_url();?>css/encabezado.css" rel="stylesheet" type="text/css">

		<link rel="stylesheet" href="<?=base_url();?>css/bootstrap.min.css" >
		<link rel="stylesheet" href="<?=base_url();?>js/bootstrap3-dialog-master/dist/css/bootstrap-dialog.min.css" >
			
	        <link rel="stylesheet" href="<?=base_url();?>js/jqwidgets/styles/jqx.base.css" type="text/css" />
		<link href="<?=base_url();?>js/jqwidgets/styles/jqx.bootstrap.css" rel="stylesheet"> 
			
		<!-- MetisMenu CSS -->
		<link href="<?=base_url();?>css/metisMenu.min.css" rel="stylesheet" type="text/css">

		<!-- Custom Fonts -->
		<link rel="stylesheet" href="<?=base_url();?>css/font-awesome-4.5.0/css/font-awesome.css">
		
		<script type="text/javascript" src="<?=base_url();?>js/jquery-1.10.2.min.js"></script> 

		<!-- Bootstrap Core JavaScript -->
		<script type="text/javascript" src="<?=base_url();?>js/bootstrap.min.js"></script> 
		<script type="text/javascript" src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script> 
		<script type="text/javascript" src="<?=base_url();?>js/metisMenu.min.js"></script>
	    <script type="text/javascript" src="<?=base_url();?>js/sb-admin-2.js"></script>
		
	 	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxcore.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxdata.js"></script>
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxscrollbar.js"></script>
		<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxtree.js"></script>
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxbuttons.js"></script>
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxpanel.js"></script>  
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxlistbox.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxradiobutton.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxwindow.js"></script>      
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxtabs.js"></script>     			
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxdropdownlist.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxinput.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxnumberinput.js"></script>
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxtooltip.js"></script>       
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxdatetimeinput.js"></script> 
    	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxcalendar.js"></script> 
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/globalization/globalize.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxrangeselector.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxvalidator.js"></script>
	
	<!-- bootstrap-select https://silviomoreto.github.io/bootstrap-select/ -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="<?=base_url();?>js/bootstrap-select-1.12.4/dist/css/bootstrap-select.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="<?=base_url();?>js/bootstrap-select-1.12.4/js/bootstrap-select.js"></script>

    
</head>
<style>
	body {background-image: url("<?=base_url();?>img/bg_body.jpg");
	}
        .nav_principal{
            margin-bottom: 0px;
        }
</style>
<body>
	<div id="wrapper">        
		<nav class="navbar navbar-fixed-top navbar-inverse nav_principal" role="navigation">
			<div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">
						<img src="<?=base_url();?>img/logo.png" class="logo_brand" alt="">
					</a>
				</div>
            <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<!-- ACCESOS DE LA DERECHA-->
					<ul class="nav navbar-nav navbar-right">
						<li class="hidden">
							<a href="#page-top"></a>
						</li>

						<li class="page-scroll">
							<a href="<?=base_url();?>login/logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
						</li>
					</ul>				
					<!-- EL MENU -->
					<ul class="nav navbar-nav">
						<li><a href="<?=base_url();?>fichapacientes"><i class="fa fa-archive fa-fw"></i>Pacientes</a>						</li>
						<li><a href="<?=base_url();?>main/agenda"><i class="fa fa-calendar-plus-o fa-fw"></i>Agenda de turnos</a>						</li>
						<li><a href="<?=base_url();?>main/profesional"><i class="fa fa-user-md fa-fw"></i>Profesionales</a>						</li>
						<!--<li><a href="<?=base_url();?>calendar"><i class="fa fa-calendar fa-fw"></i>Agenda</a></li> -->
						<!-- <li><a href="<?=base_url();?>intervenciones"><i class="fa fa-stethoscope fa-fw"></i>Intervenciones</a></li> -->
					</ul>
				</div>         
			</div>
		</nav>	               
	</div>
</body>  
</html>