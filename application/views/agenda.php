 <!DOCTYPE html>
 <html>
     <head>
         <style>
         div.container { max-width: 1200px }
         button{
             min-width: 100px;
             max-width: 150px;
         }
         table {
            table-layout: fixed;
            word-wrap: break-word;
        }


        div.container { max-width: 1200px }  
        .modal-dialog {
            max-width: 350px;
        }
        @media(max-width:768px) {
            .modal-dialog {
                max-width: 300px;
            }
        }  
        td.details-control {
                text-align:center;
                color:forestgreen;
        cursor: pointer;
        }
        tr.shown td.details-control {
            text-align:center; 
            color:red;
        }
        table td {
        word-wrap: break-word;
        max-width: 400px;
        }
        #pendientes td {
        white-space:inherit;
        }
        .table-responsive{
            height: 65%;
        }

     </style>
        <link rel="stylesheet" href="<?=base_url();?>js/bootstrap3-dialog-master/dist/css/bootstrap-dialog.min.css" >              
        <script src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>   
        <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/media/css/jquery.dataTables.css" >
        <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/css/dataTables.responsive.css" >             	
        <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	     
    </head>

      <body>
         <div id="wrapper">
             <div id="page-wrapper">
                 <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
                 <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">   
                     <div class="panel panel-default" id="fichaTabla">
                         <div class="panel-heading">                             
                              <div class="form-group row">
                                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                      <h2>Agenda de turnos </h2>
                                  </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label>Profesional:</label>
                                    <select id="profSelect" class="form-control selectpicker" data-live-search="true" title="Seleccione un Profesional..." data-width="70%">                                        
                                    </select>
                                </div>
                            </div>
                         </div>
                         <div class="panel-body">
                         <div class="table-responsive">
                            <table id="turnos" class="hover table table-bordered responsive nowrap compact" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Turno</th>
                                        <th>Descripción</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>                      
                         </div>
                         <div class="panel-footer"> 
                             <div class="row">
                                 <div class="col-lg-4 col-lg-4 col-sm-4 col-xs-12"></div>
                                 <div class="col-lg-4 col-lg-4 col-sm-4 col-xs-12"></div>              
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>  
             </div>
         </div>
     </body>
     <script type="text/javascript">    
        var anterior = 0;
        var idTurno = 0;
        var idPaciente = 0;
        var pacienteNombre = '';
        var prof = 0;
        var descrip = '';
        var turno;
        var nombre ='';
        var apellido='';
        var urlBase = "<?php echo base_url(); ?>";       
        var table;
        var profesionales;
        var idProfSelectSession=0;

        $(document).ready(function() {
            table = $('#turnos').DataTable( {
                "sAjaxSource": "<?=base_url();?>main/get_turnos_main",
                "sAjaxDataProp": "",
                "info": false,
                select: {
                    style: 'os'
                },
                language: {
                    "sProcessing":     "Procesando...",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "No hay registros" ,
                    "search": "_INPUT_",	
                    "searchPlaceholder": "Buscar..."													
                },
                "responsive": false,
                "scrollY": 300,     
                "paging": false,
                "columns": [
                    {
                     "className": 'details-control',
                     "orderable": false,
                     "data": null,
                     "defaultContent": '',
                     "orderable": false,
                     "render": function () {
                                    return '<i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i>';
                                },
                     width:"15px"
                     },
                    { "data": "id_paciente"},    
                    { "data": "id_turno" },
                    { "data": "id_profesional" },
                    { "data": "notas" },
                    { "data": "estado" },
                    { "data": "turno",
                      createdCell: function (td, cellData, rowData, row, col) {
                                        if( rowData.estado == 'warning' ) {
                                              $(td).addClass('danger');
                                          }
                                      }
                    },
                    { "data": "descripcion" },
                    { "data": "nombre"},
                    { "data": "apellido"},                    
                    { "orderable": false, 
                        data: null,
                        width: 90,
                        className: "center",
                        defaultContent: '<div class= "row">'+
                                            '<div class="col-lg-11 col-sm-11 col-xs-12">'+
                                                '<button type="button" class="btn-eliminar btn btn-outline col-lg-4 col-sm-4 col-xs-4" data-toggle="tooltip" title="Eliminar turno"><i class="fa fa-trash-o fa-lg" style="color:#286090"></i></button>'+
                                            '</div>'+
                                        '</div>'
                    }
                ],
                "columnDefs": [
                    {
                        "targets": [1,2,3,4,5],
                        "visible": false,
                        "searchable": false
                    }               
                ]
            }) ;
                //Obtener lista de profesionales ajax
                $.ajax({
                        async: false,
                        url: "<?=base_url();?>main/load_profesionales_session",
                        success: function (data, status, xhr) {
                            profesionales = $.parseJSON(data).profesionales;   
                            idProfSelectSession= $.parseJSON(data).idProf.id_profesional;
                        }
                    });               
                //cargar la lista de profesinales
                $("#profSelect").append('<option value='+0+'>' + "Seleccionar..." + '</option>');
                $.each(profesionales,function(key, value){
                    if(value.idProf==idProfSelectSession){
                        $("#profSelect").append('<option value=' + value.idProf + ' selected>' + value.nombre + '</option>');
                    }else
                        $("#profSelect").append('<option value=' + value.idProf + '>' + value.nombre + '</option>');
                });
                
                $('#profSelect').on('change', function() {
                     table.ajax.url("<?=base_url();?>main/get_turnos_main_profesional/"+this.value ).load();
                });
                
            $('#turnos tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var tdi = tr.find("i.glyphicon");
                var row = table.row(tr);
                idTurno = row.data().id_turno;
                idPaciente = row.data().id_paciente;
                pacienteNombre = row.data().nombre + " " + row.data().apellido;
                prof = row.data().id_profesional;
                descrip = row.data().descripcion.replace(/\n/g, "<br />");
                turno = row.data().turno;
                nombre = row.data().nombre;
                apellido = row.data().apellido;
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                    tdi.first().removeClass('glyphicon-arrow-up');
                    tdi.first().addClass('glyphicon-arrow-down');
                }
                else {
                    // Open this row
                    if(anterior != 0 && anterior != tr){
                        var rowant = table.row(anterior);
                        var tdiant = anterior.find("i.glyphicon");
                        rowant.child.hide();                        
                        anterior.removeClass('shown');
                        tdiant.first().removeClass('glyphicon-arrow-up');
                        tdiant.first().addClass('glyphicon-arrow-down');                    
                    }; 
                    anterior = tr;
                    row.child(format(row.data())).show();                    
                    tr.addClass('shown');
                    tdi.first().removeClass('glyphicon-arrow-down');
                    tdi.first().addClass('glyphicon-arrow-up');
                }
             });

            table.on("user-select", function (e, dt, type, cell, originalEvent) {
             if ($(cell.node()).hasClass("details-control")) {
                 e.preventDefault();
             }
            });
            function format(d){        
                // `d` is the original data object for the row
                var retorno ='<div class="panel panel-default">';
                if(d.notas){
                    retorno = retorno + '<div class="panel-body">'+d.notas+'</div>';          
                }  
                retorno = retorno + '<div class="panel-footer">\n\
                                        <div class="btn-group btn-group-justified btn-group-md">\n\
                                            <div class="btn-group"><button type="button" class="btn btn-success btn-datos">Datos</button></div>\n\
                                            <div class="btn-group"><button type="button" class="btn btn-success btn-anamnesis">Anamnesis</button></div>\n\
                                            <div class="btn-group"><button type="button" class="btn btn-success btn-medidas">Medidas</button></div>\n\
                                            <div class="btn-group"><button type="button" class="btn btn-success btn-historia">Nueva Historia</button></div>\n\
                                        </div>\n\
                                    </div>';                
                return retorno+'</div>';
           } 
        });
     
        
        $('#turnos tbody').on( 'click', 'button.btn-eliminar', function (e) {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                idTurno = row.data().id_turno;
                BootstrapDialog.show({
                 type: BootstrapDialog.TYPE_WARNING,
                 title: 'Atenci&oacute;n!!',
                 message: "Confirma eliminar el turno seleccionado?",
                 draggable: true,								
                 buttons: [{
                         label: 'Si',
                         action: function(dialogRef){   
                             var url = urlBase + "main/eliminar_turno/"+idTurno;                                                     
                             $.ajax({
                                 type: "DELETE",
                                 dataType: "json",
                                 url: url,
                                 error: function(err){
                                     if(err.status == 200){
                                         console.log('no hay error'); 
                                         location.reload();
                                     }else console.log('hay error');
                                 },                          
                                 success:  function (data) {
                                     if(!data.valido){
                                         BootstrapDialog.show({
                                             type: BootstrapDialog.TYPE_DANGER,
                                             title: 'Error!!',
                                             message: 'No se pudo eliminar el turno',
                                             draggable: true,								
                                             buttons: [{
                                                     label: 'Ok',
                                                     action: function(dialogRef){dialogRef.close();}
                                                 }]
                                         });  
                                     }else{                                                                   
                                         location.reload();
                                     }                                                            
                                 }
                             });
                             dialogRef.close();
                         }
                     },
                     {
                         label: 'No',
                         action: function(dialogRef){dialogRef.close();}
                     }
                 ]
             });
        
         }); 
         $('#turnos tbody').on( 'click', '.btn-datos', function (e) {
                //var paciente = $(this).closest('tr').children('td.idPaciente').text(); 
                window.location.href = "<?php echo base_url().'fichapacientes/load_filiatorio/';?>"+idPaciente;
                return false;
         });
         $('#turnos tbody').on('click', '.btn-anamnesis', function (e) {
            // var paciente = $(this).closest('tr').children('td.idPaciente').text(); 
             //var pacienteNombre = $(this).closest('tr').children('td.nomApe').text(); 
             var pacienteArray = {"id":idPaciente,"nombreApe":pacienteNombre}; 
             var url = urlBase + "fichapacientes/pacienteCI";
             $.ajax({type: "POST",
                 url: url,
                 data: pacienteArray,
                 success: function(data){
                     window.location.href = "<?php echo base_url().'fichapacientes/load_anamnesis/';?>"+data.clave_random;
                 },
                 dataType: "json"
             });
                     
            return false;
         }); 
         $('#turnos tbody').on( 'click', '.btn-historia', function (e) {
            // var idTurno = $(this).closest('tr').children('td.idTurno').text();
             //var prof = $(this).closest('tr').children('td.idProf').text();
             //var descrip = $(this).closest('tr').children('td.descrip').text().replace(/\n/g, "<br />");
             //var turno = $(this).closest('tr').children('td.turno').text(); 
             //var idPaciente = $(this).closest('tr').children('td.idPaciente').text(); 
             //var nombre = $(this).closest('tr').children('td.nombre').text(); 
             //var apellido = $(this).closest('tr').children('td.apellido').text(); 

             var url = urlBase + "tratamientos/asignarCITrat";  
             var action = "asignar"       
             var trat = {"action":action,"id":idPaciente,"nombre":nombre,"apellido":apellido,"prof":prof,"descrip":descrip,"turno":turno,"idTrat":idTurno}; 
             
             $.ajax({type: "POST",
                 url: url,
                 data: trat,
                 success: function(data){
                     window.location.href = "<?php echo base_url().'tratamientos/asignar_histo/';?>"+data.clave_random;
                 },
                 dataType: "json"
             });
         });
        $('#turnos tbody').on('click', '.btn-medidas', function (e) {
            var pacienteArray = {"id":idPaciente,"nombreApe":pacienteNombre}; 
             var url = urlBase + "fichapacientes/pacienteCI";
             $.ajax({type: "POST",
                 url: url,
                 data: pacienteArray,
                 success: function(data){
                     window.location.href = "<?php echo base_url().'fichapacientes/load_medidas/';?>"+data.clave_random;
                 },
                 dataType: "json"
             });
             
            return false;
         }); 

     </script>
 </html>	
      