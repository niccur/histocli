<!DOCTYPE html>
<html>
    <body>
	<div id="wrapper">             
            <div id="page-wrapper">                    
                <div class="panel panel-default" id="anamnesis">
                    <div class="panel-heading">
                        <h4><span id="anamnesisTitle"></span></h4>
                    </div>
                    <div class="panel-body">
                        <div id="rowAnamnesis"  class="row">                                        
                            <form id="formAnamnesis" role="form">
                                    <fieldset>                                                
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Antecedentes Patologicos: </label>
                                                <textarea name="patologicos" id="txtPatologicos" class="form-control" rows="3"></textarea>                                                       
                                            </div> 
                                            <div class="col-lg-6">
                                                <label>R&iacute;tmo Menstrual: </label>
                                                <textarea name="rm" id="txtRm" class="form-control" rows="3"></textarea>                                                       
                                            </div>  
                                            <div class="col-lg-6">
                                                <label>H&aacute;bito Alimentario: </label>
                                                <textarea name="alimentario" id="txtAlimentario" class="form-control" rows="3"></textarea>                                                       
                                            </div> 
                                            <div class="col-lg-6">
                                                <label>Alergias: </label>
                                                <textarea name="alergias" id="txtAlergias" class="form-control" rows="3"></textarea>                                                       
                                            </div>
                                            <div class="col-lg-6">
                                                    <div class="col-lg-3">
                                                        <label>Actividad F&iacute;sica: </label>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="fisica" id="opFisica1" value=0>Alta
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                               <input type="radio" name="fisica" id="opFisica2" value=1>Media
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                               <input type="radio" name="fisica" id="opFisica3" value=2>Baja
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="fisica" id="opFisica4" value=3>Nula
                                                            </label>
                                                        </div> 
                                                    </div>  
                                                    <div class="col-lg-3">
                                                        <label>Alcohol: </label>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="alcohol" id="opAlcohol1" value=0>Alto
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="alcohol" id="opAlcohol2" value=1>Medio
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="alcohol" id="opAlcohol3" value=2>Bajo
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="alcohol" id="opAlcohol4" value=3>Abstemio
                                                            </label>
                                                        </div>
                                                    </div>   
                                                    <div class="col-lg-3">
                                                        <label>Fumador: </label>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="tabaco" id="opTabaco1" value=0>No
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="tabaco" id="opTabaco2" value=1>Si
                                                            </label>
                                                        </div>
                                                    </div> 
                                                    <div class="col-lg-3">
                                                        <label>Sol: </label>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="sol" id="opSol1" value=0>Mucho
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="sol" id="opSol2" value=1>Medio
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="sol" id="opSol3" value=2>Poco
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="sol" id="opSol4" value=3>Nada
                                                            </label>
                                                        </div>
                                                    </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                  <label>Sue&ntilde;o: (hh:mm)</label>
                                                <input type="text" name="sueno" id ="txtSueno" class="form-control" onkeypress="return isNumberKeyTime(event)">
                                            </div> 
                                            <div class="col-lg-6">
                                                <label>Observaciones: </label>
                                                <textarea name="observaciones" id="txtObservaciones" class="form-control" rows="3"></textarea>                                                       
                                            </div>
                                        </div>
                                    </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button id="btnAnamnesis" type="button" class="btn btn-primary">Guardar</button>
                        <button id="btnCancelAnamnesis" type="button" class="btn btn-primary">Volver</button>
                    </div>                                
                </div>                 
            </div>
        </div>
    </body>
        <script type="text/javascript" src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>       
        <script type="text/javascript" src="<?=base_url();?>application/views/histoanamnesis.js"></script>  
      
        <script type="text/javascript">    
             var cambioAnamnesis = false;
             var jsonAnamnesis = null;
             var vSexo;
             var urlBase = "<?php echo base_url(); ?>";
             var idPacienteJs = 0;  
             var urlAnamnesisRun = null;             
             var vFisica = null;
             var vAlcohol = null;
             var vTabaco = null;
             var vSol = null;
             var validateDecimal = true;             
             
                $("#formAnamnesis").jqxValidator({

                    hintType: 'label',
                    animationDuration: 500,  
                    //aca agregar una rule para validar de que se haya ingresado un paciente..eu el idPaciente !=null
                    onSuccess: function(){
                        console.log(urlAnamnesisRun);
                        console.log(idPacienteJs);
                        var parametros = { 
                            "idPaciente" :idPacienteJs ,
                            "patologicos" :  $("#txtPatologicos").val().trim(),
                            "rm" : $("#txtRm").val().trim(),
                            "alimentario" : $("#txtAlimentario").val().trim(),
                            "alergias" : $("#txtAlergias").val().trim(),
                            "sueno" : $("#txtSueno").val(),
                            "observaciones" : $("#txtObservaciones").val().trim(),
                            "fisica" : vFisica,
                            "alcohol" : vAlcohol,
                            "tabaco" : vTabaco,
                            "sol" : vSol
                        };                                    
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: urlAnamnesisRun,
                            data: parametros,
                            success:  function (data) { 
                                if(!data.valido){
                                    //idPacienteJs = 0;
                                    cargarAnamnesis();
                                    BootstrapDialog.show({
                                            type: BootstrapDialog.TYPE_DANGER,
                                            title: 'Error!!',
                                            message: 'Los cambios no pudieron ser salvados',
                                            draggable: true,								
                                            buttons: [{
                                                        label: 'Ok',
                                                        action: function(dialogRef){dialogRef.close();}
                                                      }]
                                    });
                                }else{                                   
                                    cambioAnamnesis = false;
                                    cargarAnamnesis();
                                    BootstrapDialog.show({
                                            onshown: function(dialogRef){
                                                            setTimeout(function(){
                                                            dialogRef.close();
                                                            }, 2000);
                                                    },
                                            onhide: function(dialogRefh){
                                                //location.replace(document.referrer); //haciendo así hace el back con el refresco de la pantalla
                                                window.history.back();
                                            },
                                            type: BootstrapDialog.TYPE_SUCCESS,
                                            title: 'Informaci&oacute;n',
                                            message: 'Los cambios fueron salvados satisfactoriamente',
                                            draggable: true,								
                                            buttons: [{
                                                        label: 'Ok',
                                                        action: function(dialogRef){dialogRef.close();}
                                                    }]
                                    });                                                                															   
                                }
                           }
                        });
                  }                                
                }); 
             
            $(document).ready(function () { 
                var nombre = "<?php echo $nombre;?>";                
                idPacienteJs =  <?php echo $idPaciente ;?>; 
                $("#anamnesisTitle").html("Anamnesis de "+ nombre);
                cargarAnamnesis();
            // FORM ANAMNESIS ---------------------------------------------------------   
                $('input[name="fisica"]').change(function(){
                   //var genderValue = $(this).next('label').text()
                   vFisica = $(this).val();
                });
                $('input[name="alcohol"]').change(function(){
                   //var genderValue = $(this).next('label').text()
                   vAlcohol = $(this).val();
                });                 
                $('input[name="tabaco"]').change(function(){
                   //var genderValue = $(this).next('label').text()
                   vTabaco = $(this).val();
                });      
                $('input[name="sol"]').change(function(){
                   //var genderValue = $(this).next('label').text()
                   vSol = $(this).val();
                });
               $('#rowAnamnesis input').change(function() { 
                    if(cambioAnamnesis===false){
                        cambioAnamnesis = true;
                    }    
               }); 
               $('#rowAnamnesis textarea').change(function() { 
                    if(cambioAnamnesis===false){
                        cambioAnamnesis = true;
                    }    
               });
               
               $("#btnCancelAnamnesis").click(function() {  
                   cargarAnamnesis();
                   //location.replace(document.referrer); //haciendo así hace el back con el refresco de la pantalla
                   window.history.back();
                });

                $('#btnAnamnesis').on('click', function(){	
                    urlBase = "<?php echo base_url(); ?>";
                    if(cambioAnamnesis){
                        $('#formAnamnesis').jqxValidator('validate');                       
                    }
                });
		
                
            });
        </script>	
</html>	
      