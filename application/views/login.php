
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Historias Clinicas</title>

<script type="text/javascript">

	$(document).ready(function () 
	{    
		var theme = 'energyblue';
         //***********************************************************************************
        $('#ingresarButton').jqxButton({ width: 120, height: 30 , theme:theme});
        $("#txtusuario").jqxInput({placeHolder: "Nombre de Usuario", height: 25, width: 200, minLength: 1 });
		$("#txtclave").jqxInput({placeHolder: "Clave", height: 25, width: 200, minLength: 1 });
		
		
		$('#ingresarButton').on('click', function () {
			$('#loginForm').jqxValidator('validate');
        });
        
		$('#txtclave').keydown(function (e){
			if(e.keyCode == 13){
				$('#loginForm').jqxValidator('validate');
			}
		})

		if (theme.length > 0) {
			$('.text-input').addClass('jqx-input-' + theme);
		}
           
        $('#loginForm').jqxValidator({
			hintType: 'label',
            rules: [
				{ input: '#txtusuario', message: 'Ingrese usuario', action: 'keyup, blur', rule: 'required',  arrow: false},
				{ input: '#txtclave', message: 'Ingrese clave', action: 'keyup, blur', rule: 'required', arrow: false }
			], 
			theme: theme, 
			onSuccess: function()
			{
				var parametros = {
	                "txtusuario" : $("#txtusuario").val(),
	                "txtclave" : $("#txtclave").val()
				};

				$.ajax(
				{
					type: "POST",
					dataType: "json",
					url: "<?=base_url();?>login/validarLogin",
					data: parametros,
					beforeSend: function () {
						$('#ventLogin').ajaxloader();
					},					
					success:  function (data) {
						if (data.valido == false)
						{
							$('#ventLogin').ajaxloader('hide');
							new Messi('Nombre de usuario o contraseña incorrecta', 
								{modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
							);
						}
						else if (data.valido == true)
						{
							if (data.estado == 0){ 							
								$('#ventLogin').ajaxloader('hide');
								new Messi('El usuario se encuentra inactivo', 
									{modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
								)
							} else {
								var url = '<?=base_url();?>main';
								alert('banca1');
								//$.post(url, $("#loginForm").serialize() );
								$(location).attr('href',url);
							}
						}
	                }
				});
			}
		});
		$('#txtusuario').focus();
	});  

</script>
</head>

<style> 
        #mainer { width: 980px; margin: 0 auto; } 
        #loginForm { width: 300px; height: 400px; margin: 20 auto; } 
        #usr{ width: 270px; margin-top:  0 auto;}
        #pass{ width: 270px; margin-top: 0 auto;}
        #txts{ width: 200px;  height: 50px; margin-top: 20px;}
        #boton{ width: 200px;  height: 30px margin-top: 20px;}
        #t1{float: left}
        #t2{float: right}
        #p1{float: left}
        #p2{float: right}
</style> 
                
<body>
	<div align="center" id="mainer" > 
		<div id="loginForm"  >
                    <div id="txts">
                            <div id="usr">  
                                <div id="t1" >    
                                    Usuario
                                </div>
                                <div id="t2"   >    
                                    <input type="text" id="txtusuario" />
                                </div>
                            </div>
                            <div id="pass"> 
                                <div id="p1" >   
                                    Clave  
                                </div>                                 
                                <div id="p2" >   
                                    <input type="password" id="txtclave" />   
                                </div>
                            </div>
                    </div>
                    <div id="boton"> 
                        <input type="button" value="Ingresar" id="ingresarButton"  />
                    </div>		
		</div>
	</div>
</body>

