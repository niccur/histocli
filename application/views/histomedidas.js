function loadMedida(){
    $("#medidasModalLabel").html("Editar Medida");
    valido_fecha_medida = true;
    cambioMedida = false;
    urlMedidasRun = urlBase + 'histo/update_medida';
    var jsonMedida = null;
    $.ajax({async: false,
            type: "POST",
            dataType: "json",
            data:{"idMedida": idMedida},
            url: urlBase + 'histo/load_medida',
            success: function (datax, status, xhr) {
                                jsonMedida = datax;
                        }
        });

    $("#fec_medida").val(jsonMedida.fecha);
    $("#txtBrazo").val(jsonMedida.brazo);
    $("#txtBusto").val(jsonMedida.busto);                                  
    $("#txtCintura").val(jsonMedida.cintura);                    
    $("#txtCaderaAlta").val(jsonMedida.cadera_alta);
    $("#txtCaderaBaja").val(jsonMedida.cadera_baja);
    $("#txtGluteos").val(jsonMedida.gluteos);
    $("#txtMusloSuperior").val(jsonMedida.muslo_superior);
    $("#txtMusloInferior").val(jsonMedida.muslo_inferior);
    $("#txtRodilla").val(jsonMedida.rodilla);
    $("#txtPantorrilla").val(jsonMedida.pantorrilla);
    $("#txtTobillo").val(jsonMedida.tobillo);
}

function newMedida(){
    $("#medidasModalLabel").html("Nueva Medida");
    valido_fecha_medida = true;
    cambioMedida = false;
    urlMedidasRun = urlBase + 'histo/add_medida';
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
    today = dd+'/'+mm+'/'+yyyy;
    
    $("#fec_medida").val(today);
    $("#txtBrazo").val(null);
    $("#txtBusto").val(null);                                  
    $("#txtCintura").val(null);                    
    $("#txtCaderaAlta").val(null);
    $("#txtCaderaBaja").val(null);
    $("#txtGluteos").val(null);
    $("#txtMusloSuperior").val(null);
    $("#txtMusloInferior").val(null);
    $("#txtRodilla").val(null);
    $("#txtPantorrilla").val(null);
    $("#txtTobillo").val(null);
}


$('#tablaMedidas tbody').on( 'click', 'button.eliminarMedida', function (e) { 

                    e.preventDefault();                              

                    var table = $('#tablaMedidas').DataTable();
                    var parentRow = $(this).parent().parent();
                    var rowData = table.row( parentRow ).data();
                    idMedida = rowData["id"];
                    BootstrapDialog.show({
                            type: BootstrapDialog.TYPE_WARNING,
                            title: 'Atenci&oacute;n!!',
                            message: 'Confirma eliminar la medida?',
                            draggable: true,								
                            buttons: [{
                                        label: 'Si',
                                        action: function(dialogRef){
                                                var parm = {"idMedida" :idMedida};
                                                $.ajax({
                                                        type: "POST",
                                                        dataType: "json",
                                                        url: urlBase + 'histo/elimina_medida',
                                                        data: parm,
                                                        success:  function (data) {
                                                            if(!data.valido){
                                                              BootstrapDialog.show({
                                                                        type: BootstrapDialog.TYPE_DANGER,
                                                                        title: 'Error!!',
                                                                        message: 'No se pudo eliminar el registro',
                                                                        draggable: true,								
                                                                        buttons: [{
                                                                                    label: 'Ok',
                                                                                    action: function(dialogRef){dialogRef.close();}
                                                                                  }]
                                                                });  
                                                            }else{
                                                                tableMedidas.ajax.reload(null);
                                                            }                                                            
                                                        }
                                                });
                                                dialogRef.close();
                                            }
                                      },
                                      {
                                        label: 'No',
                                        action: function(dialogRef){dialogRef.close();}
                                      }
                                  ]
                    });
                    tableMedidas.ajax.reload(null);

}); 


