<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
		<link href='<?=base_url();?>js/fullcalendar/fullcalendar.css' rel='stylesheet' />
		<link href='<?=base_url();?>js/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
        <script src='<?=base_url();?>js/fullcalendar/lib/moment.min.js'></script>
		<script src='<?=base_url();?>js/fullcalendar/lib/jquery.min.js'></script>
		<script src='<?=base_url();?>js/fullcalendar/fullcalendar.min.js'></script>
<script>

	var profs = [7]; 
	
	$(document).ready(function() {
			var estadoTurno=1;
            var pacientes;
            var profesionales;
  //          var listBox = $('#selector');
          //  var lstProfesionalesTurno = $('#profesionalesTurno');
          //  var lstPacientes = $('#pacientes');
            var source = null;
          //  var fechaTurno = $('#fechaTurno');
          //  var turnoDescrip = $('#turnoDescrip');
		//	var fechaAgenda = $('#fechaAgenda');
			//var lstProfesionalesAgenda = $('#profesionalesAgenda');
			//var agendaDescrip = $('#agendaDescrip');
			var rangoStart;
			var rangoEnd;
		
                $('#calendar').fullCalendar({
                    header: {
                            left: 'prev,next   today',
                            center: 'prevYear title nextYear',
                            right: 'month,agendaWeek,agendaDay'
                    },
					eventLimit: true,
                    aspectRatio: 1.8,
                    editable: false,
                    allDaySlot: true,
                    selectable: true,
                    selectHelper: true,
                    hiddenDays: [], // ocultar dias
                    slotDuration: '00:15:00',   
					lang: 'ES',
                    theme: true,
                    allDayText: 'Todo el dia',
                    minTime: '06:00:00',
                    buttonText: {
                                    today:    'Hoy',
                                    month:    'Mes',
                                    week:     'Semana',
                                    day:      'Dia'
                                },
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
                                 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                                      'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles','Jueves', 'Viernes', 'Sabado'],
                    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],

//el evento del calendario de cuando pincha y arrastra un nuevo evento
                    select: function( start, end, jsEvent, view ) {

                                   if ((view.name == 'month' && ((end - start)> 86400000)) 
                                       || (view.name != 'month'))  { 
											
                                           // var title = prompt('Event Title:');
                                           // var idProf = prompt('Id del profesional:');
                                            //var url = prompt('Url:');
                                           // var eventData;
										   										   
											rangoStart = start.format();
											rangoEnd = end.format();
											var fDesde = new Date(Date.parse(rangoStart));
											var fHasta = new Date(Date.parse(rangoEnd));											

											var horaFormat = function(fecha){
														var lpad = function lpad(originalstr, ancho, strToPad) {
																				 var x = originalstr;
																				while (x.length < ancho)
																					x = strToPad + x;
																				return x;
																			};		                                        									
														var xfecha = new Date(fecha);
														var h = xfecha.getHours();
														var m = xfecha.getMinutes();

														return lpad(h.toString(),2,'0')+':'+lpad(m.toString(),2,'0')+"Hs";
												}; 

											var fechaFormat = function(fecha,suma){
														var lpad = function lpad(originalstr, ancho, strToPad) {
																				 var x = originalstr;
																				while (x.length < ancho)
																					x = strToPad + x;
																				return x;
																			};		                                        									
														var xfecha = new Date(fecha);
														if(suma){xfecha.setDate(xfecha.getDate()+1);}
														var d = xfecha.getDate();														
														var m = xfecha.getMonth()+1; 
														var y = xfecha.getFullYear(); 

														return lpad(d.toString(),2,'0')+'/'+lpad(m.toString(),2,'0')+'/'+y;
												}; 
                                        	/*
												switch((fHasta.getDate() - fDesde.getDate())) {
														case 0:
															var html = fechaFormat(fDesde,false);
															$("#fechaTurno").html(html);
															$("#fechaAgenda").html(html);
															html = 'de '+horaFormat(fDesde)+' a '+horaFormat(fHasta);
															$("#horarioTurno").html(html);
															$("#horarioAgenda").html(html);
															break;
														case 1:
															var html = fechaFormat(fDesde,true);
															$("#fechaTurno").html(html);
															$("#fechaAgenda").html(html);
															html = 'Todo el dia';
															$("#horarioTurno").html(html);
															$("#horarioAgenda").html(html);
															break;
														default:
															    var html = 'Desde '+fechaFormat(fDesde,true)+' hasta '+fechaFormat(fHasta,false);
																$("#fechaTurno").html(html);
																$("#fechaAgenda").html(html);
																html = 'Todo el dia';
																$("#horarioTurno").html(html);
																$("#horarioAgenda").html(html);
															
													 }
													
													turnoDescrip.val('');	
													agendaDescrip.val('');	
													lstProfesionalesAgenda.val();
													lstProfesionalesTurno.val();
													lstPacientes.val();
													$('#ventanaEvento').jqxWindow('open');
													*/
											
                                        //    if (title) {
                                         //       $.ajax({                                                       
                                        //                    url: "<?=base_url();?>calendar/add_eventos",
                                         //                   data: 'title='+ title +'&start='+ startf +'&end='+ endf +'&url='+ url+'&idProf='+idProf ,
                                        //                    type: "POST",
                                        //                    success: function(json) {
                                        //                                     if(json =='false'){  
                                        //                                        alert('No se permite solapamiento en un evento existente');
                                        //                                    }
                                        //                        }
                                        //            });                                       
                                             //  $('#calendar').fullCalendar( 'refetchEvents' );
                                                                                        
                                    }
                                       $('#calendar').fullCalendar( 'refetchEvents' );
                            },
//el evento del calendario de cuando arrastra un evento existente
                    eventDrop: function(event, delta, revertFunc) {             

                                    if (!confirm("Confirma modificar el evento?")) {
                                        revertFunc();
                                    } else{
                                                                                            
                                                if(event.tipo == 'turno'){
													 $.ajax({  
														url: "<?=base_url();?>calendar/update_turno",
														data: 'id='+ event.id +
															  '&start='+ event.start.format() +
															  '&end='+ event.end.format()+
															  '&id_profesional='+event.idProfesional+
															  '&id_paciente='+event.idPaciente+
															  '&descrip='+event.descrip+
															  '&id_estadoturno='+event.idEstadoTurno+
															  '&editable='+event.editable+
															  '&ocupado='+event.ocupado,
														type: "POST",
														success: function(json) {
																	   if(json =='false'){  
																			alert('No se permite solapamiento en un evento existente');
																		}      
															   }
														}); 
                                                }else{
													$.ajax({  
															url: "<?=base_url();?>calendar/update_agenda",
															data: 'id='+ event.id +
																  '&start='+ event.start.format() +
																  '&end='+ event.end.format()+
																  '&id_profesional='+event.idProfesional+
																  '&descrip='+event.descrip+
																  '&editable='+event.editable+
																  '&ocupado='+event.ocupado,
																type: "POST",
																success: function(json) {
																			   if(json =='false'){  
																					alert('No se permite solapamiento en un evento existente');
																				}      
																	   }
														});														  
												}                                 
                                    }
                            },
//el evento del calendario de cuando 'agrando/achico' un evento existente y se extiende o acorta el periodo
                    eventResize: function(event, delta, revertFunc) {   
                                        if (!confirm("Confirma modificar el evento?")) {
                                            revertFunc();
                                        } else{
                                                if(event.tipo == 'turno'){
													 $.ajax({  
														url: "<?=base_url();?>calendar/update_turno",
														data: 'id='+ event.id +
															  '&start='+ event.start.format() +
															  '&end='+ event.end.format()+
															  '&id_profesional='+event.idProfesional+
															  '&id_paciente='+event.idPaciente+
															  '&descrip='+event.descrip+
															  '&id_estadoturno='+event.idEstadoTurno+
															  '&editable='+event.editable+
															  '&ocupado='+event.ocupado,
														type: "POST",
														success: function(json) {
																	   if(json =='false'){  
																			alert('No se permite solapamiento en un evento existente');
																		}      
															   }
														}); 
                                                }else{
													$.ajax({  
															url: "<?=base_url();?>calendar/update_agenda",
															data: 'id='+ event.id +
																  '&start='+ event.start.format() +
																  '&end='+ event.end.format()+
																  '&id_profesional='+event.idProfesional+
																  '&descrip='+event.descrip+
																  '&editable='+event.editable+
																  '&ocupado='+event.ocupado,
																type: "POST",
																success: function(json) {
																			   if(json =='false'){  
																					alert('No se permite solapamiento en un evento existente');
																				}      
																	   }
														});														  
												}                                        
                                        }
                            },           

                     //Cargo el calendario con los eventos registrados en la base de datos
                     //Se traen los datos y se carga el calendario solo con los eventos necesarios segun
                     //la visibilidad seleccionada del calendario y segun el profesional seleccionado

                      eventSources: [
                                    // traigo los turnos
                                    function(start, end, timezone, callback){
                                    $.ajax({
                                        url: "<?=base_url();?>calendar/load_turnos",																	            
                                        data: 'f1='+start.format()+'&f2='+end.format()+'&selections='+window.profs+'&estado='+estadoTurno,
                                        type: 'POST',
                                        dataType: 'json',
                                        success: function(doc, status, xhr ) {
                                                        var events = [];
                                                        $(doc).each(function() {

                                                                       // if($(this).attr('editable') == 'true'){																	                	
                                                                                events.push({
                                                                                    id: $(this).attr('id'),
                                                                                    title: $(this).attr('title'),
                                                                                    start: $(this).attr('start'),
                                                                                    end: $(this).attr('end'),
                                                                                    color: $(this).attr('color'),                                                                                                               
                                                                                    idProfesional: $(this).attr('id_profesional'),
																					idEstadoTurno: $(this).attr('id_estadoturno'),
                                                                                    idPaciente: $(this).attr('id_paciente'),
                                                                                    ocupado: $(this).attr('ocupado'),
                                                                                    descrip: $(this).attr('descrip'),
                                                                                    editable: $(this).attr('editable'),
                                                                                    tipo: 'turno'
                                                                                });
//                                                                })}else{
//                                                                        events.push({
//                                                                            id: $(this).attr('id'),
//                                                                            title: $(this).attr('title'),
//                                                                            start: $(this).attr('start'),
//                                                                            end: $(this).attr('end'),
//                                                                            color: $(this).attr('color'),
//                                                                            idProfesional: $(this).attr('id_profesional'),
//                                                                            idPaciente: $(this).attr('id_paciente'),
//                                                                            ocupado: $(this).attr('ocupado'),
//                                                                            descrip: $(this).attr('descrip'),
//                                                                            editable: $(this).attr('editable')
//                                                                        })																						                    	


                                                            });
                                                        callback(events);
                                             }
                                        })},                                                                                                                  
                                        //traigo las agendas
                                        function(start, end, timezone, callback){
                                            var view = $('#calendar').fullCalendar('getView');
																																	
											$.ajax({
                                                url: "<?=base_url();?>calendar/load_agendas",																	            
                                                data: 'f1='+start.format()+'&f2='+end.format()+'&selections='+window.profs+'&vista='+view.name,
                                                type: 'POST',
                                                dataType: 'json',
                                                success: function(doc, status, xhr ) {
                                                                var events = [];
																
                                                                $(doc).each(function() {

                                                                                //if($(this).attr('editable') == 'true'){																	                	
                                                                                        events.push({
                                                                                            id: $(this).attr('id'),
                                                                                            title: $(this).attr('title'),
                                                                                            start: $(this).attr('start'),
                                                                                            end: $(this).attr('end'),
                                                                                            editable: $(this).attr('editable'),
                                                                                            ocupado: $(this).attr('ocupado'),
                                                                                            color: $(this).attr('color'),                                                                                                               
                                                                                            idProfesional: $(this).attr('id_profesional'),                                                                                                                                                                                                                                
                                                                                            descrip: $(this).attr('descrip'),
                                                                                            idPaciente: null,
                                                                                            tipo: 'agenda'
                                                                                            });
//                                                                                })}else{
//                                                                                        events.push({
//                                                                                            id: $(this).attr('id'),
//                                                                                            title: $(this).attr('title'),
//                                                                                            start: $(this).attr('start'),
//                                                                                            end: $(this).attr('end'),
//                                                                                            color: $(this).attr('color'),
//                                                                                            idProfesional: $(this).attr('id_profesional'),
//                                                                                            idPaciente: $(this).attr('id_paciente'),
//                                                                                            ocupado: $(this).attr('ocupado'),
//                                                                                            descrip: $(this).attr('descrip'),
//                                                                                            editable: $(this).attr('editable')
//                                                                                        })																						                    	
//                                                                                }
                                                                    });
                                                                callback(events);
                                                            }
                                                }) 
                                             }                                                                                                                       
                                ],
//Al hacer clic en una fecha en modo MES, pongo la visualizacion
//del DIA
                      dayClick: function(date, jsEvent, view) {
                               // var view = $('#calendar').fullCalendar('getView');
                               if (view.name == 'month') { 
								   $('#calendar').fullCalendar( 'changeView', 'agendaDay'); 
								   $('#calendar').fullCalendar( 'gotoDate', date); 		
								   							   								   
                               }
                      },
//En la renderizacion de cada evento le meto el widget qtip2 del cartelito
//con la animacioncita de desplegacion
                     eventRender: function( event, element, view ) { 
                                        var start = Date.parse(event.start.format());
                                        var end = Date.parse(event.end.format());
                                        
                                        var fechaFormat = function(fecha){
                                                var lpad = function lpad(originalstr, ancho, strToPad) {
                                                                         var x = originalstr;
                                                                        while (x.length < ancho)
                                                                            x = strToPad + x;
                                                                        return x;
                                                                    };		                                        									
                                                var xfecha = new Date(fecha);
                                                var h = xfecha.getHours();
                                                var m = xfecha.getMinutes();

                                                return lpad(h.toString(),2,'0')+':'+lpad(m.toString(),2,'0')+"Hs";
                                        }; 

                                        var horaComienzo = fechaFormat(start);
                                        var horaFin = fechaFormat(end);
                                        var tipo = function(t){
                                                        if(t == 'turno'){
                                                                return 'Turno:';
                                                        }else return 'Agenda:';
                                                };

                                        var text = '<span class="letraTip2">'+tipo(event.tipo)+'</span></br><p>'+event.descrip+' </p><br/><p><span class="letraTip2">Comienzo: </span>'+horaComienzo+'<br/><span class="letraTip2">Fin: </span>'+horaFin+'</p>';
                                        element.qtip({																								          
                                                       content: {title: event.title,
                                                                 text: text},
                                                       position: {
                                                                     target: 'mouse', // Track the mouse as the positioning target
                                                                     adjust: { x: 5, y: 5 } // Offset it slightly from under the mouse
                                                                 },
                                                       show: {effect: function() {
                                                                           $(this).slideDown();
                                                                        }
                                                             },
                                                       hide: {effect: function() {
                                                                            $(this).slideUp();
                                                                        }
                                                             },
                                                       style: {
                                                                classes: 'qtip-light qtip-rounded qtip-shadow letraTip',	
                                                                widget: true
                                                              }																														   		
                                                     });																			    
                                        }                     

						});
		
	});

</script>

</head>
<body>

	<div id='calendar'></div>

</body>
</html>
