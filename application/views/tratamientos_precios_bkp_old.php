 <!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="<?=base_url();?>css/tratamientos.css" >
    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/media/css/jquery.dataTables.css" >
    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/css/dataTables.responsive.css" >             	
    <link rel="stylesheet" href="<?=base_url();?>js/bootstrap3-dialog-master/dist/css/bootstrap-dialog.min.css" >              

    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>   
        
    <script>
		var cabecera_id = 99999999999;
		var estado_cab = 0;
		var estado_det = 0;
		
        $(document).ready(function() {	
                                
            $('#altas').on('click', function(){
                $("#nuevo_tratamiento").modal();
            });     
            //-- TRATAMIENTOS ----------------------------------------------------------------------------------------------------------------------------------------------------
            function format_cab ( d ) {                
                var detalle = dt_det.data();
                //console.log(detalle);
                var precio = 0;
                var precioTot = 0;
                var precio_trat;
                var precio_int;
                
                detalle.each( function (d) {
                        var row_precio = rowPrecioDet(d.id);
                        if(row_precio.pesos_unit){
                          precio = row_precio.pesos_unit * row_precio.sesiones;  
                          precio_trat = ((precio * row_precio.porc_desc_trat)/100);
                          precio_int = (((precio -precio_trat)  * row_precio.porc_desc_intrv)/100);
                          precioTot = precioTot + (precio - precio_trat - precio_int);
                        }                      
                    } );
                    
                var salida = "<div>";
                    salida = salida +"<div><i class='fa fa-stethoscope'></i><div class='badge'>"+d.sesiones+"</div></div><br>";
                    salida = salida +"<div><h4><span class='label label-info'>&nbsp;$"+precioTot+"</span></h4></div>";
                    
                    if(d.descripcion){
                        salida = salida +"<div class='well well-lg'>"+d.descripcion +"</div>";
                    };
                    salida = salida +"</div>";               
                
                return salida;
            };
			
            var dt = $('#tratamientos').DataTable( {                
                "sAjaxSource": "<?=base_url();?>tratamientos/load_tratamientos", 
                "info": false,	
				"sAjaxDataProp": "",
				"bProcessing": true,
				"bServerSide": true,				
				"sServerMethod": "POST",					
				"fnServerParams": function ( aoData ) {
					  aoData.push( { "name": "estado", "value": estado_cab} );
					},				
                language: {
                            "sProcessing":     "Procesando...",
                            "sZeroRecords":    "No se encontraron resultados",
                            "sEmptyTable":     "No hay registros",
                            "search": "_INPUT_",
                            "searchPlaceholder": "Buscar..."						
                            }, 
                "fnInitComplete": function(oSettings, json) {
                          $('#tratamientos tbody tr:eq(0)').click();
                        },							
                "responsive": true,
                "scrollY": 300,     
                "paging": false,
                "autoWidth": false,
                "columns": [
                    {
                        "class":          "details-control",
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ""
                    },
                    { "data": "nombre" }
                ],
                "order": [[1, 'asc']]
                } );
                
            var detailRows = [];

             //Mostrar la descripcion de tratamientos (mas y menos)    ---------------------------------------------------------------------------
            $('#tratamientos tbody').on( 'click', 'tr td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = dt.row( tr );
                    var idx = $.inArray( tr.attr('id'), detailRows );
                    
                    if ( row.child.isShown() ) {
                        tr.removeClass( 'details' );
                        row.child.hide();

                        // Remove from the 'open' array
                        detailRows.splice( idx, 1 );
                    }
                    else {
                        tr.addClass( 'details' );
                         
                            row.child( format_cab( row.data() ) ).show();
                        // Add to the 'open' array
                            if ( idx === -1 ) {
                                detailRows.push( tr.attr('id') );
                            }                        
                    }
                } );
            //-------------------------------------------------------------------------------------
             dt.on( 'draw', function () {
                            $.each( detailRows, function ( i, id ) {                            
                                    $('#'+id+' td.details-control').trigger( 'click' );
                            } );
                    } );        
            //-- TRATAMIENTOS  fin-------------------------------------------------------------------------------------------------------------------------------------------------       
            //-- COMPOSICION DE TRATAMIENTOS ---------------------------------------------------------------------------------------------------------------
      
            function format_det ( d ) {
                var salida = "<div class='col-md-12'>";
                var precio;
                var rowP = rowPrecioDet(d.id);
                var rowString = JSON.stringify(rowP);
                //console.log(rowP);     
                
                    if(rowP.pesos_unit){
                       precio = rowP.pesos_unit * rowP.sesiones;
                       if(rowP.porc_desc_trat){                           
                           precio = precio - ((precio * rowP.porc_desc_trat)/100);                           
                       }
                       if(rowP.porc_desc_intrv){
                           precio = precio - ((precio * rowP.porc_desc_intrv)/100);
                       }
                       
                        salida = salida + "<div class='row'>";
                        if(rowP.dolares_unit){
                                salida = salida +"<div class='col-md-4'><h4><span class='label label-info'><i class='fa fa-hospital-o'></i>&nbsp;"+d.departamento+"</span></h4></div>";
                                salida = salida +"<div class='col-md-4'><h4><button onclick='verPrecio("+rowString+")' type='button' class='btn btn-success'>&nbsp;$ "+precio +"</button></h4></div>";
                                salida = salida +"<div class='col-md-4'><h4><button type='button' class='btn btn-default'>&nbsp;u$d "+rowP.dolares+"</button></h4></div>";
                        }else{
                                salida = salida +"<div class='col-md-6'><h4><span class='label label-info'><i class='fa fa-hospital-o'></i>&nbsp;"+d.departamento+"</span></h4></div>";
                                salida = salida +"<div class='col-md-6'><h4><button onclick='verPrecio("+rowString+")' type='button' class='btn btn-success'>&nbsp;$ "+precio+"</button></h4></div>";
                        } 
                        salida = salida + "</div>";	
                }else{
                    salida = salida + "<div class='row'>";
                    salida = salida +"<div class='col-md-4'><h4><span class='label label-info'><i class='fa fa-hospital-o'></i>&nbsp;"+d.departamento+"</span></h4></div>";
                    salida = salida + "</div>";
                }
                if(d.descrip){
                        salida = salida +"<div class='well well-lg'>"+d.descrip+"</div>"   
                };
                salida = salida +"</div>";               
                
                return salida;
            };
            var dt_det = $('#tratamiento_det').DataTable( {
                
                "sAjaxSource": "<?=base_url();?>tratamientos/load_tratamiento_det", 
                "sAjaxDataProp": "",
                "bProcessing": true,
                "bServerSide": true,				
                "sServerMethod": "POST",					
                "fnServerParams": function ( aoData ) {
                      aoData.push( { "name": "cabecera", "value": cabecera_id},
                                   { "name": "estado_det", "value": estado_det});
                        },
                "info": false,
                language: {
                            "sProcessing":     "Procesando...",
                            "sZeroRecords":    "No se encontraron resultados",
                            "sEmptyTable":     "No hay registros",
                            "search": "_INPUT_",
                            "searchPlaceholder": "Buscar..."						
                            }, 						 
                "responsive": true,
                "scrollY": 300,     
                "paging": false,
                "autoWidth": false,
                "columns": [
                    {
                        "class":          "details-control",
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ""
                    },
                    { "data": "nombre" },
					{ "data": "sesiones" }
                ],
                "order": [[2, 'asc']]
                } );
                
            var detailRows = [];
			
             //Mostrar la descripcion del detalle (mas y menos)    ---------------------------------------------------------------------------
            $('#tratamiento_det tbody').on( 'click', 'tr td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = dt_det.row( tr );
                    var idx = $.inArray( tr.attr('id'), detailRows );
                    
                    if ( row.child.isShown() ) {
                        tr.removeClass( 'details' );
                        row.child.hide();

                        // Remove from the 'open' array
                        detailRows.splice( idx, 1 );
                    }
                    else {
                        tr.addClass( 'details' );
                         
                            row.child( format_det( row.data() ) ).show();
                        // Add to the 'open' array
                            if ( idx === -1 ) {
                                detailRows.push( tr.attr('id') );
                            }                        
                    }
					
                } );
            $('#tratamiento_det tbody').on( 'click', 'tr td.precio', function () {                
                alert("muestro el precio");
            });
            //-------------------------------------------------------------------------------------
             dt_det.on( 'draw', function () {
                            $.each( detailRows, function ( i, id ) {                            
                                    $('#'+id+' td.details-control').trigger( 'click' );									
                            } );
                    } );     
					
            //--Mostrar intervenciones------------------------------------------------------------------------------------------------
              $('#tratamientos tbody').on( 'click', 'tr', function () {                  
                    dt.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    var aPos = $('#tratamientos').dataTable().fnGetPosition(this);
                    var aData = $('#tratamientos').dataTable().fnGetData();
                    cabecera_id = aData[aPos].id;
                    $('#composicion_nombre').html('('+aData[aPos].nombre+')');
                    dt_det.ajax.reload(null);					
                } );			
            //--------------------------------------------------------------------------------------------------------------------------------						
            //-- COMPOSICION DE TRATAMIENTOS   fin-----------------------------------------------------------------------------------------------------------
			
        });
	</script>	
</head>
<style>
    div.container { max-width: 1200px }  
</style>
<body>
	<div id="wrapper">
		<div id="page-wrapper">
                    <div class="col-lg-12">
                        <div class="panel panel-default" id="fichaTabla">
                            <div class="panel-heading">
                                <h3> Lista de Tratamientos </h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-6">
                                    <div class="table-responsive">
                                        <div class='well well-sm'>
                                                <h4>Nombre</h4>
                                        </div>
                                        <table id="tratamientos" class="display" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th></th>                                                
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>   
                                <div class="col-lg-6">
                                    <div class="table-responsive">
                                        <div class='well well-sm'>
                                            <h4>Composici&oacute;n &nbsp; <span id='composicion_nombre'></span>
											</h4>
                                        </div>
                                        <table id="tratamiento_det" class="display" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
													<th></th> 
                                                    <th>Nombre</th>                                                
                                                    <th>Sesiones</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>                                
                            </div>
                            <div class="panel-footer"> 
                                <button id="nuevo_tratamiento" type="button" class="btn btn-primary" >Nuevo Tratamiento</button>
                                <button id="editar_tratamiento" type="button" class="btn btn-primary" >Editar Tratamiento</button>
                            </div>
                        </div>
                    </div>
                            <!-- Modal -->
                            <div class="modal fade" id="precios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content well well-lg">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel"></h4>
                                        </div>                                        
                                        <div class="modal-body">
                                            <div class="well well-sm">                                               
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div><h4> Precio Unitario:&nbsp;<span id="pesos_unitario" class='label label-default'></span></h4></div>
                                                        <div><h4> Total por Sesiones:&nbsp;<span id="total_sesiones" class='label label-default'></span></h4></div>
                                                        <div><h4> Descuento tratamiento:&nbsp;<span id="porc_desc_trat" class='label label-warning'></span></h4></div>
                                                        <div><h4> Descuento intervenci&oacute;n:&nbsp;<span id="porc_desc_intrv" class='label label-warning'></span></h4></div>
                                                        <div><h4> Total:&nbsp;<span id="precio_total" class='label label-success'></span></h4></div>
                                                    </div>                                                        
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" >Detalle de Precios</button>                                            
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>                    
                </div>
	</div>
</body>
<script>
	function verPrecio(datos){
            var precio = datos.pesos_unit * datos.sesiones;
            var precio_trat = ((precio * datos.porc_desc_trat)/100);                           
            var precio_int = (((precio -precio_trat)  * datos.porc_desc_intrv)/100);
                       
                $("#precios").modal();
                $("#myModalLabel").html(datos.nombre);
                $("#pesos_unitario").html('$'+datos.pesos_unit);
                $("#total_sesiones").html('$'+precio);
                $("#porc_desc_trat").html('-$'+precio_trat);
                $("#porc_desc_intrv").html('-$'+precio_int);
                $("#precio_total").html('$'+(precio - precio_trat -precio_int));
	};
        
        function rowPrecioDet(id){
            var parametros = {"idDet": id};
            var rowDet;
            $.ajax({
                    async: false,
                    url: "<?=base_url();?>tratamientos/load_tratamiento_det_precios", 
                    data: parametros,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data, status, xhr) {                                                                    
                               rowDet = data
                            }
                });
                //console.log(rowDet);
            return rowDet;
        };
</script>
</html>	
      