<!DOCTYPE html>
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>Historias Clinicas - Login</title>
	
	<link rel="stylesheet" href="<?=base_url();?>css/bootstrap.min.css" >
	<link rel="stylesheet" href="<?=base_url();?>js/bootstrap3-dialog-master/dist/css/bootstrap-dialog.min.css" >
		
		
	<link rel="stylesheet" href="<?=base_url();?>js/jqwidgets/styles/jqx.base.css">
	<link href="<?=base_url();?>js/jqwidgets/styles/jqx.bootstrap.css" rel="stylesheet"> 
		
		
	<link rel="stylesheet" href="<?=base_url();?>css/messi/messi.min.css" type="text/css">
	<link rel="stylesheet" href="<?=base_url();?>css/ajaxLoader/ajaxloader.css" type="text/css">
	
	
    <!-- MetisMenu CSS -->
    <link href="<?=base_url();?>css/metisMenu.min.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="<?=base_url();?>css/sb-admin-2.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="<?=base_url();?>css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
</head>

<body>

    <div class="container"> 
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div id ="ventLogin" class="panel-body">
                        <form id="loginForm" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input id ="txtusuario" class="form-control" placeholder="Usuario" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input id="txtclave" class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button id="ingresarButton" type="button" class="btn btn-lg btn-success btn-block" >Login</button>
								
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 


    <script type="text/javascript" src="<?=base_url();?>js/jquery-1.10.2.min.js"></script> 

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url();?>js/bootstrap.min.js"></script> 
	<script src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script> 


	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxcore.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxvalidator.js"></script> 
	<script type="text/javascript" src="<?=base_url();?>js/jqwidgets/jqxbuttons.js"></script>
	<script type="text/javascript" src="<?=base_url();?>js/messi/messi.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=base_url();?>js/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?=base_url();?>js/sb-admin-2.js"></script>	
	
	<script type="text/javascript" src="<?=base_url();?>js/ajaxLoader/jquery.ajaxloader.1.5.1.min.js"></script>	
	
	<script type="text/javascript">
			/*
			function logeo(){
			alert("hola mundo");
			//$('#loginForm').jqxValidator('validate');			
		};
		*/
		
	$(document).ready(function (){				
		
            $('#ingresarButton').on('click', function(){			
				$('#loginForm').jqxValidator('validate');

            });
		
            $('#loginForm').jqxValidator({
			hintType: 'label',
    		onSuccess: function()
			{
				var parametros = {
	                "txtusuario" : $("#txtusuario").val(),
	                "txtclave" : $("#txtclave").val()
				};

				$.ajax(
				{
					type: "POST",
					dataType: "json",
					url: "<?=base_url();?>login/validarLogin",
					data: parametros,
					beforeSend: function () {
						$('#ventLogin').ajaxloader();
					},					
					success:  function (data) {
						if (data.valido == false)
						{
							$('#ventLogin').ajaxloader('hide');
							/*new Messi('Nombre de usuario o contraseña incorrecta', 
								{modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}
							);*/

							BootstrapDialog.show({
								title: 'Acceso denegado',
								message: 'Usuario o contrase&ntilde;a incorrecta',
								draggable: true,
								cssClass: 'login-dialog',
								buttons: [{
											label: 'Ok',
											action: function(dialogRef){
												dialogRef.close();
											}
										}]
							});
						}
						else if (data.valido == true)
						{
							if (data.estado == 0){ 							
								$('#ventLogin').ajaxloader('hide');
								/*new Messi('El usuario se encuentra inactivo', 
									{modal: true, title: 'Error', buttons: [{id: 0, label: 'Cerrar', val: 'X'}]}									
								)*/
								BootstrapDialog.show({
									title: 'Acceso denegado',
									message: 'El usuario se encuentra inactivo',
									draggable: true,
									cssClass: 'login-dialog',
									buttons: [{
												label: 'Ok',
												action: function(dialogRef){
													dialogRef.close();
												}
											}]
								});
							
							} else {
                                window.location.href = "<?php echo base_url().'Google_Calendar/login';?>";	
							}
						}
					},
					error(err){
						alert('error');
						console.log(err);
					}
				});
			}
		});
				
	});
	</script>

</body>

</html>
