<!DOCTYPE html>
<html>
    <head>
        <style>
            .file-drop-zone.clickable:hover                     { border: 1px dashed #ff0000; }
            .krajee-default.file-preview-frame                  { margin:2px; padding:0; }
            .krajee-default.file-preview-frame .kv-file-content { width:207px; height:160px; }
            .krajee-default .file-upload-indicator              { display:none; }
            .form-check-input                                   { position: absolute; margin-top: .3rem; margin-left: -1.45rem!important; }
            .file-no-browse                                     { z-index:-1; }   
            .portaSig                                           { width:100%; border:1px solid rgba(0,0,0,.2); margin:0 15px 15px; text-align:center; background-color:cornsilk; }
            /* #sig                                                { width:238px; height:238px; border:1px solid rgba(255,124,0,.2); } */
            /* #sig canvas                                         { background-color: #fff width:571px; height:238px; } */
            @media only screen and (max-width:1200px) {
                .krajee-default.file-preview-frame .kv-file-content     { width:165px; height:120px; }
            }
        </style>
        <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.0/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="<?=base_url();?>js/bootstrap3-dialog-master/dist/js/bootstrap-dialog.min.js"></script>       
        <script type="text/javascript" src="<?=base_url();?>application/views/dataValidator.js"></script>  
        <script type="text/javascript" src="<?=base_url();?>js/bootstrap-datepicker-master/js/bootstrap-datepicker.js"></script>       
         
        <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.0/js/fileinput.min.js"></script>
        <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.0/js/locales/LANG.js"></script>


        <script type="text/javascript">    
            var cambioHisto = false;
            var jsonHisto = null;
            var urlBase = "<?php echo base_url(); ?>";
            var idPacienteJs = 0;  
            var urlHistoRun = null;             
            var nombre = "<?php echo $nombre;?>";     
            var apellido = "<?php echo $apellido;?>"; 
            var action =  "<?php echo $action;?>"; 
            var idTrat = "<?php echo $idTrat;?>";  
            var fotos = [];
            
            $(document).ready(function () {

                $('#fotos').fileinput({
                   // uploadUrl: "<?=base_url();?>resources/UploadHandler.php", // your upload server url
                   //uploadUrl: "<?=base_url();?>resources/fileserver.php", 
                    uploadUrl: "<?=base_url();?>resources/",                    
                    allowedFileExtensions : ['jpg', 'gif', 'png', 'jpeg'],
                    msgInvalidFileExtension : 'Archivo "{name}" inválido. Solo archivos "{extensions}" son permitidos.',
                    msgSelected: "{n} archivos seleccionados",
                    msgDuplicateFile: 'Archivo duplicado. El archivo "{name}" ya esta seleccionado',
                    msgPlaceholder: 'Archivos de imagen ...',
                    msgLoading: 'Cargando {index} archivos de {files} ...',
                    msgNoFilesSelected: "No se ha seleccionado ningún archivo.",
                    dropZoneEnabled : false,
                    showUpload: false,
                    showCancel : false,
                    showPreview: true,
                    minFileCount: 1,
                    msgFilesTooLess: 'Debe seleccionar al menos <b>{n}</b> {files}. Puede limpiar todo con el boton si desea.',
                    validateInitialCount: false,
                    dropZoneTitle: "Arrastrar y soltar...",
                    browseLabel : "Agregar",
                    removeTitle : "Quitar todo",
                    removeLabel : "Limpiar",
                    fileActionSettings: {showRemove: true,
                                        showUpload: false,
                                        showDownload: false,
                                        showZoom: true,
                                        removeTitle: 'Quitar'}
                })
           

                //Obtener lista de profesionales ajax
                $.ajax({
                        async: false,
                        url: "<?=base_url();?>tratamientos/load_profesionales",
                        success: function (data, status, xhr) {
                            profesionales = $.parseJSON(data);                            
                        }
                    });               
                //cargar la lista de profesinales
                $("#profesional").append('<option value="0">Seleccione...</option>');
                $.each(profesionales,function(key, value){
                    $("#profesional").append('<option value=' + value.idProf + '>' + value.nombre + '</option>');
                });
                //cargar los datos si es que esta pantalla es llamada desde la lista de tratamientos (boton historia)
                if(action == 'asignar'){
                    var descrip ="<?php echo str_replace('<br />','\n',$descrip);?>";
                    $("#profesional").val("<?php echo $prof;?>");
                    $("#txtIntervencion").val(descrip);                    
                }
                //-------
                $('.input-group.date').datepicker({
                        format: "dd/mm/yyyy",
                        language: "es",
                        clearBtn: true,
                        autoclose: true,
                        startDate: '01/01/1900',
                        endDate: '01/01/5000'
                    });

            $("#formHisto").jqxValidator({
                    hintType: 'label',
                    animationDuration: 500,  
                    rules: [
                            {input: '#fecha', 
                            message: 'Fecha Incorrecta!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){  
                                    var valid = dateValidatorNN($('#fecha'));
                                    return valid;
                                }
                            },
                            {input: '#fecha', 
                                message: 'Debe ingresar una fecha!', 
                                action: 'keyup, blur', 
                                rule: 'required' },
                            {input: '#profesional', 
                                message: 'Debe ingresar una profesional!', 
                                action: 'keyup, blur', 
                                rule: function(input,commit){
                                    if ($("#profesional").val()!= 0){
                                        return true;
                                    }else return false;
                                } 
                            },
                            {input: '#txtIntervencion', 
                                message: 'Debe ingresar un valor', 
                                action: 'keyup, blur', 
                                rule: 'required'}
                    ] ,
                    onSuccess: function(){
                        var parametros = { 
                            "idPaciente" :idPacienteJs ,
                            "fecha" :  $("#fecha").val(),
                            "profesional" : $("#profesional").val(),
                            "tratamiento" : $("#txtTratamiento").val().trim(),
                            "diagnostico" : $("#txtDiagnostico").val().trim(),
                            "medicamentos" : $("#txtMedicamentos").val().trim(),
                            "intervencion" : $("#txtIntervencion").val().trim(),
                            "idTrat":idTrat,
                            "fotos" : fotos
                        }; 
                        
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            data: parametros,                            
                            url: urlHistoRun,
                            success:  function (data) {                                    
                                if(!data.valido){
                                    //idPacienteJs = 0;
                                    BootstrapDialog.show({
                                            type: BootstrapDialog.TYPE_DANGER,
                                            title: 'Error!!',
                                            message: 'Los cambios no pudieron ser salvados',
                                            draggable: true,								
                                            buttons: [{
                                                        label: 'Ok',
                                                        action: function(dialogRef){dialogRef.close();}
                                                    }]
                                    });
                                }else{                                   
                                    cambioHisto = false;
                                    var titulo = "Informaci&oacute;n";
                                    var tipo = BootstrapDialog.TYPE_SUCCESS;
                                    var mensaje = "Los cambios fueron salvados satisfactoriamente";
                                    if(data.repetidas.length >0 ){
                                        titulo = "Atenci&oacute;n";
                                        mensaje = "La selección contiene alguna foto de nombre repetido. Verifique las imagenes que se han cargado.";
                                        tipo = BootstrapDialog.TYPE_WARNING;
                                        console.log(data.repetidas);
                                    }
                                    if(data.conError.length >0){
                                        titulo = "Atenci&oacute;n";
                                        mensaje = "Ocurrio un error al guardar una foto";
                                        tipo = BootstrapDialog.TYPE_WARNING
                                        console.log(data.conError);
                                    }
                                    
                                    BootstrapDialog.show({
                                            onshown: function(dialogRef){
                                                            setTimeout(function(){
                                                            dialogRef.close();
                                                            }, 2000);
                                                    },
                                            onhide: function(dialogRefh){
                                                location.replace(document.referrer); //haciendo así hace el back con el refresco de la pantalla
                                            },
                                            type: tipo,
                                            title: titulo,
                                            message: mensaje,
                                            draggable: true,								
                                            buttons: [{
                                                        label: 'Ok',
                                                        action: function(dialogRef){dialogRef.close();}
                                                    }]
                                    });                                                                															   
                                }
                        },
                        error: function(xhr,status,error){
                                console.log(error);
                                console.log(status);
                                console.log(xhr);
                            }
                        });
                },
                onError: function (e){
                    console.log("hay error onError");
                    console.log(e);
                    console.log(fotos);
                }                                
                }); 
            
                    urlHistoRun = urlBase + 'histo/add_histo'; 
                    var today = new Date();
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = today.getFullYear();
                    today = dd + '/' + mm + '/' + yyyy;
                    $('#fecha').val(today);
                    
                            
                idPacienteJs =  <?php echo $idPaciente ;?>;  
            // FORM HISTO ---------------------------------------------------------                                    
            
                $("#btnCancelHisto").click(function() {  
                    window.history.back();
                });

                //GUARDAR DATOS Y UPLOAD DE FOTOS SELECCIONADAS
                $('#btnGuardarHisto').on('click', function(){	
                    var filestack = $('#fotos').fileinput('getFileStack');
                    
                    $.each(filestack, function(fileId, fileObj) {
                        if (fileObj !== undefined) {
                            fotos.push(fileObj.name);
                        }
                    });
                    if (fotos.length){
                        $('#fotos').fileinput('upload');
                    }
                    $('#formHisto').jqxValidator('validate');      
                });     
                        
            });

        </script>	
    </head>
    <body>
	<div id="wrapper">             
            <div id="page-wrapper">                    
                <div class="panel panel-default" id="anamnesis">
                    <div class="panel-heading">
                        <h4><span id="histoTitle">Historia clínica de <?php echo $nombre;?>  <?php echo $apellido;?></span></h4>
                    </div>
                    <div class="panel-body">
                        <div id="rowHisto"  class="row">                                        
                            <form id="formHisto" role="form">
                                    <fieldset>                                                
                                        <div class="form-group">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <label>* Fecha:</label>
                                                <div class="input-group date">
                                                    <input id="fecha" name="fecha" class="form-control input-append date" data-date-format="dd/mm/yyyy"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <label>* Profesional:</label>
                                                <select id="profesional" class="form-control selectpicker" data-live-search="true" title="Seleccione un Profesional..." data-width="70%">                                        
                                                </select>
                                            </div>  
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <label>* Descripci&oacute;n Intervenci&oacute;n: </label>
                                                <textarea name="intervencion" id="txtIntervencion" class="form-control" rows="3"></textarea>                                                       
                                            </div>                                                                                   
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <label>Descripci&oacute;n Tratamiento: </label>
                                                <textarea name="tratamiento" id="txtTratamiento" class="form-control" rows="3"></textarea>                                                       
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <label>Diagn&oacute;stico: </label>
                                                <textarea name="diagnostico" id="txtDiagnostico" class="form-control" rows="3"></textarea>                                                       
                                            </div>                                            
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                  <label>Medicamentos:</label>
                                                <textarea name="medicamentos" id="txtMedicamentos" class="form-control" rows="3"></textarea>                                                       
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">(* campos obligatorios) </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></br></div>                 
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label>Fotos:</label>
                                                <div class="file-loading">
                                                    <input id="fotos" name="fotos[]" type="file" multiple>
                                                </div> 
                                            </div>
                                        </div>
                                    </fieldset>
                            </form>
                        </div>
                    <div class="col-lg-1 col-md-1 col-sm-1"></div>                 
                    <div class="panel-footer">
                        <button id="btnGuardarHisto" type="button" class="btn btn-primary">Guardar</button>
                        <button id="btnCancelHisto" type="button" class="btn btn-primary">Volver</button>
                    </div>                                
                </div> 
                <div class="col-lg-1 col-md-1 col-sm-1"></div>                 
            </div>
        </div>
    </body>
</html>	
      