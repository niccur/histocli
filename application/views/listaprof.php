 <!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/media/css/jquery.dataTables.css" >
    <link rel="stylesheet" href="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/css/dataTables.responsive.css" >             	
    
    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/DataTables-1.10.6/extensions/Responsive/js/dataTables.responsive.min.js"></script>

    <script>
       
        var urlBase = "<?php echo base_url(); ?>"; 
        var botonera = '';
        $(document).ready(function() {	
            botonera = '<div class= "row">\n\
                                    <div class="col-lg-11 col-sm-11 col-xs-12">\n\
                                        <button type="button" class="btn-eliminar btn btn-outline col-lg-4 col-sm-4 col-xs-4" data-toggle="tooltip" title="Eliminar profesional"><i class="fa fa-trash-o fa-lg" style="color:red"></i></button>\n\
                                        </div>\n\
                                </div>';

            $('#altas').on('click', function(){
                var url = "<?php echo base_url().'main/profesionalForm';?>";
                $(location).attr('href',url); 
            });
            var table = $('#profesional').DataTable( {
                "sAjaxSource": "<?=base_url();?>main/load_profesionales",
                "sAjaxDataProp": "",
                "info": false,
                select: {
                    style: 'os'
                },
                language: {
                    "sProcessing":     "Procesando...",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "No hay registros" ,
                    "search": "_INPUT_",	
                    "searchPlaceholder": "Buscar..."													
                },
                "responsive": false,
                "scrollY": 300,     
                "paging": false,
                "columns": [
                    { "data": "idProf"},    
                    { "data": "nombre" },
                    { "data": "apellido" },
                    { "orderable": false, 
                        data: null,
                        width: 90,
                        className: "center",
                        defaultContent: botonera
                    }
                ],
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    }
                ]
            }) ;
            $('#profesional tbody').on( 'click', 'button.btn-eliminar', function (e) {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var idProf = row.data().idProf;
                console.log(idProf);
                BootstrapDialog.show({
                 type: BootstrapDialog.TYPE_WARNING,
                 title: '¡Atenci&oacute;n!',
                 message: "¿Confirma eliminar el profesional seleccionado?",
                 draggable: true,								
                 buttons: [{
                         label: 'Si',
                         action: function(dialogRef){   
                             var url = urlBase + "main/eliminar_profesional/"+idProf;                                                     
                             $.ajax({
                                 type: "DELETE",
                                 dataType: "json",
                                 url: url,
                                 error: function(err){
                                     if(err.status == 200){
                                        console.log('hay error1');
                                     }else{
                                        console.log('hay error2');
                                     } 
                                     console.log(err); 
                                    location.reload();
                                 },                          
                                 success:  function (data) {
                                    console.log(data.valido);
                                     if(data.valido==1){
                                         BootstrapDialog.show({
                                             type: BootstrapDialog.TYPE_DANGER,
                                             title: 'Error!!',
                                             message: 'No se pudo eliminar el profesional.',
                                             draggable: true,								
                                             buttons: [{
                                                     label: 'Ok',
                                                     action: function(dialogRef){dialogRef.close();}
                                                 }]
                                         }); 
                                        }else if(data.valido==2){                                        
                                         BootstrapDialog.show({
                                             type: BootstrapDialog.TYPE_DANGER,
                                             title: 'Error!!',
                                             message: 'No se puede eliminar el profesional. Contiene historias clinicas asociadas.',
                                             draggable: true,								
                                             buttons: [{
                                                     label: 'Ok',
                                                     action: function(dialogRef){dialogRef.close();}
                                                 }]
                                         }); 
                                        }else if(data.valido==3){                                        
                                         BootstrapDialog.show({
                                             type: BootstrapDialog.TYPE_DANGER,
                                             title: 'Error!!',
                                             message: 'No se puede eliminar el profesional. Contiene turnos con pacientes asignados.',
                                             draggable: true,								
                                             buttons: [{
                                                     label: 'Ok',
                                                     action: function(dialogRef){dialogRef.close();}
                                                 }]
                                         }); 
                                    }else{                                                                   
                                         location.reload();
                                     }                                                            
                                 }
                             });
                             dialogRef.close();
                         }
                     },
                     {
                         label: 'No',
                         action: function(dialogRef){dialogRef.close();}
                     }
                 ]
             });
        
            }); 
       
         
        });
	</script>	
</head>
<style>
    div.container { max-width: 1200px }  
    .modal-dialog {
        max-width: 350px;
    }
    @media(max-width:768px) {
        .modal-dialog {
            max-width: 300px;
        }
    }  
    td.details-control {
            text-align:center;
            color:forestgreen;
    cursor: pointer;
    }
    tr.shown td.details-control {
        text-align:center; 
        color:red;
    }
    table {
        table-layout:fixed;
      }
    table td {
      word-wrap: break-word;
      max-width: 400px;
    }
    #pacientes td {
      white-space:inherit;
    }
    .table-responsive{
        height: 65%;
    }

</style>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="col-lg-12">
                <div class="panel panel-default" id="fichaTabla">
                    <div class="panel-heading">
                        <h4>Profesionales</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="profesional" class="table hover table-bordered responsive nowrap compact" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button id="altas" type="button" class="btn btn-primary" >Nuevo Profesional</button>
                    </div>
                </div>
            </div>           
        </div>
    </div>
</body>
</html>