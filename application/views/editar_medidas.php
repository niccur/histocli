<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="<?= base_url(); ?>js/bootstrap-datetimepicker-4.17.47/build/css/bootstrap-datetimepicker.min.css" >   
        <script type="text/javascript" src="<?=base_url();?>application/views/dataValidator.js"></script> 
        <script src="<?= base_url(); ?>js/moment-master/moment.js"></script>   
        <script src="<?= base_url(); ?>js/bootstrap-datetimepicker-4.17.47/build/js/bootstrap-datetimepicker.min.js"></script>   
        <script type="text/javascript" src="<?=base_url();?>application/views/histomedidas.js"></script>  
    </head>
    <style>
        div.container { max-width: 1200px }
    </style>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="col-lg-1 col-md-1 col-sm-1"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">                       
                    <div class="panel panel-default" id="fichaTabla">
                        <div class="panel-heading">
                            <h4 id="medidasModalLabel">Nueva Medida</h4>
                        </div>
                        <div class="panel-body"> 
                            <form id="formMedidas" role="form">
                                <fieldset> 
                                    <div class="form-group row">
                                    <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">-->
                                    <div class="col-md-6">
                                            <label>Fecha:</label>
                                            <div class="input-group date">
                                            <input id="fec_medida" name="fec_medida class="form-control input-append date" data-date-format="dd/mm/yyyy"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>  
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Brazo:</label>
                                            <input name="brazo" id ="txtBrazo" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Busto:</label>
                                            <input name="busto" id ="txtBusto" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Cintura:</label>
                                            <input name="cintura" id ="txtCintura" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Cadera Alta:</label>
                                            <input name="caderaAlta" id ="txtCaderaAlta" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Cadera Baja:</label>
                                            <input name="caderaBaja" id ="txtCaderaBaja" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Gluteos:</label>
                                            <input name="gluteos" id ="txtGluteos" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Muslo Superior:</label>
                                            <input name="musloSuperior" id ="txtMusloSuperior" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Muslo Inferior:</label>
                                            <input name="musloInferior" id ="txtMusloInferior" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Rodilla:</label>
                                            <input name="rodilla" id ="txtRodilla" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Pantorrilla:</label>
                                            <input name="pantorrilla" id ="txtPantorrilla" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>
                                        <div class="col-lg-6">   
                                            <label>Tobillo:</label>
                                            <input name="tobillo" id ="txtTobillo" class="form-control" onkeypress="return isNumberDecimal(event)">
                                        </div>                            
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                            
                        <div class="panel-footer"> 
                            <div>
                                <button id="btnMedida" type="button" class="btn btn-primary">Guardar</button>
                                <button id="cancelar" type="button" class="btn btn-primary" >Volver</button>
                            </div>
                        </div>
                    </div>                      
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1"></div> 
            </div>
        </div>
    </body>
       
        <script>
            var urlBase = "<?php echo base_url(); ?>";        
            var modo = "<?php echo $modo;?>";  
            var idMedida = <?php echo $idMedida;?>; 
            var idPaciente = <?php echo $idPaciente;?>; 
            
            $("#formMedidas").jqxValidator({        
                hintType: 'label',
                animationDuration: 500,  
                rules: [
                        {input: '#fec_medida', 
                            message: 'Fecha Incorrecta!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){                                
                                        return valido_fecha_medida;
                                    }
                        },
                        {input: '#txtBrazo', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtBusto', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtCintura', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtCaderaAlta', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtCaderaBaja', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtGluteos', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtMusloSuperior', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtMusloInferior', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtRodilla', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 )
                                            return false;                                                
                                        return true;
                                    }
                        },
                        {input: '#txtPantorrilla', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        },
                        {input: '#txtTobillo', 
                            message: 'Valor Incorrecto!', 
                            action: 'keyup, blur', 
                                rule: function(input, commit){ 
                                        var countComma = substr_count(input.val(), ',');
                                        var countDot = substr_count(input.val(), '.');
                                        if(countComma + countDot > 1 ){
                                            return false;
                                        }
                                        return true;
                                    }
                        }
                    ],    
                onSuccess: function(){   
                    var parametros = { 
                        "idPaciente" :idPaciente ,
                        "idMedida" : idMedida,
                        "fecha" :  $("#fec_medida").val(),
                        "brazo": $("#txtBrazo").val(),
                        "busto": $("#txtBusto").val(),
                        "cintura": $("#txtCintura").val(),
                        "cadera_alta": $("#txtCaderaAlta").val(),
                        "cadera_baja": $("#txtCaderaBaja").val(),
                        "gluteos": $("#txtGluteos").val(),
                        "muslo_superior": $("#txtMusloSuperior").val(),
                        "muslo_inferior": $("#txtMusloInferior").val(),
                        "rodilla": $("#txtRodilla").val(),
                        "pantorrilla": $("#txtPantorrilla").val(),
                        "tobillo": $("#txtTobillo").val()
                    };                                    
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: urlMedidasRun,
                        data: parametros,
                        success:  function (data) {  
                            if(!data.valido){
                                BootstrapDialog.show({
                                        type: BootstrapDialog.TYPE_DANGER,
                                        title: 'Error!!',
                                        message: 'Los cambios no pudieron ser salvados',
                                        draggable: true,								
                                        buttons: [{
                                                    label: 'Ok',
                                                    action: function(dialogRef){dialogRef.close();}
                                                }]
                                });
                            }else{
                                //parent.history.back();
                                location.replace(document.referrer); //haciendo así hace el back con el refresco de la pantalla
                            }
                        }
                    });
                }                                
            });    
            $('#fec_medida').change(function(){
                    valido_fecha_medida = dateValidatorNN($(this));
                    $('#formMedidas').jqxValidator('validateInput', '#fec_medida'); 
            });                    
             $( "#btnMedida" ).click(function() {
                if(cambioMedida){
                    $('#formMedidas').jqxValidator('validate');
                }            
            });
            $( "#cancelar" ).click(function() {
                parent.history.back();
            });
            $('#formMedidas input').change(function() { 
                if(cambioMedida===false){
                    cambioMedida = true;
                }    
            });


            $(document).ready(function () {
                if (modo === "editar")
                    loadMedida();                        
                else 
                    newMedida();
                });
        </script>	
</html>	
